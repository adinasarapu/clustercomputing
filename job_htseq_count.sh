#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N HTSeq.iPSC
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot
#$ -q all.q
#$ -l h_rt=220:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting slots/cores ( ?? )
#$ -pe smp 30
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR=$HOME/Hyder_Jinnah/HJI11124_IPSC
GENES_GTF=$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf

for postfix in {145..162}
do
	SID=SL146${postfix}
	MAP_DIR="${PROJ_DIR}/${SID}/star_mapping"
	OUT_DIR="${PROJ_DIR}/${SID}/htseq_count"
	BAM_FILE=${MAP_DIR}/Aligned.sortedByCoord.out.bam

	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}
	fi
	
	# -t TYPE, --type=TYPE  type of read_file
	# 
	htseq-qa -t bam -o ${OUT_DIR}/${SID} ${BAM_FILE}
	
	# -m union: union is one of three options for htseq count for binning of
	# 	reads to features (genes/exons).  it is the recommended option per
	# 	htseq count’s manual.  It is the most stringent option as it will only
	# 	count a read if it aligns to only 1 gene.
	# -r pos : meaning that the BAM/SAM file is sorted by genomic
	# 	coordinate/position
	# -i gene_name : tells htseq-count to use the gene name in output file
	# -a 10 : tells htseq-count to skip reads with alignment score less
	# 	than 10
	# –stranded=no : the RNAseq reads are not strand specific
	htseq-count -f bam \
		-m union \
		-r pos \
		-i gene_id \
		-a 10 \
		-s no \
		${BAM_FILE} ${GENES_GTF} \
		> ${OUT_DIR}/${SID}.counts
done
