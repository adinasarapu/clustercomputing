#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N HTSeq
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot
#$ -q b.q
#$ -l h_rt=220:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting slots/cores ( ?? )
#$ -pe smp 9
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M xyz@emory.edu

PROJ_DIR=$HOME/Hyder_Jinnah

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
        echo “Error. Cannot find mktemp to create tmp directory”
        exit
fi

chmod a+trwx $TMPDIR

# copy genes.gtf into $TMPDIR
rsync -av $HOME/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf $TMPDIR/
rsync -av $HOME/Hyder_Jinnah/MAPPING $TMPDIR/

for SID in AK DP2 MG PB PY TH TS WW; do
 BAM=$TMPDIR/MAPPING/$SID/STAR/Aligned.sortedByCoord.out.bam
 OUT_DIR=$TMPDIR/HTSeq/$SID
 if [ ! -d $OUT_DIR ]; then
  /bin/mkdir -p $OUT_DIR
 fi
	
 # -t TYPE, --type=TYPE  type of read_file 
 htseq-qa -t bam -o $OUT_DIR/$SID $BAM
	
 # -m union: union is one of three options for htseq count for binning of
 # 	reads to features (genes/exons).  it is the recommended option per
 # 	htseq count’s manual.  It is the most stringent option as it will only
 # 	count a read if it aligns to only 1 gene.
 # -r pos : meaning that the BAM/SAM file is sorted by genomic coordinate/position
 # -i gene_name : tells htseq-count to use the gene name in output file
 # -a 10 : tells htseq-count to skip reads with alignment score less than 10
 # –stranded=no : the RNAseq reads are not strand specific
 htseq-count -f bam -m union -r pos -i gene_id -a 10 -s no $BAM $TMPDIR/genes.gtf > $OUT_DIR/${SID}.counts

done

/bin/rm $TMPDIR/genes.gtf
/bin/rm -rf $TMPDIR/MAPPING
rsync -av $TMPDIR/* $PROJ_DIR/
/bin/rm -rf $TMPDIR
