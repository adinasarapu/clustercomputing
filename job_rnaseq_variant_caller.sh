#!/bin/sh

#################################################
# Author: Ashok R. Dinasarapu Ph.D		#
# Emory University, Dept. of Human Genetics	#
# E-mail: ashok.reddy.dinasarapu@emory.edu	#
# Webpage: http://adinasarapu.github.io 	#
#################################################

echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N GATK.iDB4
## The cluster resources (the nodes) are grouped into Queues
## Each queue is associated with a number of slots 
## one computational process runs in each slot
#$ -q b.q
##$ -l h_rt=200:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 28 slots/cores ( ?? )
#$ -pe smp 28
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

#=======================================================================================#
# Cores = Cores per socket X Sockets (8 x 2 = 16 Cores)					#				
# CPUs = Threads per core X Cores (1 x 16 = 16 CPUs/Processors)				#
# lscpu (2 sockets; 4 cores/ socket; 1 thread / core; i.e total 16 threads)		#
# lscpu | egrep 'Thread|Core|Socket|^CPU\('						#
# need to be careful of the term CPU as it means different things in different contexts #
#=======================================================================================#

# https://software.broadinstitute.org/gatk/documentation/article.php?id=3891
# https://github.com/ryanabo/rnaseq-analysis/blob/master/run_star.sh

echo -----------------------------------------------
echo -e `cat $PE_HOSTFILE` # | awk 'BEGIN{ORS=" ";} {print $1}'`
echo -----------------------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ------------------------------------------------------

set -o nounset      # Treat unset variables as an error

PROJ_DIR=$HOME/Hyder_Jinnah
FASTQ_DIR=$PROJ_DIR/data/ipsc/dystonia

# add a single RGSM but different RGID to all the replicates (from the same sample)
# i.e. treating them as the same sample but with different libraries (read groups)

SID="SL146158"
PICARD_RGSM="Dystonia" # "Control,Dystonia"
PICARD_RGLB="iDB4" # "None"

STAR_DIR=$HOME/STAR/bin/Linux_x86_64
GENOME_DIR=$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta
STAR_DATA=/sw/hgcc/Pkgs/GATK/3.8/hg38bundle
INDEL_VCF=$STAR_DATA/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
SNP_VCF=$STAR_DATA/1000G_phase1.snps.high_confidence.hg38.vcf.gz

#========================#
# Create a unique TMPDIR #
#========================#
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR

# Create folder and subfolders for results
OUT_DIR=$TMPDIR/GATK_${PICARD_RGSM}/iPSC/$SID
if [ ! -d $OUT_DIR ]; then
 /usr/bin/mkdir -p $OUT_DIR/{star1pass,star2pass,gatk}
fi

#=====================#
# Copy data to TMPDIR #
#=====================#
rsync -av $GENOME_DIR/genome* $TMPDIR/
rsync -av $INDEL_VCF $TMPDIR/known.hg38.indels.vcf.gz
rsync -av $SNP_VCF $TMPDIR/known.hg38.snps.vcf.gz
rsync -av $FASTQ_DIR/C8DF6ANXX_[1,2]_GSLv3-7_${SID}.fastq.gz $TMPDIR/

R1=$TMPDIR/C8DF6ANXX_1_GSLv3-7_${SID}.fastq.gz
R2=$TMPDIR/C8DF6ANXX_2_GSLv3-7_${SID}.fastq.gz

# samtools needed for tabix indexing
# .idx index is for uncompressed VCFs
# .tbi tabix index is for block-compressed .gz VCFs
# tabix: command not found error in samtools/1.5
module load samtools/1.3
tabix -p vcf $TMPDIR/known.hg38.snps.vcf.gz
tabix -p vcf $TMPDIR/known.hg38.indels.vcf.gz
module unload samtools/1.3

# After STAR runs, you should have a few files in the /STAR_hg38/ directory:
# Genome, genomeParameters.txt, chrLength.txt, chrNameLength.txt,
# chrStart.txt, SA, SAindex, Log.out

genomeDir1=$TMPDIR/STAR_hg38
/usr/bin/mkdir -p $genomeDir1

# STAR genome indexing
$STAR_DIR/STAR \
 --runMode genomeGenerate \
 --genomeDir $genomeDir1 \
 --genomeFastaFiles $TMPDIR/genome.fa \
 --runThreadN 26

ls -la $genomeDir1

# STAR generates:
# Log.out & Log.progress.out
# Log.final.out – summary mapping statistics – will give you an idea on qc
# Aligned.out.sam & SJ.out.tab – high confidence splice junction in tab-delimited format

runDir1=$OUT_DIR/star1pass
$STAR_DIR/STAR \
 --genomeDir $genomeDir1 \
 --readFilesIn $R1 $R2 \
 --runThreadN 26 \
 --readFilesCommand zcat \
 --outFileNamePrefix $OUT_DIR/star1pass/

ls -la $runDir1
	
# For the 2-pass STAR, a new index is then created using splice junction information contained 
# in the file SJ.out.tab from the first pass

genomeDir2=$OUT_DIR/STAR_hg38_2pass	
/usr/bin/mkdir $genomeDir2

$STAR_DIR/STAR \
 --runMode genomeGenerate \
 --genomeDir $genomeDir2 \
 --genomeFastaFiles $TMPDIR/genome.fa \
 --sjdbFileChrStartEnd $OUT_DIR/star1pass/SJ.out.tab \
 --sjdbOverhang 99 \
 --runThreadN 26

ls -la $genomeDir2

runDir2=$OUT_DIR/star2pass

$STAR_DIR/STAR \
 --genomeDir $genomeDir2 \
 --readFilesIn $R1 $R2 \
 --readFilesCommand zcat \
 --runThreadN 26 \
 --outFileNamePrefix $OUT_DIR/star2pass/

ls -la $runDir2

# MESSAGE: Writing failed because there is no space left on the disk or hard drive. 
# Please make some space or specify a different location for writing output files. 
/bin/rm -rf $genomeDir1	
/bin/rm -rf $genomeDir2

# Add read groups, sort, mark duplicates, and create index
# RGLB (read group library)
# RGID (read group ID)
# RGPU (run barcode)
# RGPL (platform, i.e. illumina, solid, etc)
# RGSM (sample name)

module load picardtools/2.6.0
$PICARD AddOrReplaceReadGroups \
 I=$runDir2/Aligned.out.sam \
 O=$runDir2/RG_Aligned.Sorted.out.bam \
 SO=coordinate \
 RGID=$SID \
 RGLB=$PICARD_RGLB \
 RGPL=illumina \
 RGPU=None \
 RGSM=$PICARD_RGSM

$PICARD MarkDuplicates \
 I=$runDir2/RG_Aligned.Sorted.out.bam \
 O=$runDir2/${SID}.dedupped.bam \
 CREATE_INDEX=true \
 VALIDATION_STRINGENCY=SILENT \
 M=$runDir2/${SID}.output.metrics
module unload picardtools/2.6.0

runDir3=$OUT_DIR/gatk

# -T  <analysis_type>		Name of the tool to run
# -I  <input_file>
# -nt <num_threads>     	Number of data threads to allocate to this analys
# -R  <reference_sequence> 
# -rf <read_filter>             Filters to apply to reads before analysis (intended primarily for RNA-Seq data)
# -RMFQ 			Original mapping quality (255)
# -RMQT 			Desired mapping quality (60)
# -U <unsafe operation>	 

# Split'N'Trim and reassign mapping qualities
# module load java/1.8.0_152 (may conflict with already loaded module)

module load GATK/3.8
# By default this also loads java 1.8

# location of the dir used to hold temporary files is defined by the property java.io.tmpdir 
# The default value is typically "/tmp" on Unix-like platforms

# -D<name>=<value>	set a system property
# -Xms<size>		set initial Java heap size
# -Xmx<size>		set maximum Java heap size

JAVA_ARGS="-Xms512m -Xmx16G -Djava.io.tmpdir=$TMPDIR"

#====================================================#
# Splits reads that contain Ns in their CIGAR string #
#====================================================#
# -T  <analysis_type>           Name of the tool to run
# -I  <input_file>
# -nt <num_threads>             Number of data threads to allocate to this analys
# -R  <reference_sequence> 
# -rf <read_filter>             Filters to apply to reads before analysis (intended primarily for RNA-Seq data)
# -RMFQ                         Original mapping quality (255)
# -RMQT                         Desired mapping quality (60)
# -U <unsafe operation>  

java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T SplitNCigarReads \
 -R $TMPDIR/genome.fa \
 -I $runDir2/${SID}.dedupped.bam \
 -o $runDir3/${SID}.split.bam \
 -rf ReassignOneMappingQuality \
 -RMQF 255 \
 -RMQT 60 \
 -U ALLOW_N_CIGAR_READS
	
#=================================================================#
# IndelRealigner, perform local realignment of reads around indel #
#=================================================================#
java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T RealignerTargetCreator \
 -R $TMPDIR/genome.fa \
 -o $TMPDIR/intervalListFromRTC.intervals \
 -known $TMPDIR/known.hg38.indels.vcf.gz

java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T IndelRealigner \
 -R $TMPDIR/genome.fa \
 -I $runDir3/${SID}.split.bam \
 -known $TMPDIR/known.hg38.indels.vcf.gz \
 -targetIntervals $TMPDIR/intervalListFromRTC.intervals \
 -o $runDir3/${SID}.realignedBam.bam
 
#========================================#
# BQSR, Base quality score recalibration #
#========================================#

java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T BaseRecalibrator \
 -R $TMPDIR/genome.fa \
 -I $runDir3/${SID}.realignedBam.bam \
 -knownSites $TMPDIR/known.hg38.snps.vcf.gz \
 -o $runDir3/${SID}.recal_data.table

java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T PrintReads \
 -R $TMPDIR/genome.fa \
 -I $runDir3/${SID}.realignedBam.bam \
 -BQSR $runDir3/${SID}.recal_data.table \
 -o $runDir3/${SID}.baserecal.bam
	
#=================#
# Variant calling #
#=================#
# The analysis HaplotypeCaller currently does not support parallel execution with nt
# stand_call_conf: The minimum phred-scaled confidence threshold at which variants should be called
java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T HaplotypeCaller \
 -R $TMPDIR/genome.fa \
 -I $runDir3/${SID}.baserecal.bam \
 -dontUseSoftClippedBases \
 -stand_call_conf 20.0 \
 -o $runDir3/${SID}.vcf

#===================#
# Variant filtering #
#===================#
# https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set
# -cluster: The number of SNPs which make up a cluster
# -window: The window size (in bases) in which to evaluate clustered SNPs
	
# QualByDepth (QD)
# FisherStrand (FS)
# RMSMappingQuality (MQ)
# MappingQualityRankSumTest (MQRankSum)
# ReadPosRankSumTest (ReadPosRankSum)
# StrandOddsRatio (SOR) 

java $JAVA_ARGS -jar /sw/hgcc/Pkgs/GATK/3.8/GenomeAnalysisTK.jar \
 -T VariantFiltration \
 -R $TMPDIR/genome.fa \
 -V $runDir3/${SID}.vcf \
 -window 35 \
 -cluster 3 \
 -filterName FS -filter "FS > 30.0" \
 -filterName QD -filter "QD < 2.0" \
 -o $runDir3/${SID}.filtered.vcf

module unload GATK/3.8
# module unload java/1.8.0_152
/bin/rm $TMPDIR/*.fastq.gz
# /bin/rm -rf $genomeDir2
# /bin/rm -rf $genomeDir1
/bin/rm -fr $TMPDIR/genome.*
/bin/rm $TMPDIR/known.hg38.*

# copy results
rsync -av $TMPDIR/* $PROJ_DIR/

# remove the temp directory
/bin/rm -fr $TMPDIR

# TO DO:
# WARN HaplotypeCallerGenotypingEngine - location chr1:26475246-26475249: too many alternative alleles found (7) larger than the maximum requested with -maxAltAlleles (6), the following will be dropped: ATTT.
# WARN  17:33:22,686 HaplotypeScore - Annotation will not be calculated, must be called from UnifiedGenotyper, not HaplotypeCaller
# CatVariants
