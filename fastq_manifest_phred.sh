#!/bin/sh

# checks-fastq-quality-score-format

FASTQ_FILE=$HOME/microbiome/PTB_0816/MZW20791-0129.8.1_G0065-1-RecM1_S1_L001_R2_001.fastq.gz

zcat ${FASTQ_FILE} \
	| awk 'NR%4==0 {printf $0}' \
	| tr -d '\n' \
	| hexdump -v -e'/1 "%u\n"' \
	| head -n100000 \
	| sort -u

# Also you can use 
# https://github.com/brentp/bio-playground/blob/master/reads-utils/guess-encoding.py

# According to "guess-encoding.py" script the possible ranges are
# 33-93 (Sanger/Illumina1.8), 
# 64-104 (Illumina1.3 or Illumina1.5) and 
# 59-104 (Solexa). 
