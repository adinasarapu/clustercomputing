# ChAMP is both available for DNA Methylation EPIC arrays 450k or EPIC.
# run QC on the Illumina Methylation EPIC arrays

# 1. Singular value decomposition (SVD) method - Batch effects
# 2. Batch effect correction: ComBat method
# 3. Differentially methylated regions (DMRs): Bumphunter or ProbeLasso Algorithms 
# 4. Cell heterogeneity problem: RefFreeEWAS (Houseman, 2014) & RefbaseEWAS (Houseman, 2012)
# 5. copy number alterations

source("http://bioconductor.org/biocLite.R")
biocLite()
biocLite(c('minfi', 
           'DNAcopy', 
           'impute', 
           'marray', 
           'limma',
           'preprocessCore',
           'RPMM', 
           'sva', 
           'IlluminaHumanMethylation450kmanifest', 
           'IlluminaHumanMethylationEPICmanifest',
           'wateRmelon',
           'isva',
           'quadprog',
           'bumphunter',
           'doParallel',
           'qvalue',
           'RefFreeEWAS',
           'GenomicRanges',
           'plyr'))

biocLite(c('FEM', 'DMRcate'))

# STEP 1
library("ChAMP")
# RnBeads 

# data()
# library("ChAMPdata")
setwd("/home/adinasarapu")
#setwd("/Users/adinasa/eicc/ASM11398-16580/combined")
#getwd()

# install.packages("doParallel")
# library("doParallel")
# detectCores()

# STEP 2
# The load function saves 3 quality control images (see Figures 3, 5 and 6. 
# The clustering image will not be saved if there are more than 65 samples in the dataset.
#myLoad <- champ.load(directory=getwd(),arraytype="EPIC")
#save(myLoad, file = "ChAMPload.RData")
# load("ChAMPload.RData")

# myLoad$pd
# CpG.GUI(CpG=rownames(myLoad$beta),arraytype="EPIC")

# STEP 3
# myCNA <- champ.CNA(arraytype="EPIC")
# save(myCNA, file = "ChAMPcna.RData")

# STEP 4
myNorm <- champ.norm(plotBMIQ = TRUE, arraytype="EPIC", QCimages = TRUE, cores=5)
save(myNorm, file = "ChAMPnorm.RData")
# load("ChAMPnorm.RData")

mySVD <- champ.SVD()
save(mySVD, file = "ChAMPsvd.RData")

# If Batch detected, run champ.runCombat() here.
batchNorm <- champ.runCombat()
save(batchNorm, file = "ChAMPbatchNorm.RData")

limma <- champ.MVP(arraytype="EPIC")
save(myLimma, file = "ChAMPlimma.RData")

myDMR <- champ.DMR(arraytype="EPIC")
save(myDMR, file = "ChAMPdmr.RData")



champ.load(directory = getwd(), 
           methValue = "B", 
           resultsDir = paste(getwd(),"resultsChamp", sep = "/"), 
           filterXY = TRUE, 
           QCimages = TRUE, 
           filterDetP = TRUE, 
           detPcut = 0.01,  
           removeDetP = 0, 
           filterBeads=TRUE, 
           beadCutoff=0.05, 
           filterNoCG=FALSE,
           filterSNPs=TRUE,
           filterMultiHit=TRUE,
           arraytype="EPIC")

champ.norm(beta = myLoad$beta, 
           rgSet = myLoad$rgSet, 
           pd = myLoad$pd, 
           mset = myLoad$mset, 
           sampleSheet = "sampleSheet.txt", 
           resultsDir = paste(getwd(), "resultsChamp", sep = "/"), 
           methValue = "B", 
           fromIDAT = TRUE, 
           norm = "BMIQ", 
           fromFile = FALSE, 
           betaFile, 
           filter = TRUE, 
           filterXY = TRUE, 
           QCimages = TRUE, 
           plotBMIQ = FALSE,
           arraytype="450K")

champ.process(fromIDAT = TRUE, 
              fromFile = FALSE, 
              directory = getwd(), 
              resultsDir = paste(getwd(), "resultsChamp", sep = "/"), 
              methValue = "B", 
              filterDetP = TRUE, 
              detPcut = 0.01, 
              filterXY = TRUE, 
              removeDetP = 0, 
              filterBeads = TRUE, 
              beadCutoff = 0.05, 
              filterNoCG = FALSE, 
              QCimages = TRUE, 
              batchCorrect = TRUE, 
              runSVD = TRUE, 
              studyInfo = FALSE, 
              infoFactor = c(), 
              norm = "BMIQ", 
              adjust.method = "BH", 
              adjPVal = 0.05, 
              runDMR = TRUE,  
              runCNA = TRUE, 
              plotBMIQ = FALSE, 
              DMRpval = 0.05,
              sampleCNA=TRUE,
              plotSample = TRUE,
              groupFreqPlots=TRUE,
              freqThreshold=0.3, 
              bedFile = FALSE, 
              methProfile = FALSE, 
              controlProfile = FALSE,
              arraytype="EPIC")

head(myLoad$detP)

myNorm <- champ.norm()
champ.SVD()
batchNorm <- champ.runCombat()
limma <- champ.MVP()
myDMR <- champ.DMR()
myRefBase <- champ.refbase()
myRefFree <- champ.reffree()
champ.CNA()

??champ.load

