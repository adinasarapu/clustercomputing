#!/bin/sh

echo "Start - `date`" 
#$ -N step4
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email>@emory.edu

source activate qiime2-2018.2

EXPERIMENT="LHA11561"
FEATURE_TABLE="filtered-table.qza"

PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_2
DATA_DIR1=$QIIME2_DIR/plate_merged_20_260
DATA_DIR2=$QIIME2_DIR/data_filtered_20_260

OUT_DIR=$QIIME2_DIR/data_diversity_20_260

export TMPDIR=$HOME/tmp

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

cp $DATA_DIR1/merged_seqs.qza $TMP_DIR/representative_sequences.qza
cp $DATA_DIR2/$FEATURE_TABLE $TMP_DIR/
cp $HOME/gg_13_8_otus/rep_set/85_otus.fasta $TMP_DIR/
cp $HOME/gg_13_8_otus/taxonomy/85_otu_taxonomy.txt $TMP_DIR/
cp $PROJ_DIR/${EXPERIMENT}.txt $TMP_DIR/meta_file.txt

/bin/mkdir -p $TMP_DIR/{phylogeny,taxonomy,alpha_diversity,beta_diversity,composition}

qiime tools import \
 --type 'FeatureData[Sequence]' \
 --input-path $TMP_DIR/85_otus.fasta \
 --output-path $TMP_DIR/taxonomy/85_otus.qza

qiime vsearch cluster-features-closed-reference \
 --i-sequences $TMP_DIR/representative_sequences.qza \
 --i-table $TMP_DIR/filtered-table.qza \
 --i-reference-sequences $TMP_DIR/taxonomy/85_otus.qza \
 --p-strand both \
 --p-threads 8 \
 --p-perc-identity 0.85 \
 --o-clustered-table table-cr-85.qza \
 --o-clustered-sequences table-cr-85.qza \
 --o-unmatched-sequences unmatched.qza

# Column name from meta_file.txt
META_DATA_CATEGORY="VID"

# SAMPLING_DEPTH for rarefaction
DEPTH_MIN=1
DEPTH_MAX=25000
DEPTH_STEPS=1000

ls -la $TMP_DIR/

/bin/mkdir -p $TMP_DIR/{phylogeny,taxonomy,alpha_diversity,beta_diversity,composition}

############################################
# Tree for phylogenetic diversity analyses #
############################################

# Perform de novo multiple sequence alignment using MAFFT
qiime alignment mafft \
	--i-sequences $TMP_DIR/representative_sequences.qza \
	--p-n-threads -1 \
	--o-alignment $TMP_DIR/phylogeny/aligned-rep-seqs.qza

qiime tools export --output-dir $TMP_DIR/phylogeny/export_aligned $TMP_DIR/phylogeny/aligned-rep-seqs.qza

# mask (or filter) the alignment to remove positions that are highly variable. 
# These positions are generally considered to add noise to a resulting phylogenetic tree.
qiime alignment mask \
	--i-alignment $TMP_DIR/phylogeny/aligned-rep-seqs.qza \
	--o-masked-alignment $TMP_DIR/phylogeny/masked-aligned-rep-seqs.qza

qiime tools export --output-dir $TMP_DIR/phylogeny/export_masked $TMP_DIR/phylogeny/masked-aligned-rep-seqs.qza

# apply FastTree to generate a phylogenetic tree from the masked alignment.
qiime phylogeny fasttree \
	--i-alignment $TMP_DIR/phylogeny/masked-aligned-rep-seqs.qza \
	--p-n-threads -1 \
	--o-tree $TMP_DIR/phylogeny/unrooted-tree.qza

# final step in this section we apply midpoint rooting
qiime phylogeny midpoint-root \
	--i-tree $TMP_DIR/phylogeny/unrooted-tree.qza \
	--o-rooted-tree $TMP_DIR/phylogeny/rooted-tree.qza

qiime tools export --output-dir $TMP_DIR/phylogeny/export_rooted $TMP_DIR/phylogeny/rooted-tree.qza

##########################
# Diversity core metrics #
##########################

# Applies a collection of diversity metrics (both phylogenetic and non-
# phylogenetic) to a feature table
qiime diversity core-metrics-phylogenetic \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--p-sampling-depth $DEPTH_MAX \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--output-dir $TMP_DIR/core_metrics_phylogenetic

###################
# Alpha Diversity #
###################

# Computes a user-specified alpha diversity metric for all samples in a feature table
# At the moment you can only supply one metric at a time 

# shannon
qiime diversity alpha \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--p-metric shannon \
	--output-dir $TMP_DIR/alpha_diversity/shannon \
	--verbose

qiime tools export --output-dir $TMP_DIR/alpha_diversity/shannon/export \
	$TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza

# chao1
qiime diversity alpha \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--p-metric chao1 \
	--output-dir $TMP_DIR/alpha_diversity/chao1 \
	--verbose

qiime tools export --output-dir $TMP_DIR/alpha_diversity/chao1/export \
	$TMP_DIR/alpha_diversity/chao1/alpha_diversity.qza

# Visually and statistically compare groups of alpha diversity values
qiime diversity alpha-group-significance \
	--i-alpha-diversity $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_group_significance.qzv \
	--verbose

# Determine whether numeric sample metadata category is correlated with alpha diversity
# The alpha-correlation method correlates an alpha diversity metric with all numeric values in the input mapping file

# https://forum.qiime2.org/t/qiime-diversity-alpha-correlation/1487/4

# qiime diversity alpha-correlation \
#	--i-alpha-diversity $TMP_DIR/alpha_diversity/shannon/alpha_diversity.qza \
#	--m-metadata-file $TMP_DIR/meta_file.txt \
#	--o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_correlation.qzv

# alpha rarefaction curves by computing rarefactions between `min_depth` and `max_depth`
qiime diversity alpha-rarefaction \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--p-max-depth $DEPTH_MAX \
	--p-steps $DEPTH_STEPS \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--p-metrics shannon \
	--o-visualization $TMP_DIR/alpha_diversity/shannon/alpha_rarefaction.qzv

qiime diversity alpha-rarefaction \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--p-max-depth $DEPTH_MAX \
	--p-steps $DEPTH_STEPS \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--p-metrics chao1 \
	--o-visualization $TMP_DIR/alpha_diversity/chao1/alpha_rarefaction.qzv

/bin/mkdir -p $TMP_DIR/alpha_diversity/faith_pd

# Computes a user-specified phylogenetic alpha diversity metric for all samples in a feature table
qiime diversity alpha-phylogenetic \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--p-metric faith_pd \
	--o-alpha-diversity $TMP_DIR/alpha_diversity/faith_pd/alpha_phylogenetic.qza

qiime tools export --output-dir $TMP_DIR/alpha_diversity/faith_pd/export_phylogenetic \
	$TMP_DIR/alpha_diversity/faith_pd/alpha_phylogenetic.qza

##################
# Beta Diversity #
##################

# Computes a user-specified beta diversity metric for all pairs of samples in a feature table.
qiime diversity beta \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--p-metric braycurtis \
	--output-dir $TMP_DIR/beta_diversity/braycurtis \
	--verbose

qiime diversity beta \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--p-metric jaccard \
	--output-dir $TMP_DIR/beta_diversity/jaccard \
	--verbose

# Determine whether groups of samples are significantly different from one
# another using a permutation-based statistical test
qiime diversity beta-group-significance \
	--i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--m-metadata-category $META_DATA_CATEGORY \
	--p-method permanova \
	--p-pairwise \
	--o-visualization $TMP_DIR/beta_diversity/braycurtis/beta_group_significance.qzv

# Apply principal coordinate analysis
qiime diversity pcoa \
	--i-distance-matrix $TMP_DIR/beta_diversity/braycurtis/distance_matrix.qza \
	--o-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza

# Generate visualization of your ordination
qiime emperor plot \
	--i-pcoa $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--o-visualization $TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa_emperor.qzv

qiime tools export --output-dir $TMP_DIR/beta_diversity/braycurtis/export \
	$TMP_DIR/beta_diversity/braycurtis/braycurtis_pcoa.qza

# Repeatedly rarefy a feature table to compare beta diversity results across
# and within rarefaction depths
qiime diversity beta-rarefaction \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--p-metric braycurtis \
	--p-sampling-depth $DEPTH_MAX \
	--p-correlation-method spearman \
	--p-clustering-method nj \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--o-visualization $TMP_DIR/beta_diversity/braycurtis/beta_rarefaction.qzv \
	--verbose

######################
# Taxonomic analysis #
######################
qiime tools import \
	--type 'FeatureData[Sequence]' \
	--input-path $TMP_DIR/85_otus.fasta \
	--output-path $TMP_DIR/taxonomy/85_otus.qza

qiime tools import \
	--type 'FeatureData[Taxonomy]' \
	--source-format HeaderlessTSVTaxonomyFormat \
	--input-path $TMP_DIR/85_otu_taxonomy.txt \
	--output-path $TMP_DIR/taxonomy/ref-taxonomy.qza

# extract reference reads
# --p-f-primer CCTACGGGNGGCWGCAG
# --p-r-primer GACTACHVGGGTATCTAATCC

# 515f PCR Primer: GTGCCAGCMGCCGCGGTAA
# 806r PCR Primer: GGACTACHVGGGTWTCTAAT

# GTGCCAGCMGCCGCGGTAA

#  --p-trunc-len 210 \
qiime feature-classifier extract-reads \
	--i-sequences $TMP_DIR/taxonomy/85_otus.qza \
	--p-f-primer CCTACGGGNGGCWGCAG \
	--p-r-primer GACTACHVGGGTATCTAATCC \
	--o-reads $TMP_DIR/taxonomy/ref-seqs.qza \
	--verbose

# Create a scikit-learn naive_bayes classifier for reads
qiime feature-classifier fit-classifier-naive-bayes \
	--i-reference-reads $TMP_DIR/taxonomy/ref-seqs.qza \
	--i-reference-taxonomy $TMP_DIR/taxonomy/ref-taxonomy.qza \
	--o-classifier $TMP_DIR/taxonomy/classifier.qza \
	--verbose

# Classify reads by taxon using a fitted classifier
qiime feature-classifier classify-sklearn \
	--i-classifier $TMP_DIR/taxonomy/classifier.qza \
	--i-reads $TMP_DIR/representative_sequences.qza \
	--p-n-jobs -1 \
	--o-classification $TMP_DIR/taxonomy/taxonomy.qza \
	--verbose

# Generate a tabular view of Metadata
qiime metadata tabulate \
	--m-input-file $TMP_DIR/taxonomy/taxonomy.qza \
	--o-visualization $TMP_DIR/taxonomy/taxonomy.qzv

# view the taxonomic composition of our samples with interactive bar plots.
qiime taxa barplot \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-taxonomy $TMP_DIR/taxonomy/taxonomy.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--o-visualization $TMP_DIR/taxonomy/taxa-bar-plots.qzv

#=================================#
# Differential abundance analysis #
#=================================#
# Analysis of Composition of Microbiomes (ANCOM)

qiime composition add-pseudocount \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--o-composition-table $TMP_DIR/composition/composition-table.qza

# Apply ANCOM to identify features that are differentially abundant across groups
qiime composition ancom \
	--i-table $TMP_DIR/composition/composition-table.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--p-transform-function log \
	--m-metadata-category $META_DATA_CATEGORY \
	--o-visualization $TMP_DIR/composition/ancom-group.qzv

# differential abundance test at a specific taxonomic level
qiime taxa collapse \
	--i-table $TMP_DIR/$FEATURE_TABLE \
	--i-taxonomy $TMP_DIR/taxonomy/taxonomy.qza \
	--p-level 7 \
	--o-collapsed-table $TMP_DIR/composition/table-l7.qza

qiime composition add-pseudocount \
	--i-table $TMP_DIR/composition/table-l7.qza \
	--o-composition-table $TMP_DIR/composition/composition-table-l7.qza

qiime composition ancom \
	--i-table $TMP_DIR/composition/composition-table-l7.qza \
	--m-metadata-file $TMP_DIR/meta_file.txt \
	--p-transform-function log \
	--m-metadata-category $META_DATA_CATEGORY \
	--o-visualization $TMP_DIR/composition/l7-ancom-group.qzv \
	--verbose

source deactivate qiime2-2018.2

# 2D PCoA plots
source activate qiime1 
make_2d_plots.py \
	-i $TMP_DIR/beta_diversity/braycurtis/export/ordination.txt \
	-m $TMP_DIR/meta_file.txt \
	-o $TMP_DIR/beta_diversity/braycurtis/pcoa_2d
source deactivate qiime1

/bin/rm $TMP_DIR/*filtered-table.qza
/bin/rm $TMP_DIR/*_sequences.qza
/bin/rm $TMP_DIR/85_otus.fasta
/bin/rm $TMP_DIR/85_otu_taxonomy.txt

if [ ! -d $OUT_DIR ]; then
        mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
