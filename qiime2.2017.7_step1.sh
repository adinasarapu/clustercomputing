#!/bin/sh

# Plugin error from demux: Invalid DISPLAY variable
# run the following command  
# echo "backend : agg" > ~/.config/matplotlib/matplotlibrc

echo "Start - `date`" 
#$ -N QIIME2.QC
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 30
#$ -cwd
#$ -j y
#$ -m abe
#$ -M adinasa@emory.edu

# module load Anaconda3/4.2.0
source activate qiime2-2017.7

PROJ_DIR=$HOME/YGO11472
QIIME2_DIR=$PROJ_DIR/qiime2
OUT_DIR=$QIIME2_DIR/out

if [ ! -d ${OUT_DIR} ]; then
	mkdir -p ${OUT_DIR}
fi

# for EICC (Since no /scratch dir at EICC computing nodes  use ...)
export TMPDIR=$HOME/tmp

# for HGCC (creates a unique dir on local drive)
# export TMPDIR=/scratch/tmp 

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

FASTQ_DIR=$PROJ_DIR/data_new

rsync -av $FASTQ_DIR/YGO11472*.fastq.gz $TMP_DIR

# manifest.txt file format
#===============================================================================#
# sample-id,absolute-filepath,direction						#
# EIGC9-001-2,$HOME/PTB_0816/EIGC9-001-2_S37_L001_R1_001.fastq.gz,forward	#
# EIGC9-001-2,$HOME/PTB_0816/EIGC9-001-2_S37_L001_R2_001.fastq.gz,reverse	#
# EIGC9-002-2,$HOME/PTB_0816/EIGC9-002-2_S38_L001_R1_001.fastq.gz,forward	#
# EIGC9-002-2,$HOME/PTB_0816/EIGC9-002-2_S38_L001_R2_001.fastq.gz,reverse	#
#===============================================================================#

echo -e "sample-id,absolute-filepath,direction" \
        >> $TMP_DIR/manifest.txt

for entry in $TMP_DIR/*.fastq.gz
do
        fname=$(basename $entry)
	# YGO11472-0011-D1-2_S11_L001_R2_001.fastq.gz to
	# YGO11472-0011-D1-2
        fbname=${fname%_S*}
        fcount=`ls $TMP_DIR/ | grep $fbname* | wc -l`
        if [[ ${fcount} == 2 ]]; then
		# Don't use underscores (_) in sample-id of manifest file 
                fbname=`echo $fbname | sed 's/_/-/'`
                fpre="$fbname,$TMP_DIR/$fname"
                if [[ $entry == *"_R1_"* ]]; then
			echo -e "$fpre,forward" >> $TMP_DIR/manifest.txt
                fi
                if [[ $entry == *"_R2_"* ]]; then
                        echo -e "$fpre,reverse" >> $TMP_DIR/manifest.txt
                fi
        fi
done

#==========================================#
# Import FASTQ files into QIIME2 Artifact  #
#==========================================#

qiime tools import \
	--type SampleData[PairedEndSequencesWithQuality] \
	--input-path $TMP_DIR/manifest.txt \
	--output-path $TMP_DIR/paired-end-data.qza \
	--source-format PairedEndFastqManifestPhred33

#============================#
# Plot positional qualitites #
#============================#
# demux summarize accepts demultiplexed sequences of type 
#	SampleData[SequencesWithQuality] or SampleData[PairedEndSequencesWithQuality]. 
# This type of data is produced by demux emp-single or demux emp-paired, or 
#	by importing your data in a supported format.

qiime demux summarize \
	--i-data $TMP_DIR/paired-end-data.qza \
	--o-visualization $TMP_DIR/data-qualities.qzv

/bin/rm $TMP_DIR/*.fastq.gz

rsync -av $TMP_DIR/* $OUT_DIR

/bin/rm -rf $TMP_DIR

source deactivate qiime2-2017.7
# module unload Anaconda3/4.2.0

echo "Finish - `date`"
