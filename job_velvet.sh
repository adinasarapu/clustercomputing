#!/bin/sh

#=================================================================#
# The hash length is related to how velveth prepares the dataset, 
# and has a significant impact on how good the final assembly by 
# velvetg will be.
# Therefore it is good to try running your assemblies multiple times 
# using different hash lengths.
#=================================================================#

echo "Start - `date`" 
#$ -N VELVET.29
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 60
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR="$HOME/Projects/WGS_TCA21177"

#set -e 
export OMP_NUM_THREADS=60
export TMPDIR=/tmp

module load velvet/1.2.10
# SL151929-SL1539
for postfix in 929
do
	SID="SL151${postfix}"
	GRP_DIR=${PROJ_DIR}/${SID}
	OUT_DIR="${GRP_DIR}/velvet_2"

	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}
	fi

	# create a unique folder on the local compute drive
	if [ -e /bin/mktemp ]; then
		TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
	elif [ -e /usr/bin/mktemp ]; then
		TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
	else
		echo “Error. Cannot find mktemp to create tmp directory”
		exit
	fi

	R_1=`ls -la ${GRP_DIR}/fastq/*_1_* | awk '{print $9}'`
	R_2=`ls -la ${GRP_DIR}/fastq/*_2_* | awk '{print $9}'`
	
	echo -e "${TMP_DIR} created"

	cp ${R_1} ${TMP_DIR}/
	cp ${R_2} ${TMP_DIR}/
	
	echo -e "${R_1##*/} and\n ${R_2##*/} copied to\n ${TMP_DIR}"

	# Paired-end short reads (using separate files for the paired reads)	
	# -separate	: Read 2 separate files for paired reads
	# directory/Roadmaps
	# directory/Sequences
	
	echo -e "velveth ${TMP_DIR}/hash 51,71,10 \
		-shortPaired -fastq.gz -separate \
		${TMP_DIR}/*_1_*_${SID}.fastq.gz ${TMP_DIR}/*_2_*_${SID}.fastq.gz \
		> ${TMP_DIR}/${SID}.stdOut"

	velveth ${TMP_DIR}/hash 51,71,10 \
		-shortPaired -fastq.gz -separate \
		${TMP_DIR}/*_1_*_${SID}.fastq.gz ${TMP_DIR}/*_2_*_${SID}.fastq.gz \
		> ${TMP_DIR}/${SID}.stdOut

	echo -e "removing ${TMP_DIR}/*_${SID}.fastq.gz"
	/bin/rm ${TMP_DIR}/*_${SID}.fastq.gz

	# contigs.fa
	# stats.txt
	# LastGraph
	# velvet_asm.afg
	for((n=51; n<71; n=n+10)); do 
		velvetg ${TMP_DIR}/hash_"$n" \
			-unused_reads yes \
			-exp_cov auto;
	done

	# copy your local data to your user directory
	echo -e "rsync -av ${TMP_DIR}/ ${OUT_DIR}"
	rsync -av ${TMP_DIR}/ ${OUT_DIR}

	# remove the temp directory
	echo -e "removing ${TMP_DIR}"
	/bin/rm -fr ${TMP_DIR}
done
module unload velvet/1.2.10
# http://hpc.ilri.cgiar.org/beca/training/AdvancedBFX2015/NGS_Computer_Laboratory.pdf
