#!/bin/sh

echo "Start - `date`" 
#$ -N step2
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email>@emory.edu

# makes the job dependent on the 1st job completion
# qsub -hold_jid <job id> ../script/qiime2.2017.8_step1.sh

source activate qiime2-2018.2

PROJ_DIR=$HOME/microbiome/LHA11561

# sed -i 's/\_/\-/g' Plate1.txt
META_FILE="LHA11561_Plate2.txt"
META_DATA_CATEGORY="CUID"

QIIME2_DIR=$PROJ_DIR/qiime2_2018_2
DATA_DIR=$QIIME2_DIR/plate2_data_imported
OUT_DIR=$QIIME2_DIR/plate2_denoised

export TMPDIR=$HOME/tmp

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

cp $DATA_DIR/paired-end-data.qza $TMP_DIR/
cp $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

if [ ! -d $OUT_DIR ]; then
 mkdir -p $OUT_DIR
fi

# PHRED-based filtering and trimming
# Summary statistics of the filtering process

# q2-quality-filter shouldn't be a necessary step since DADA2 will perform its own quality control
QUAL_FILT=false

# A feature is essentially any unit of observation, e.g., an OTU, a sequence variant, a gene or  a metabolite
FEATURE_DATA=true
OTU_DATA=true

if [ "$QUAL_FILT" = true ]; then
 qiime quality-filter q-score \
  --i-demux $TMP_DIR/paired-end-data.qza \
  --p-min-quality 10 \
  --output-dir $TMP_DIR/qscore_min10_filtered

 qiime tools export $TMP_DIR/qscore_min10_filtered/filter_stats.qza \
  --output-dir $TMP_DIR/qscore_min10_filtered/quality_stats
fi

chmod a+trwx $TMPDIR

#=======================================#
# Denoise paired-end sequences		#
# Dereplicate, and filter chimeras	#
#=======================================#
# Chimeras are a product of the unwanted combination of 2 or more sequences
# "pooled": All reads are pooled prior to chimera detection [default: consensus]
# --p-n-threads The number of threads to use for multithreaded processing. 
#		If 0 is provided, all available cores will be used.

if [ "$FEATURE_DATA" = true ]; then

 /usr/bin/mkdir -p $TMP_DIR/dada2

 for s in 22; do
  for e in 240 250 260; do

   DADA_DIR=$TMP_DIR/dada2/trim_${s}_${e}

   qiime dada2 denoise-paired \
    --i-demultiplexed-seqs $TMP_DIR/paired-end-data.qza \
    --output-dir $DADA_DIR \
    --p-n-threads 0 \
    --p-chimera-method pooled \
    --p-trim-left-f $s \
    --p-trim-left-r $s \
    --p-trunc-len-f $e \
    --p-trunc-len-r $e

   # features observed in a user-defined fraction of the samples 
   qiime feature-table core-features \
    --i-table $DADA_DIR/table.qza \
    --o-visualization $DADA_DIR/table_core_features.qzv

   # Summarize table
   qiime feature-table summarize \
    --i-table $DADA_DIR/table.qza \
    --o-visualization $DADA_DIR/table_summarize.qzv \
    --m-sample-metadata-file $TMP_DIR/meta_file.txt

   # View sequence associated with each feature
   qiime feature-table tabulate-seqs \
    --i-data $DADA_DIR/representative_sequences.qza \
    --o-visualization $DADA_DIR/representative_sequences.qzv

  done
 done
 rsync -av $DADA_DIR $OUT_DIR
fi

# Join paired-end sequence reads using vsearch's merge_pairs function.

if [ "$OTU_DATA" = true ]; then 
 
 /usr/bin/mkdir -p $TMP_DIR/vsearch

 for ovlen in 10 15 20; do

  VSEARCH_DIR=$TMP_DIR/vsearch/ovlen_${ovlen}
  /usr/bin/mkdir -p $VSEARCH_DIR

  qiime vsearch join-pairs \
   --p-minovlen $ovlen \
   --i-demultiplexed-seqs $TMP_DIR/paired-end-data.qza \
   --o-joined-sequences $VSEARCH_DIR/vsearch_joined.qza

  # Visualize joined paired-end seqs
  qiime demux summarize \
   --i-data $VSEARCH_DIR/vsearch_joined.qza \
   --o-visualization $VSEARCH_DIR/vsearch_joined.qzv
  
  # Filter joined sequence based on quality scores and the presence of ambiguous base calls
  qiime quality-filter q-score-joined \
   --i-demux $VSEARCH_DIR/vsearch_joined.qza \
   --o-filtered-sequences $VSEARCH_DIR/vsearch_joined_filtered.qza \
   --o-filter-stats $VSEARCH_DIR/vsearch_joined_filter_stats.qza

  qiime tools export $VSEARCH_DIR/vsearch_joined_filter_stats.qza \
   --output-dir $VSEARCH_DIR/joined_filter_stats
 
  # Dereplicate sequence data and create a feature table and feature representative sequences.
  qiime vsearch dereplicate-sequences \
   --i-sequences $VSEARCH_DIR/vsearch_joined_filtered.qza \
   --o-dereplicated-table $VSEARCH_DIR/vsearch_joined_filtered_table.qza \
   --o-dereplicated-sequences $VSEARCH_DIR/vsearch_joined_filtered_rep_seqs.qza

  # Summarize table
  qiime feature-table summarize \
   --i-table $VSEARCH_DIR/vsearch_joined_filtered_table.qza \
   --o-visualization $VSEARCH_DIR/vsearch_joined_filtered_table_summarize.qzv \
   --m-sample-metadata-file $TMP_DIR/meta_file.txt

  # View sequence associated with each OTU
  qiime feature-table tabulate-seqs \
   --i-data $VSEARCH_DIR/vsearch_joined_filtered_rep_seqs.qza \
   --o-visualization $VSEARCH_DIR/representative_sequences.qzv
  
 done
 rsync -av $TMP_DIR/vsearch $OUT_DIR
fi

/bin/rm $TMP_DIR/paired-end-data.qza
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.2
echo "Finish - `date`"
