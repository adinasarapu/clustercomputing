#!/bin/sh

# Will remove the chr to the VCF with chr.

# files=`ls -la ${HOME}/Projects/WGS_TCA21177/DBA/*.vcf.gz | awk '{print $9}'`
# echo $files
# awk '{gsub(/^chr/,""); print}' your.vcf > no_chr.vcf

for i in ${HOME}/Projects/WGS_TCA21177/DBA/*dbSNP142.vcf.gz; do
	in=$(echo "$i")
	out=$(echo $i | sed 's/\.vcf.gz$/.chr.vcf.gz/'); 
	echo $in $out
	zcat $in | \
		awk '{gsub(/^chr/,""); print}' $in | \
		gzip > $out   
done
