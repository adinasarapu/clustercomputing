#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N mpileup2
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one
## computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=220:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 40
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR="$HOME/Projects/TCA21192"
GENES_GTF="$HOME/iGenomes/Mus_musculus/UCSC/mm10/Annotation/Genes/genes.gtf"
REF_FILE="$HOME/iGenomes/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa"
BAM_LIST="${PROJ_DIR}/script/bam_file_list.txt" 

#module load picardtools/2.0.1
module load samtools/1.3
module load bcftools/1.3
module load bedtools/2.25.0

OUT_DIR="${PROJ_DIR}/Out/JointCall/genotype"

if [ ! -d ${OUT_DIR} ]; then
	mkdir -p ${OUT_DIR}
fi

var=""
for postfix in 889 890 891 892 893 894 895 896 897 898 899 900 901 902
do
	MAP_DIR="${PROJ_DIR}/Out/SL151${postfix}/star_mapping"
	BAM_FILE=${MAP_DIR}/Aligned.sortedByCoord
	# Mark duplicates with Picard
	# This creates a sorted BAM file called xxxx.dedupe.bam with the same 
	# content as the input file, except that any duplicate reads are marked 
	# as such. It also produces a metrics file called xxxx.metrics
	$PICARD MarkDuplicates MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
		METRICS_FILE=${OUT_DIR}/SL151${postfix}.metrics \
		REMOVE_DUPLICATES=true \
		ASSUME_SORTED=true \
		VALIDATION_STRINGENCY=LENIENT \
		INPUT=${BAM_FILE}.out.bam \
		OUTPUT=${BAM_FILE}.dedupe.bam
		
	# bam indexing
	samtools index ${BAM_FILE}.dedupe.bam
	var="$var ${BAM_FILE}.dedupe.bam"
done
echo $var

# call SNPs simultaneously on all samples and output a multi-sample VCF
# if one of the samples has a variant, then the calls for all the other
# samples will be emitted too, even if it's reference or low quality.

# Call SNPs using samtools (mpileup: multi-way pileup)
# to limit pileup to regions use (samtools mpileup -I regions.bed ...)
# '-' is used samtools/bcftools to tell the program to take the input
# from pipe; '-' = /dev/stdin
# BCF, the binary variant call format
# --skip-indels (report only SNPs)

#-o, --output FILE       write output to FILE [standard output]
#-g, --BCF               generate genotype likelihoods in BCF format
#-v, --VCF               generate genotype likelihoods in VCF format

samtools mpileup \
	-go ${OUT_DIR}/genotype.bcf \
	-uf ${REF_FILE} \
	-b ${BAM_LIST}

# BCFtools call
# Index the BCF file with tabix, then use bacftools call to call SNPs
tabix ${OUT_DIR}/genotype.bcf

# --skip-variants indels (report only SNPs)
bcftools call -vm -Oz \
	--skip-variants indels \
	-o ${OUT_DIR}/genotype.vcf.gz \
	${OUT_DIR}/genotype.bcf

tabix -p vcf ${OUT_DIR}/genotype.vcf.gz

#module unload picardtools/2.0.1
module unload samtools/1.3
module unload bcftools/1.3
module unload bedtools/2.25.0
