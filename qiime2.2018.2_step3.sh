#!/bin/sh

echo "Start - `date`" 
#$ -N step3
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 2
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email>@emory.edu

source activate qiime2-2018.2

EXPERIMENT="LHA11561"

# SAMPLE_METADATA
META_FILE=${EXPERIMENT}.txt
META_DATA_COLUMN="VisitID" 

PROJ_DIR=$HOME/microbiome/$EXPERIMENT
QIIME2_DIR=$PROJ_DIR/qiime2_2018_2
DATA_DIR=$QIIME2_DIR/plates_merged/dada2/trim_20_240
OUT_DIR=$QIIME2_DIR/data_filtered/dada2/trim_20_240

export TMPDIR=$HOME/tmp

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

rsync -av $DATA_DIR/*.qza  $TMP_DIR/
rsync -av $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

######################################################################
# Filter (i.e., remove) samples and/or features from a feature table #
######################################################################

frequency_based_filtering=true
tot_abundance_filtering=false
sample_contingency_filtered=true
sample_subset=true

#===================#
# Feature filtering #
#===================#
#Filter features from table based on frequency and/or metadata.

# Remove features with a total abundance (summed across all samples) of < 10 as follows
if [ "$tot_abundance_filtering" = true ]; then
 qiime feature-table filter-features \
  --i-table $TMP_DIR/merged_table.qza \
  --p-min-frequency 10 \
  --o-filtered-table $TMP_DIR/feature-frequency-features-table.qza \
  --verbose
fi

# Contingency-based filtering
# Features that are present in only a single sample could be filtered from a feature table as follows
# Removes singletons
if [ "$sample_contingency_filtered" = true ]; then
  qiime feature-table filter-features \
   --i-table $TMP_DIR/merged_table.qza \
   --p-min-samples 2 \
   --o-filtered-table $TMP_DIR/singletons-filtered-table.qza
  # Summarize table
  qiime feature-table summarize \
   --i-table $TMP_DIR/singletons-filtered-table.qza \
   --o-visualization $TMP_DIR/singletons-filtered-table.qzv \
   --m-sample-metadata-file $TMP_DIR/meta_file.txt
fi

#===================#
# Sample filtering  #
#===================#
# Filter samples from table based on frequency and/or metadata
if [ "$frequency_based_filtering" = true ]; then
  qiime feature-table filter-samples \
   --i-table $TMP_DIR/singletons-filtered-table.qza \
   --p-min-frequency 25000 \
   --o-filtered-table $TMP_DIR/feature-frequency-filtered-table.qza
fi

# Sample subset/# Filter samples from table based on metadata
if [ "$sample_subset" = true ]; then
 qiime feature-table filter-samples \
  --i-table $TMP_DIR/feature-frequency-filtered-table.qza \
  --m-metadata-file $TMP_DIR/meta_file.txt \
  --p-where "Experiment='$EXPERIMENT'" \
  --o-filtered-table $TMP_DIR/sample-subset-filtered-table.qza
fi

#===========================#
# Post-filter table summary #
#===========================#
qiime feature-table summarize \
 --i-table $TMP_DIR/sample-subset-filtered-table.qza \
 --o-visualization $TMP_DIR/sample-subset-filtered-table.qzv \
 --m-sample-metadata-file $TMP_DIR/meta_file.txt \
 --verbose

qiime feature-table heatmap \
 --i-table $TMP_DIR/sample-subset-filtered-table.qza \
 --m-metadata-file $TMP_DIR/meta_file.txt \
 --p-metric euclidean \
 --p-cluster both \
 --p-method average \
 --m-metadata-column $META_DATA_COLUMN \
 --o-visualization $TMP_DIR/sample-subset-filtered-table_heatmap.qzv

#=================================#
# Export QIIME2 Artifact to BIOM  #
#=================================#

# OUTPUT: feature-table.biom
BIOM_EXP=$TMP_DIR/biom

qiime tools export $TMP_DIR/sample-subset-filtered-table.qza \
 --output-dir $BIOM_EXP

##########################
# Summarize a BIOM table #
##########################
biom summarize-table \
 -i $BIOM_EXP/feature-table.biom > $BIOM_EXP/biom_summary.txt

cat $TMP_DIR/meta_file.txt

biom add-metadata \
 -i $BIOM_EXP/feature-table.biom \
 -o $BIOM_EXP/feature-table-mdata.biom \
 -m $TMP_DIR/meta_file.txt

biom summarize-table \
 -i $BIOM_EXP/feature-table-mdata.biom > $BIOM_EXP/biom_mdata_summary.txt

##################################################
# Normalizing a BIOM table to relative abundnace #
##################################################
biom_normalize=false
if [ "$biom_normalize" = true ]; then
 biom normalize-table \
  -i $BIOM_EXP/feature-table.biom \
  -r \
  -o $BIOM_EXP/normalized_table.biom

 biom add-metadata \
  -i $BIOM_EXP/normalized_table.biom \
  -o $BIOM_EXP/normalized_table_mdata.biom \
  -m $TMP_DIR/meta_file.txt

 biom convert \
  -i $BIOM_EXP/normalized_table_mdata.biom \
  -o $BIOM_EXP/normalized_table_mdata.json \
  --table-type="OTU table" \
  --to-json

 biom convert \
  -i $BIOM_EXP/normalized_table_mdata.biom \
  -o $BIOM_EXP/normalized_table_mdata.tsv \
  --table-type="OTU table" \
  --to-tsv
fi

source deactivate qiime2-2018.2

/bin/rm $TMP_DIR/merged*

if [ ! -d $OUT_DIR ]; then
        mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

echo "Finish - `date`"
