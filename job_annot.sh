#!/bin/sh
echo "Start - `date`"
#$ -N SNP_Ann
#$ -q all.q
#$ -l h_rt=200:00:00
#$ -pe smp 10
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR="$HOME/Projects/TCA21192"
VCF_DIR="$PROJ_DIR/vcf"

# To Do: need snpEff 4.3 installation

#module load snpEff/4.2
module load annovar/201602
# SL151893_s8.vcf.gz
for i in 889 890 891 892 893 894 895 896 897 898 899 900 901 902 
do
	OUT_DIR=${PROJ_DIR}/annotation/SL151${i}
	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}/{annovar,snpEff}
	fi
	for j in s6 s7 s8
	do
		VCF_FILE="${VCF_DIR}/SL151${i}_${j}.vcf.gz"
		#Annotate a VCF with snpEff
	#	$SNPEFF -v \
	#		-stats ${OUT_DIR}/snpEff/SL151${i}_${j}.mm10.snpEff.html mm10 \
	#		${VCF_FILE} > ${OUT_DIR}/snpEff/SL151${i}_${j}.mm10.snpEff.vcf
		# Annotate a VCF with annovar
		convert2annovar.pl \
			-format vcf4 ${VCF_FILE} \
			>${OUT_DIR}/annovar/SL151${i}_${j}.mm10.avinput
		annotate_variation.pl \
			-out ${OUT_DIR}/annovar/SL151${i}_${j}.mm10 \
			-build mm10 \
			${OUT_DIR}/annovar/SL151${i}_${j}.mm10.avinput \
			/sw/eicc/Pkgs/annovar/201602/bin/mousedb/ \
			-dbtype refGene
	done
done
#module unload snpEff/4.2
module unload annovar/201602
