#!/bin/sh

# PECaller, a genotype-calling model
# PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).
# https://github.com/wingolab-org/pecaller.git

# SGE
MAX_THREADS=60
QNAME=all.q
PE=smp
E_MAIL="ashok.reddy.dinasarapu@emory.edu"

# PROJECT
PROJ_DIR=${HOME}/zwick_rare
PROJ_ID=TYEP0004

PED=${PROJ_DIR}/${PROJ_ID}/${PROJ_ID}_calling.ped

# 59 TYE + 3 PROJ genomes (pileup.gz & indel.txt.gz)
TYEcalldirectory=${PROJ_DIR}/TYEcalldirectory
MAP_DIR=${PROJ_DIR}/${PROJ_ID}/MappingResults

TYE_DIR=${PROJ_DIR}/${PROJ_ID}/TYEDirectory
CALL_DIR=${PROJ_DIR}/${PROJ_ID}/CallingResults

# PECaller
SDX=${PROJ_DIR}/hg38/hg38.sdx
BED=${PROJ_DIR}/hg38/hg38_unique.bed
CALLER=${PROJ_DIR}/script/pecaller
Prob_to_call=0.95 

# measure of variation, counts the number of heterozygotes
# called per sample per base
theta=0.001 

haploids=n
use_pedfile=y 
guide_file_bed_format=${PROJ_DIR}/hg38/hg38_unique.bed

if [ ! -d "$TYE_DIR" ]; then
        mkdir -p "$TYE_DIR"
	ln -s ${TYEcalldirectory}/* ${TYE_DIR}/
	for i in {5..7}
	do
		ln -s "${MAP_DIR}/SL15631${i}/SL15631${i}.indel.txt.gz" ${TYE_DIR}/SL15631${i}.indel.txt.gz
		ln -s "${MAP_DIR}/SL15631${i}/SL15631${i}.pileup.gz" ${TYE_DIR}/SL15631${i}.pileup.gz
	done
fi

if [ ! -d "$CALL_DIR" ]; then
	mkdir -p "$CALL_DIR"
fi

tot_files=`ls -l ${TYE_DIR}/*.pileup.gz | wc -l`
filename=`basename ${TYE_DIR}`

echo "#!/bin/sh
echo \"Start - \`date\`\"
${CALLER} pileup ${SDX} ${tot_files} ${filename} ${Prob_to_call} ${theta} ${haploids} ${MAX_THREADS} ${use_pedfile} ${PED} 1e-8 ${guide_file_bed_format}
echo \"Finish - \`date\`\"" >> ${TYE_DIR}/${PROJ_ID}_call.sh

cd ${TYE_DIR}

qsub -v USER -v PATH -cwd -q ${QNAME} -pe ${PE} ${MAX_THREADS} -M ${E_MAIL} -N ${PROJ_ID} -m abe -j y ${PROJ_ID}_call.sh
