#!/bin/sh

# PECaller, a genotype-calling model
# PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).
# https://github.com/wingolab-org/pecaller.git

# SGE
MAX_THREADS=30
QNAME=b.q
PE=smp
E_MAIL="xyz@emory.edu"

# PROJECT
PROJ_DIR=$HOME/zwick_rare
PROJ_ID=TYEP0006

SAMPLES=$PROJ_DIR/$PROJ_ID/sids.txt

PED=$PROJ_DIR/$PROJ_ID/${PROJ_ID}.ped

# 59 TYE + 2 PROJ genomes (pileup.gz & indel.txt.gz)
TYEcalldirectory=$PROJ_DIR/TYEcalldirectory
MAP_DIR=$PROJ_DIR/$PROJ_ID/MappingResults

TYE_DIR=$PROJ_DIR/$PROJ_ID/TYEDirectory
CALL_DIR=$PROJ_DIR/$PROJ_ID/CallingResults

# PECaller
SDX=$PROJ_DIR/hg38/hg38.sdx
# BED=${PROJ_DIR}/hg38/hg38_unique.bed
CALLER=$PROJ_DIR/script/pecaller
Prob_to_call=0.95 

# measure of variation, counts the number of heterozygotes
# called per sample per base
theta=0.001

haploids=n
use_pedfile=y 
guide_file_bed_format=$PROJ_DIR/hg38/hg38_unique.bed

if [ ! -d "$CALL_DIR" ]; then
	mkdir -p "$CALL_DIR"
fi

my_var=$(cat "$SAMPLES")

if [ ! -d "$TYE_DIR" ]; then
        mkdir -p "$TYE_DIR"
	ln -s $TYEcalldirectory/* $TYE_DIR/
	for ID in ${my_var[@]}; do
		ln -s "$MAP_DIR/$ID/${ID}.indel.txt.gz" $TYE_DIR/${ID}.indel.txt.gz
		ln -s "$MAP_DIR/$ID/${ID}.pileup.gz" $TYE_DIR/${ID}.pileup.gz
	done
fi

tot_files=`ls -l $TYE_DIR/*.pileup.gz | wc -l`
filename=`basename $TYE_DIR`

# if use_pedfile=n, remove $PED
# ${CALLER} pileup ${SDX} ${tot_files} ${filename} ${Prob_to_call} ${theta} ${haploids} ${MAX_THREADS} ${use_pedfile} ${PED} 1e-8 ${guide_file_bed_format}

echo "#!/bin/sh
echo \"Start - \`date\`\"
$CALLER pileup \
	$SDX \
	$tot_files \
	$filename \
	$Prob_to_call \
	$theta \
	$haploids \
	$MAX_THREADS \
	$use_pedfile \
	$PED \
	1e-8 \
	$guide_file_bed_format
echo \"Finish - \`date\`\"" >> $TYE_DIR/${PROJ_ID}_call.sh

cd $TYE_DIR

qsub -v USER -v PATH -cwd -q $QNAME -pe $PE $MAX_THREADS -M $E_MAIL -N $PROJ_ID -m abe -j y ${PROJ_ID}_call.sh

# Once you submit PECaller script, then
# Update job_merger.sh (qsub -hold_jid $PROJ_ID ...) 
# qsub -hold_jid $PROJ_ID ../script/job_merger.sh
