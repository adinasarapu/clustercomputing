#!/bin/sh
# join -j 1 <(sort SL151*_s*_basename.counts) | head
files=$(ls ../htseq_count/SL151*_s*_basename.counts)

arr=($files)
# get length of an array
tLen=${#arr[@]}

colnames="GeneSymbol"
file1=$(basename ${arr[0]})
colnames+="\t${file1%_*}"
file2=$(basename ${arr[1]})
colnames+="\t${file2%_*}"

# Note that the array indices are counted 0,1,..
# join file1 file2 | join - file3 > output
run_cmd="join -t $'\t' ${arr[0]} ${arr[1]}"
for (( i=2; i<${tLen}; i++ ))
do
	run_cmd+=" | join - -t $'\t' ${arr[$i]}"
	filex=$(basename ${arr[$i]})
	colnames+="\t${filex%_*}"
done
echo $colnames
run_cmd="${run_cmd} > count_merged.txt"
eval $run_cmd
echo -e $colnames | cat - count_merged.txt > count_merged_header.txt
