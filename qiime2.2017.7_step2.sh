#!/bin/sh

echo "Start - `date`" 
#$ -N QIIME2.BIOM
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 30
#$ -cwd
#$ -j y
#$ -m abe
#$ -M adinasa@emory.edu

# module load Anaconda3/4.2.0
source activate qiime2-2017.7

PROJ_DIR=$HOME/YGO11472
QIIME2_DIR=$PROJ_DIR/qiime2
DATA_DIR=$QIIME2_DIR/out

# for EICC (Since no /scratch dir at EICC computing nodes  use ...)
export TMPDIR=$HOME/tmp

# for HGCC (creates a unique dir on local drive)
# export TMPDIR=/scratch/tmp 

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

cp $DATA_DIR/paired-end-data.qza $TMP_DIR/

#=======================================#
# denoise paired-end sequences		#
# dereplicate, and filter chimeras	#
#=======================================#
# Run the following steps after looking quality plots
# OUTPUT: table.qza and representative_sequences.qza
# Value for --p-n-threads depends on # of slots/cores available (-pe smp 30)

qiime dada2 denoise-paired \
	--i-demultiplexed-seqs $TMP_DIR/paired-end-data.qza \
	--output-dir $TMP_DIR/table-dada2 \
	--p-n-threads 28 \
	--p-chimera-method pooled \
	--p-trim-left-f 20 \
	--p-trim-left-r 20 \
	--p-trunc-len-f 220 \
	--p-trunc-len-r 220 \
	--verbose

#=======================================#
# Export data from a QIIME2 Artifact	#
#=======================================#
# OUTPUT: feature-table.biom
BIOM_EXP=$TMP_DIR/table-dada2/exported-feature-table

qiime tools export \
	$TMP_DIR/table-dada2/table.qza \
	--output-dir $BIOM_EXP

# pip install biom-format python package
# /home/adinasarapu/YGO11472/sample_map_group_corrected.txt
# Summarize a BIOM table
biom summarize-table \
	-i $BIOM_EXP/feature-table.biom > $BIOM_EXP/biom_summary.txt

# Normalizing a BIOM table to relative abundnace
biom normalize-table \
	-i $BIOM_EXP/feature-table.biom \
	-r \
	-o $BIOM_EXP/normalized_table.biom

biom convert \
        -i $BIOM_EXP/normalized_table.biom \
        -o $BIOM_EXP/normalized_table.json \
        --table-type="OTU table" \
        --to-json

biom convert \
	-i $BIOM_EXP/normalized_table.biom \
	-o $BIOM_EXP/normalized_table.tsv \
	--table-type="OTU table" \
	--to-tsv

biom add-metadata \
	-i $BIOM_EXP/normalized_table.biom \
	-o $BIOM_EXP/normalized_table_mdata.biom \
	-m $PROJ_DIR/sample_map_group_corrected.txt

/bin/rm $TMP_DIR/paired-end-data.qza

rsync -av $TMP_DIR/* $DATA_DIR
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2017.7
# module unload Anaconda3/4.2.0
echo "Finish - `date`"
