#!/bin/sh

echo "Start - `date`"
#$ -r n
#$ -N VCF.isec
#$ -q all.q
#$ -l h_rt=100:00:00
#$ -pe smp 20
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# Create intersections, unions and complements of VCF files.

PROJ_DIR=${HOME}/Projects/WGS_TCA21177

module load bcftools/1.3
module load vcftools
module load samtools

for i in {929..939}; do

        SID="SL151${i}"

        VCF1="${PROJ_DIR}/Tamara/${SID}.dbSNP142.vcf.gz"
	VCF2=${PROJ_DIR}/DBA/DBA_2J.mgp.v5.snps.indels.ver2.dbSNP142.chr.vcf.gz

	OUT_DIR="${PROJ_DIR}/DBA2J_isec/${SID}"

        if [ ! -d ${OUT_DIR} ]; then
                mkdir -p ${OUT_DIR}
        fi

	#===========#
	# vcf-merge #
	#===========#

	vcf-merge ${VCF1} ${VCF2} | \
		bgzip > ${OUT_DIR}/merged.vcf.gz
	bcftools index ${OUT_DIR}/merged.vcf.gz -t

	#===============#
	# VCF intersect	#
	#===============#
	# -w, list of files to write with -p given as 1-based indexes. By default, all files are written
	# --threads <int>           number of extra output compression threads [0]

	# Create intersection and complements of two sets saving the output in dir/*
	# 0000.vcf, 0001.vcf, 0002.vcf, 0003.vcf and README.txt

	bcftools isec ${VCF1} ${VCF2} \
		-p ${OUT_DIR} \
		--threads 20

	bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%SAMPLE=%GT]\n' 0000.vcf -o 0000.tab
	bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%SAMPLE=%GT]\n' 0001.vcf -o 0001.tab
	bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%SAMPLE=%GT]\n' 0002.vcf -o 0002.tab
	bcftools query -f '%CHROM\t%POS\t%REF\t%ALT[\t%SAMPLE=%GT]\n' 0003.vcf -o 0003.tab	
	# Extract and write records from A shared by both A and B using exact allele match
	#bcftools isec ${VCF1} ${VCF2} \
	#	-p ${OUT_DIR} \
	#	-n=2 \
	#	-w1 \
	#	--threads 9
done
 
module unload vcftools
module unload samtools
module unload bcftools/1.3
