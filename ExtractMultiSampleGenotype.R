# https://rdrr.io/bioc/VariantAnnotation/man/readVcf-methods.html
library("VariantAnnotation")
library("data.table")

# setwd("/home/adinasarapu/Projects/TCA21192/eqtl")

#sample_list <- c("SL151899", "SL151900", "SL151901", "SL151902")
sample_list <-
	c("SL151894","SL151895","SL151896","SL151897","SL151898",
	  "SL151889","SL151900","SL151901","SL151902")
vcf_file='/home/adinasarapu/Projects/TCA21192/Out/JointCall/genotype/genotype.vcf.gz'

## Genotype
GT <- readGT(vcf_file)
vcf <- readVcf(vcf_file, "mm10")
	
## Sample Name
hdr <- header(vcf)
full_path <- samples(hdr)
# full_path
full_path <- sub("/home/adinasarapu/Projects/TCA21192/Out/","",full_path)
sample_names <- sub("/star_mapping/Aligned.sortedByCoord.dedupe.bam","",full_path)

colnames(GT) <- sample_names
GT <- GT[,sample_list]
# head(GT)

# SNP - location
res <- genotypeToSnpMatrix(vcf)
X <- t(as(res$genotype, "character"))
r_names <- rownames(X)
M <- matrix(unlist(strsplit(r_names,":|_")), ncol=3, byrow=TRUE)
Y <- as.data.frame(cbind(X,M))
# colnames(Y) <- c("snp","chr","pos","genotype2")
# Y <- Y[,!(names(Y) == "genotype2")]
colnames(Y) <- c(sample_names,"chr","pos","gt")
Y$snp <- row.names(Y)
Y <- Y[,c("snp","chr","pos")]

write.table(GT,
			"/home/adinasarapu/Projects/TCA21192/Out/JointCall/GT_All_Sup_original.txt",
			sep="\t", row.names=T, col.names=NA, quote=FALSE)

## snpMatrix : 
# 0/0 is hom-ref
GT[GT == "0/0"] <- "0"
# 0/1 is het
GT[GT == "0/1"] <- "1"
# 1/1 is hom-var
GT[GT == "1/1"] <- "2"

GT[GT == "."] <- NA
GT[GT == "1/2"] <- NA
GT[GT == "2/2"] <- NA
GT[GT == "0/2"] <- NA

head(GT)
# remove NA containing columns
GT <- GT[complete.cases(GT),]

# Genotype - outfile

Y <- Y[row.names(Y) %in% row.names(GT),]

GT <- setDT(as.data.frame(GT), keep.rownames = TRUE)[]
colnames(GT)[1] <- "id"

write.table(GT,
			"/home/adinasarapu/Projects/TCA21192/Out/JointCall/GT_All_Sup.txt",
			sep="\t", row.names=F, col.names=T, quote=FALSE)
write.table(Y,
			 "/home/adinasarapu/Projects/TCA21192/Out/JointCall/SNP_All_Sup.txt",
			 sep="\t", row.names=F, col.names=T, quote=FALSE)
