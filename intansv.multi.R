library("intansv")

# AIM: 
# SV predictions with low quality filter out and resolve overlapped predictions
# If a SV has both read-pair and split-read support, it is a strong candidate.

# For paired-end reads
# CNVnator is read-depth (RD) approach for predicting SVs
# BreakDancer is a read-pair (RP) approach for predicting SVs
# SVSeq2 and Pindel are split-read (SR) mapping methods to call SVs. 

#############################################################
# https://genome.ucsc.edu/goldenPath/help/hg38.chrom.sizes
chrom.size.file <- "/Users/adinasa/Desktop/Geisert_CNV/hg38.chrom.sizes2.txt"
chrom.size <- read.table(chrom.size.file, header = F, sep ='\t', stringsAsFactors = FALSE)
colnames(chrom.size) <- c("chr","size")
chrom.size <- chrom.size # [!chrom.size$chr %in% c("chrX", "chrY"),]

genome <- GRanges(
  seqnames = Rle(chrom.size$chr),
  ranges = IRanges(start=1,end=chrom.size$size, width=chrom.size$size, names=chrom.size$chr),
  # seqlengths=as.vector(chrom.size$size), 
  seqinfo = Seqinfo(seqnames = chrom.size$chr, seqlengths = chrom.size$size , genome="hg38", isCircular=rep(FALSE,dim(chrom.size)[1]))
)
genome

# gff_file <- "/Users/adinasa/Desktop/Geisert_CNV/gencode.v23.basic.annotation.gff3"
# gencode_gff <- import.gff(gff_file)
# seqinfo(gencode_gff) <- Seqinfo(genome="hg38") 

GRCh38.anno.RData <- "/Users/adinasa/Desktop/Geisert_CNV/GRCh38.anno.RData"
load(GRCh38.anno.RData)
head(GRCh38.anno)

df.meta <- elementMetadata(GRCh38.anno)
df.meta <- unique(df.meta[,c("gene_id","gene_type","gene_status","gene_name")])
df.meta
##############################################################

# Read predictions of different programs
base.dir = "/Users/adinasa/eicc/zwick_rare/geisert"
samples <- c("SL148576","SL148577","SL148578","SL148579")

############
# CNVnator #
############
i = 4
cnv_file = paste(base.dir, paste("/cnvnator_bin_size500/","/out",sep=samples[i]), sep="")
cnvnator <- readCnvnator(cnv_file)
cnvnator$del <- cnvnator$del[-grep("_random|chrUn|EBV|chrM|_alt", cnvnator$del$chromosome), ]
cnvnator$dup <- cnvnator$dup[-grep("_random|chrUn|EBV|chrM|_alt", cnvnator$dup$chromosome), ]
str(cnvnator)

##########
# SVseq2 #
##########
# SVseq2 is able to predict deletions
# the files contain the predicted deletions in this directory should be named with suffix of .del
# use replace_file_name.sh
svseq2_file = paste(base.dir, paste("/svseq2",samples[i],sep="/"), sep="")
svseq2 <- readSvseq(svseq2_file)
str(svseq2)

##########
# Pindel #
##########
# Pindel is split-read for both short indels and SVs

# Integrate predictions of different programs
# sv_all_methods <- methodsMerge(cnvnator,svseq2, numMethodsSupDup = 0, numMethodsSupInv = 0)
# function (..., 

others = NULL
overLapPerDel = 0.8
overLapPerDup = 0.8 
overLapPerInv = 0.8
numMethodsSupDel = 2
numMethodsSupDup = 1 
numMethodsSupInv = 1 


  svIn <- list(cnvnator,svseq2)
  DeletionList <- lapply(svIn, function(x) {
    return(x$del)
  })
  
  DeletionDf <- do.call(rbind, DeletionList)
  svInMethod <- sapply(svIn, function(x) {
    return(attr(x, "method"))
  })
  DeletionDf <- rbind(DeletionDf, others[others$type == "del", ][, 1:4])
  
  DeletionDf$method <- c(rep(svInMethod, unlist(lapply(DeletionList, 
                                                       function(x) {
                                                         ifelse(is.null(nrow(x)), 0, nrow(x))
                                                       }))), others[others$type == "del", ]$methods)
  DuplicationList <- lapply(svIn, function(x) {
    return(x$dup)
  })
  DuplicationDf <- do.call(rbind, DuplicationList)
  DuplicationDf <- rbind(DuplicationDf, others[others$type == "dup", ][, 1:4])
  
  DuplicationDf$method <- c(rep(svInMethod, unlist(lapply(DuplicationList, 
                                                          function(x) {
                                                            ifelse(is.null(nrow(x)), 0, nrow(x))
                                                          }))), others[others$type == "dup", ]$methods)
  
  # Inversion
  # InversionList <- lapply(svIn, function(x) {
  #   return(x$inv)
  # })
  # InversionDf <- do.call(rbind, InversionList)
  # InversionDf <- rbind(InversionDf, others[others$type == "inv", 
  #                                          ][, 1:4])
  # InversionDf$method <- c(rep(svInMethod, unlist(lapply(InversionList, 
  #                                                       function(x) {
  #                                                         ifelse(is.null(nrow(x)), 0, nrow(x))
  #                                                       }))), others[others$type == "inv", ]$methods)
  # 
  # MethodsName <- union(svInMethod, names(table(others$method)))
  # 
  # 
  # InversionIrange <- GRanges(seqnames = InversionDf$chromosome,
  #                            ranges = IRanges(start = InversionDf$pos1, end = InversionDf$pos2))
  # InversionRes <- findOverlaps(InversionIrange, reduce(InversionIrange))
  # InversionDf$class <- subjectHits(InversionRes)
  # 
  # if (nrow(InversionDf) > 0) {
  #   InversionDfMerge <- ddply(InversionDf, ("class"), methodsCluster,
  #                             methodsName = MethodsName, overLapPer = overLapPerInv,
  #                             numMethodsSup = numMethodsSupInv)
  #   if (nrow(InversionDfMerge) > 0) {
  #     InversionDfMerge$class <- NULL
  #     InversionDfMerge$cl <- NULL
  #     names(InversionDfMerge)[1:3] <- c("chromosome", "pos1","pos2")
  #     InversionDfMerge$pos1 <- as.numeric(InversionDfMerge$pos1)
  #     InversionDfMerge$pos2 <- as.numeric(InversionDfMerge$pos2)
  #   }
  #   else {
  #     InversionDfMerge <- NULL
  #   }
  # } else {
  #   InversionDfMerge <- NULL
  # }

  methodsCluster = NULL
    
  DeletionIrange <- GRanges(seqnames = DeletionDf$chromosome, 
                            ranges = IRanges(start = DeletionDf$pos1, end = DeletionDf$pos2))
  DeletionRes <- findOverlaps(DeletionIrange, reduce(DeletionIrange))
  DeletionDf$class <- subjectHits(DeletionRes)
  if (nrow(DeletionDf) > 0) {
    DeletionDfMerge <- ddply(DeletionDf, ("class"), methodsCluster, 
                             methodsName = MethodsName, overLapPer = overLapPerDel, 
                             numMethodsSup = numMethodsSupDel)
    if (nrow(DeletionDfMerge) > 0) {
      DeletionDfMerge$class <- NULL
      DeletionDfMerge$cl <- NULL
      names(DeletionDfMerge)[1:3] <- c("chromosome", "pos1", "pos2")
      DeletionDfMerge$pos1 <- as.numeric(DeletionDfMerge$pos1)
      DeletionDfMerge$pos2 <- as.numeric(DeletionDfMerge$pos2)
    } else {
      DeletionDfMerge <- NULL
    }
  } else {
    DeletionDfMerge <- NULL
  }
  DuplicationIrange <- GRanges(seqnames = DuplicationDf$chromosome, 
                               ranges = IRanges(start = DuplicationDf$pos1, end = DuplicationDf$pos2))
  DuplicationRes <- findOverlaps(DuplicationIrange, reduce(DuplicationIrange))
  DuplicationDf$class <- subjectHits(DuplicationRes)
  if (nrow(DuplicationDf) > 0) {
    DuplicationDfMerge <- ddply(DuplicationDf, ("class"), 
                                methodsCluster, methodsName = MethodsName, overLapPer = overLapPerDup, 
                                numMethodsSup = numMethodsSupDup)
    if (nrow(DuplicationDfMerge) > 0) {
      DuplicationDfMerge$class <- NULL
      DuplicationDfMerge$cl <- NULL
      names(DuplicationDfMerge)[1:3] <- c("chromosome", 
                                          "pos1", "pos2")
      DuplicationDfMerge$pos1 <- as.numeric(DuplicationDfMerge$pos1)
      DuplicationDfMerge$pos2 <- as.numeric(DuplicationDfMerge$pos2)
    } else {
      DuplicationDfMerge <- NULL
    }
  } else {
    DuplicationDfMerge <- NULL
  }
mergedCNV <- list(del = DeletionDfMerge, dup = DuplicationDfMerge)
# list(del = DeletionDfMerge, dup = DuplicationDfMerge, inv = InversionDfMerge)


plotChromosomeEdited <- function (genomeAnnotation, structuralVariation, windowSize = 1e+06) 
{
  genomeDf <- as.data.frame(cbind(rep(as.character(seqnames(genomeAnnotation)@values), 
                                      seqnames(genomeAnnotation)@lengths), start(ranges(genomeAnnotation)), 
                                  end(ranges(genomeAnnotation))), stringsAsFactors = F)
  genomeDf$V2 <- as.numeric(genomeDf$V2)
  genomeDf$V3 <- as.numeric(genomeDf$V3)
  genomeDfRes <- ddply(genomeDf, c("V1", "V3"), function(df) {
    if (df$V3[1] > windowSize) {
      pos1 <- seq(1, df$V3[1], by = as.numeric(windowSize))
      pos2 <- seq(as.numeric(windowSize), df$V3[1], by = as.numeric(windowSize))
      if (length(pos1) != length(pos2)) {
        pos2 <- c(pos2, df$V3[1])
      }
    }
    else {
      pos1 <- 1
      pos2 <- df$V3[1]
    }
    dfRes <- as.data.frame(cbind(pos1, pos2))
    return(dfRes)
  })
  genomeDfResIrange <- GRanges(seqnames = genomeDfRes$V1, IRanges(start = genomeDfRes$pos1,end = genomeDfRes$pos2))
  genomeDfRes$query <- 1:nrow(genomeDfRes)
  if (!is.null(structuralVariation$del) && (is.data.frame(structuralVariation$del)) && 
      (nrow(structuralVariation$del) > 0)) {
    delDf <- structuralVariation$del[(structuralVariation$del)$chromosome %in% seqnames(genomeAnnotation)@values, ]
    delIrange <- GRanges(seqnames = delDf$chromosome, IRanges(start = as.numeric(delDf$pos1), end = as.numeric(delDf$pos2)))
    delOverlap <- findOverlaps(genomeDfResIrange, delIrange)
    delOverlapRes <- as.data.frame(cbind(queryHits(delOverlap), subjectHits(delOverlap)))
    names(delOverlapRes) <- c("query", "delTag")
    delOverlapCount <- ddply(delOverlapRes, ("query"), nrow)
    names(delOverlapCount)[2] <- "delTag"
    genomeDfDel <- merge(genomeDfRes, delOverlapCount, by = "query",all = T)
  }
  else {
    genomeDfDel <- genomeDfRes
    delOverlapCount <- NULL
  }
  if (!is.null(structuralVariation$dup) && (is.data.frame(structuralVariation$dup)) && (nrow(structuralVariation$dup) > 0)) {
    dupDf <- structuralVariation$dup[(structuralVariation$dup)$chromosome %in% seqnames(genomeAnnotation)@values, ]
    dupIrange <- GRanges(seqnames = dupDf$chromosome, IRanges(start = as.numeric(dupDf$pos1), end = as.numeric(dupDf$pos2)))
    dupOverlap <- findOverlaps(genomeDfResIrange, dupIrange)
    dupOverlapRes <- as.data.frame(cbind(queryHits(dupOverlap), subjectHits(dupOverlap)))
    names(dupOverlapRes) <- c("query", "subject")
    dupOverlapCount <- ddply(dupOverlapRes, ("query"), nrow)
    names(dupOverlapCount)[2] <- "dupTag"
  }
  else {
    dupOverlapCount <- NULL
  }
  if (!is.null(structuralVariation$inv) && (is.data.frame(structuralVariation$inv)) && 
      (nrow(structuralVariation$inv) > 0)) {
    invDf <- structuralVariation$inv[(structuralVariation$inv)$chromosome %in% seqnames(genomeAnnotation)@values, ]
    invIrange <- GRanges(seqnames = invDf$chromosome, IRanges(start = as.numeric(invDf$pos1), end = as.numeric(invDf$pos2)))
    invOverlap <- findOverlaps(genomeDfResIrange, invIrange)
    invOverlapRes <- as.data.frame(cbind(queryHits(invOverlap), subjectHits(invOverlap)))
    names(invOverlapRes) <- c("query", "subject")
    invOverlapCount <- ddply(invOverlapRes, ("query"), nrow)
    names(invOverlapCount)[2] <- "invTag"
  }
  else {
    invOverlapCount <- NULL
  }
  if (!is.null(dupOverlapCount)) {
    genomeDfDelDup <- merge(genomeDfDel, dupOverlapCount, by = "query", all = T)
  }
  else {
    genomeDfDelDup <- genomeDfDel
  }
  if (!is.null(invOverlapCount)) {
    genomeDfDelDupInv <- merge(genomeDfDelDup, invOverlapCount, by = "query", all = T)
  }
  else {
    genomeDfDelDupInv <- genomeDfDelDup
  }
  genomeDfDelDupInvIrange <- GRanges(seqnames = genomeDfDelDupInv$V1, 
                                     IRanges(start = genomeDfDelDupInv$pos1, end = genomeDfDelDupInv$pos2))
  if (!is.null(delOverlapCount)) {
    genomeDfDelDupInv$delTag[is.na(genomeDfDelDupInv$delTag)] <- 0
    genomeDfDelDupInvIrange$delScore <- genomeDfDelDupInv$delTag
  }
  if (!is.null(dupOverlapCount)) {
    genomeDfDelDupInv$dupTag[is.na(genomeDfDelDupInv$dupTag)] <- 0
    genomeDfDelDupInvIrange$dupScore <- genomeDfDelDupInv$dupTag
  }
  if (!is.null(invOverlapCount)) {
    genomeDfDelDupInv$invTag[is.na(genomeDfDelDupInv$invTag)] <- 0
    genomeDfDelDupInvIrange$invScore <- genomeDfDelDupInv$invTag
  }
  seqlengths(genomeDfDelDupInvIrange) <- seqlengths(genomeAnnotation)[match(names(seqlengths(genomeDfDelDupInvIrange)), 
                                                                            names(seqlengths(genomeAnnotation)))]
  p <- ggplot() + layout_circle(genomeDfDelDupInvIrange, geom = "ideo", fill = "gray70", radius = 30, trackwidth = 4)
  p <- p + layout_circle(genomeDfDelDupInvIrange, geom = "scale", size = 2, radius = 35, trackWidth = 2)
  p <- p + layout_circle(genomeAnnotation, geom = "text", aes(label = seqnames), vjust = 0, radius = 38, trackWidth = 4)
  if (!is.null(delOverlapCount)) {
    p <- p + layout_circle(genomeDfDelDupInvIrange, geom = "bar", radius = 25, trackwidth = 4, aes(y = "delScore"), fill = "blue", color = "blue")
  }
  if (!is.null(dupOverlapCount)) {
    p <- p + layout_circle(genomeDfDelDupInvIrange, geom = "bar", radius = 20, trackwidth = 4, aes(y = "dupScore"), fill = "red", color = "red")
  }
  if (!is.null(invOverlapCount)) {
    p <- p + layout_circle(genomeDfDelDupInvIrange, geom = "bar",radius = 15, trackwidth = 4, aes(y = "invScore"), color = "green4", fill = "green4")
  }
  return(p)
}

mergedCNV$del <- mergedCNV$del[mergedCNV$del$method == "SVseq2", ]

## SV ANNOTATION
cnvnator.anno <- llply(mergedCNV, svAnnotation, genomeAnnotation=GRCh38.anno)

## Gene Annotation
# cnvnator.anno <- llply(mergedCNV, geneAnnotation, genomeAnnotation=GRCh38.anno)

cnvnator.anno$del <- cnvnator.anno$del[cnvnator.anno$del$overlap >= 0.1 & cnvnator.anno$del$annotation %in% c("mRNA","gene"),]
# cnvnator.anno$dup <- cnvnator.anno$dup[cnvnator.anno$dup$overlap >= 0.1 & cnvnator.anno$dup$annotation %in% c("mRNA","gene"),]

cnvnator.anno$del$cnv <- "del"
# cnvnator.anno$dup$cnv <- "dup"

# head(cnvnator.anno$del)

# number of deletions, duplications in each window
# function (genomeAnnotation, structuralVariation, windowSize = 1e+06) 
# plotlist[[i]] <- plotChromosome(genome, cnvnator.anno, windowSize=1000000) 
# function (structuralVariation, genomeAnnotation, regionChromosome, regionStart, regionEnd)

# out_file = paste("/Users/adinasa/Desktop/Geisert_CNV/","_merged_bin150.png",sep=samples[i])
# png(filename=out_file)
# plotChromosomeEdited(genome, cnvnator.anno, windowSize=1000000) 
# dev.off()

out <- cnvnator.anno$del
#out <- rbind(cnvnator.anno$del,cnvnator.anno$dup)
out <- merge(out,df.meta, by.x="id", by.y="gene_id",all.x=T)
out <- out[out$overlap > 0.1 & out$annotation == "gene",]

out_file2 = paste("/Users/adinasa/Desktop/Geisert_CNV/","_SvSeq2_Deletions_bin500.txt",sep=samples[i])
write.table(out,file=out_file2, row.names=F, col.names=T, quote=F, sep="\t")
