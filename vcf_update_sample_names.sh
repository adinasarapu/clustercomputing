#!/bin/sh

# For multi sample VCF file

VCF_FILE_IN=${HOME}/Projects/WGS_TCA21177/SL151929.vcf.gz
VCF_FILE_OUT=${HOME}/Projects/WGS_TCA21177/SL151929.header.vcf.gz

# one name per line, in the same order as they appear in the VCF file. 
# Alternatively, only samples which need to be renamed can be listed as 
# "old_name new_name\n" pairs separated by whitespaces, each on separate line. 
sample.names=${HOME}/Projects/WGS_TCA21177/sample.names

module load bcftools/1.3

# create a text file, sample.names (a row with a new sample name)
bcftools reheader -s ${sample.names} ${VCF_FILE_IN} > ${VCF_FILE_OUT}
bcftools index ${VCF_FILE_OUT} -t

module unload bcftools/1.3
