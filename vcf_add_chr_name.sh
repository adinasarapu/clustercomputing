#!/bin/sh

#===============================================#
# adds 'chr' to the VCF file for column, #CHROM #
#===============================================#

# files=`ls -la ${HOME}/Projects/WGS_TCA21177/DBA/*.vcf.gz | awk '{print $9}'`
# echo $files
# awk '{if($0 !~ /^#/) print "chr"$0; else print $0}' \

for i in ${HOME}/Projects/WGS_TCA21177/DBA/*.vcf.gz; do
	in=$(echo "$i")
	out=$(echo $i | sed 's/\.vcf.gz$/.chr.vcf.gz/'); 
	echo $in $out
	zcat $in | \
		awk '{ if($0 !~ /^#/) print "chr"$0; else if(match($0,/(##contig=<ID=)(.*)/,m)) print m[1]"chr"m[2]; else print $0 }' | \
		bgzip > $out
done

#================#
# Index VCF file #
#================#

# module load samtools 
# tabix -p vcf <.vcf.gz file>

# module load bcftools
# bcftools index <.vcf.gz file> -t

#=================#
# Merge VCF files #
#=================#

# module load vcftools
# module load samtools

# vcf-merge A.vcf.gz B.vcf.gz C.vcf.gz | bgzip > out.vcf.gz
# bcftools index <.vcf.gz file> -t

#=========================#
# Sample name in VCF file #
#=========================#
# create a text file, SL151929.txt (a row with a new sample name)
# bcftools reheader -s SL151929.txt SL151929.vcf.gz > SL151929.header.vcf.gz
# bcftools index <.vcf.gz file> -t

# http://www.danielecook.com/rename-samples-within-vcfbcf/
