#!/bin/sh

# https://forum.qiime2.org/t/filtering-and-then-merging-2-different-runs-same-barcodes-used-in-both-runs/897

echo "Start - `date`" 
#$ -N table.merge
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email>@emory.edu

# makes the job dependent on the 1st job completion
# qsub -hold_jid <job id> ../script/qiime2.2017.8_step1.sh

source activate qiime2-2018.2

PROJ_DIR=$HOME/microbiome/LHA11561
QIIME2_DIR=$PROJ_DIR/qiime2_2018_2

META_FILE="LHA11561.txt"
TABLES_MERGE=true

export TMPDIR=$HOME/tmp

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

chmod a+trwx $TMPDIR
MERGED_DIR=$QIIME2_DIR/plates_merged

cp $PROJ_DIR/$META_FILE $TMP_DIR/meta_file.txt

#===DADA2 file paths - start =======#

# TABLE_1=$QIIME2_DIR/plate1_denoised/dada2/trim_20_240/table.qza
# TABLE_2=$QIIME2_DIR/plate2_denoised/dada2/trim_20_240/table.qza

# SEQ_1=$QIIME2_DIR/plate1_denoised/dada2/trim_20_240/representative_sequences.qza
# SEQ_2=$QIIME2_DIR/plate2_denoised/dada2/trim_20_240/representative_sequences.qza

# OUT_DIR=$MERGED_DIR/dada2/trim_20_240

#===DADA2 file paths - end =======#

#===VSEARCH file paths - start =======#

TABLE_1="$QIIME2_DIR/plate1_denoised/vsearch/ovlen_15/vsearch_joined_filtered_table.qza"
TABLE_2="$QIIME2_DIR/plate2_denoised/vsearch/ovlen_15/vsearch_joined_filtered_table.qza"

SEQ_1="$QIIME2_DIR/plate1_denoised/vsearch/ovlen_15/vsearch_joined_filtered_rep_seqs.qza"
SEQ_2="$QIIME2_DIR/plate2_denoised/vsearch/ovlen_15/vsearch_joined_filtered_rep_seqs.qza"

OUT_DIR="$MERGED_DIR/vsearch/ovlen_15"

#===VSEARCH file paths - end =======#

if [ "$TABLES_MERGE" = true ]; then

 TMP_SUB_DIR=$TMP_DIR/sub_dir 
 /usr/bin/mkdir -p $TMP_SUB_DIR

 cp $TABLE_1 $TMP_SUB_DIR/table1.qza
 cp $TABLE_2 $TMP_SUB_DIR/table2.qza
 cp $SEQ_1 $TMP_SUB_DIR/rep_seq1.qza
 cp $SEQ_2 $TMP_SUB_DIR/rep_seq2.qza
 
 qiime feature-table merge \
  --i-tables $TMP_SUB_DIR/table1.qza \
  --i-tables $TMP_SUB_DIR/table2.qza \
  --p-overlap-method sum \
  --o-merged-table $TMP_SUB_DIR/merged_table.qza

 qiime feature-table merge-seqs \
  --i-data $TMP_SUB_DIR/rep_seq1.qza \
  --i-data $TMP_SUB_DIR/rep_seq2.qza \
  --o-merged-data $TMP_SUB_DIR/merged_seqs.qza

 # Features observed in a user-defined fraction of the samples 
 qiime feature-table core-features \
  --i-table $TMP_SUB_DIR/merged_table.qza \
  --o-visualization $TMP_SUB_DIR/table_core_features.qzv

 # Summarize table
 qiime feature-table summarize \
  --i-table $TMP_SUB_DIR/merged_table.qza \
  --o-visualization $TMP_SUB_DIR/table_summarize.qzv \
  --m-sample-metadata-file $TMP_DIR/meta_file.txt

 # View sequence associated with each feature
 qiime feature-table tabulate-seqs \
  --i-data $TMP_SUB_DIR/merged_seqs.qza \
  --o-visualization $TMP_SUB_DIR/merged_seqs.qzv
 
 /bin/rm $TMP_SUB_DIR/*1.qza
 /bin/rm $TMP_SUB_DIR/*2.qza
 
 if [ ! -d $OUT_DIR ]; then
  mkdir -p $OUT_DIR
 fi
 
 rsync -av $TMP_SUB_DIR/* $OUT_DIR
 /bin/rm -rf $TMP_SUB_DIR

fi

/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.2
echo "Finish - `date`"
