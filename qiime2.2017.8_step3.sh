#!/bin/sh

echo "Start - `date`" 
#$ -N QIIME.Filt
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 1
#$ -cwd
#$ -j y
#$ -m abe
#$ -M adinasa@emory.edu

# module load Anaconda3/4.2.0
source activate qiime2-2017.8

PROJ_DIR=$HOME/RAH11569
QIIME2_DIR=$PROJ_DIR/qiime2
DATA_DIR=$QIIME2_DIR/data-denoise-export/table-dada2

# table.qza 
# representative_sequences.qza
# exported-feature-table/feature-table.biom

# for EICC (Since no /scratch dir at EICC computing nodes  use ...)
export TMPDIR=$HOME/tmp

# for HGCC (creates a unique dir on local drive)
# export TMPDIR=/scratch/tmp 

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

cp $DATA_DIR/*.qza  $TMP_DIR/
cp $PROJ_DIR/85_otu* $TMP_DIR/

###################################################################
# Filter (i.e., remove) samples and features from a feature table #
###################################################################

frequency_based_filtering=false
tot_abundance_filtering=false
sample_contingency_filtered=true
sample_subset=false

# total-frequency-based filtering
if [ "$frequency_based_filtering" = true ]; then
	qiime feature-table filter-samples \
		--i-table $TMP_DIR/table.qza \
		--p-min-frequency 1500 \
		--o-filtered-table $TMP_DIR/feature-frequency-samples-table.qza
fi

# remove all features with a total abundance (summed across all samples) of less than 10 as follows
if [ "$tot_abundance_filtering" = true ]; then
	qiime feature-table filter-features \
		--i-table $TMP_DIR/table.qza \
		--p-min-frequency 10 \
		--o-filtered-table $TMP_DIR/feature-frequency-features-table.qza
fi
# Contingency-based filtering
# Features that are present in only a single sample could be filtered from a feature table as follows
if [ "$sample_contingency_filtered" = true ]; then
	qiime feature-table filter-features \
		--i-table $TMP_DIR/table.qza \
		--p-min-samples 2 \
		--o-filtered-table $TMP_DIR/sample-contingency-filtered-table.qza
fi

if [ "$sample_subset" = true ]; then
	qiime feature-table filter-samples \
		--i-table $TMP_DIR/sample-contingency-filtered-table.qza \
		--m-metadata-file $PROJ_DIR/RAH11569.tsv \
		--p-where "Source='Fecal' AND Experiment='102616a4b'" \
		--o-filtered-table $TMP_DIR/experiment-102616a4b-filtered-table.qza
fi

# FeatureTable and FeatureData summaries
qiime feature-table summarize \
	--i-table $TMP_DIR/sample-contingency-filtered-table.qza \
	--o-visualization $TMP_DIR/sample-contingency-filtered-table.qzv \
	--m-sample-metadata-file $PROJ_DIR/RAH11569.tsv

qiime feature-table tabulate-seqs \
	--i-data $TMP_DIR/representative_sequences.qza \
	--o-visualization $TMP_DIR/representative_sequences.qzv

#=======================================#
# Export data from a QIIME2 Artifact	#
#=======================================#
# OUTPUT: feature-table.biom
BIOM_EXP=$TMP_DIR/feature-filtered-table

qiime tools export $TMP_DIR/sample-contingency-filtered-table.qza \
	--output-dir $BIOM_EXP

# pip install biom-format python package
# /home/adinasarapu/YGO11472/sample_map_group_corrected.txt
# Summarize a BIOM table
biom summarize-table -i $BIOM_EXP/feature-table.biom > $BIOM_EXP/biom_summary.txt

head $PROJ_DIR/RAH11569.tsv

biom add-metadata \
	-i $BIOM_EXP/feature-table.biom \
	-o $BIOM_EXP/feature-filtered-table-mdata.biom \
	-m $PROJ_DIR/RAH11569.tsv

biom summarize-table -i $BIOM_EXP/feature-filtered-table-mdata.biom > $BIOM_EXP/biom_mdata_summary.txt

biom_normalize=false
# Normalizing a BIOM table to relative abundnace
if [ "$biom_normalize" = true ]; then
	biom normalize-table \
		-i $BIOM_EXP/feature-table.biom \
		-r \
		-o $BIOM_EXP/normalized_table.biom

	biom add-metadata \
        	-i $BIOM_EXP/normalized_table.biom \
        	-o $BIOM_EXP/normalized_table_mdata.biom \
        	-m $PROJ_DIR/RAH11569.tsv

	biom convert \
        	-i $BIOM_EXP/normalized_table_mdata.biom \
        	-o $BIOM_EXP/normalized_table_mdata.json \
        	--table-type="OTU table" \
        	--to-json

	biom convert \
		-i $BIOM_EXP/normalized_table_mdata.biom \
		-o $BIOM_EXP/normalized_table_mdata.tsv \
		--table-type="OTU table" \
		--to-tsv
fi

/bin/mkdir -p $TMP_DIR/{phylogeny,taxonomy}

# Generate a tree for phylogenetic diversity analyses
# multiple sequence alignment of the sequences in FeatureData[Sequence] 
# to create a FeatureData[AlignedSequence] QIIME 2 artifact.
qiime alignment mafft \
	--i-sequences $TMP_DIR/representative_sequences.qza \
	--o-alignment $TMP_DIR/phylogeny/aligned-rep-seqs.qza
# mask (or filter) the alignment to remove positions that are highly variable. 
# These positions are generally considered to add noise to a resulting phylogenetic tree.
qiime alignment mask \
	--i-alignment $TMP_DIR/phylogeny/aligned-rep-seqs.qza \
	--o-masked-alignment $TMP_DIR/phylogeny/masked-aligned-rep-seqs.qza
# apply FastTree to generate a phylogenetic tree from the masked alignment.
qiime phylogeny fasttree \
	--i-alignment $TMP_DIR/phylogeny/masked-aligned-rep-seqs.qza \
	--o-tree $TMP_DIR/phylogeny/unrooted-tree.qza
# final step in this section we apply midpoint rooting
qiime phylogeny midpoint-root \
	--i-tree $TMP_DIR/phylogeny/unrooted-tree.qza \
	--o-rooted-tree $TMP_DIR/phylogeny/rooted-tree.qza

# Alpha and beta diversity analysis
qiime diversity core-metrics \
	--i-phylogeny $TMP_DIR/phylogeny/rooted-tree.qza \
	--i-table $TMP_DIR/sample-contingency-filtered-table.qza \
	--p-sampling-depth 80 \
	--output-dir $TMP_DIR/phylogeny/core-metrics-results

qiime diversity alpha-group-significance \
	--i-alpha-diversity $TMP_DIR/phylogeny/core-metrics-results/faith_pd_vector.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/faith-pd-group-significance.qzv

qiime diversity alpha-group-significance \
	--i-alpha-diversity $TMP_DIR/phylogeny/core-metrics-results/evenness_vector.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/evenness-group-significance.qzv

# analyze sample composition in the context of discrete metadata using PERMANOVA 
#qiime diversity beta-group-significance \
#	--i-distance-matrix $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_distance_matrix.qza \
#	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
#	--m-metadata-category Experiment \
#	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/unweighted-unifrac-experiment-significance.qzv \
#	--p-pairwise

qiime diversity beta-group-significance \
	--i-distance-matrix $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_distance_matrix.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--m-metadata-category Group \
	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/unweighted-unifrac-subject-group-significance.qzv \
	--p-pairwise

qiime emperor plot \
	--i-pcoa $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_pcoa_results.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--p-custom-axis Group \
	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/unweighted-unifrac-emperor.qzv

qiime tools export --output-dir $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_pcoa \
	$TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_pcoa_results.qza

# (qiime1) make_2d_plots.py -i exported/ordination.txt -m sample-metadata.tsv -o 2D-plots

qiime emperor plot \
	--i-pcoa $TMP_DIR/phylogeny/core-metrics-results/bray_curtis_pcoa_results.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--p-custom-axis Group \
	--o-visualization $TMP_DIR/phylogeny/core-metrics-results/bray-curtis-emperor.qzv

qiime tools export --output-dir $TMP_DIR/phylogeny/core-metrics-results/bray_curtis_pcoa \
	$TMP_DIR/phylogeny/core-metrics-results/bray_curtis_pcoa_results.qza

# Taxonomic analysis
# wget -O "85_otus.fasta" "https://data.qiime2.org/2017.8/tutorials/training-feature-classifiers/85_otus.fasta"
# wget -O "85_otu_taxonomy.txt" "https://data.qiime2.org/2017.8/tutorials/training-feature-classifiers/85_otu_taxonomy.txt"

qiime tools import \
	--type 'FeatureData[Sequence]' \
	--input-path $TMP_DIR/85_otus.fasta \
	--output-path $TMP_DIR/taxonomy/85_otus.qza

qiime tools import \
	--type 'FeatureData[Taxonomy]' \
	--source-format HeaderlessTSVTaxonomyFormat \
	--input-path $TMP_DIR/85_otu_taxonomy.txt \
	--output-path $TMP_DIR/taxonomy/ref-taxonomy.qza

# extract reference reads
qiime feature-classifier extract-reads \
	--i-sequences $TMP_DIR/taxonomy/85_otus.qza \
	--p-f-primer CCTACGGGNGGCWGCAG \
	--p-r-primer GACTACHVGGGTATCTAATCC \
	--p-trunc-len 300 \
	--o-reads $TMP_DIR/taxonomy/ref-seqs.qza

# now train a Naive Bayes classifier
qiime feature-classifier fit-classifier-naive-bayes \
	--i-reference-reads $TMP_DIR/taxonomy/ref-seqs.qza \
	--i-reference-taxonomy $TMP_DIR/taxonomy/ref-taxonomy.qza \
	--o-classifier $TMP_DIR/taxonomy/classifier.qza

# test the classifier
qiime feature-classifier classify-sklearn \
	--i-classifier $TMP_DIR/taxonomy/classifier.qza \
	--i-reads $TMP_DIR/representative_sequences.qza \
	--o-classification $TMP_DIR/taxonomy/taxonomy.qza

qiime metadata tabulate \
	--m-input-file $TMP_DIR/taxonomy/taxonomy.qza \
	--o-visualization $TMP_DIR/taxonomy/taxonomy.qzv

# view the taxonomic composition of our samples with interactive bar plots.
qiime taxa barplot \
	--i-table $TMP_DIR/sample-contingency-filtered-table.qza \
	--i-taxonomy $TMP_DIR/taxonomy/taxonomy.qza \
	--m-metadata-file $PROJ_DIR/RAH11569.tsv \
	--o-visualization $TMP_DIR/taxonomy/taxa-bar-plots.qzv

source deactivate qiime2-2017.8

source activate qiime1
make_2d_plots.py -i $TMP_DIR/phylogeny/core-metrics-results/bray_curtis_pcoa/ordination.txt \
	-m ~/RAH11569/RAH11569.tsv \
	-o $TMP_DIR/phylogeny/core-metrics-results/bray_curtis_pcoa

make_2d_plots.py -i $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_pcoa/ordination.txt \
	-m ~/RAH11569/RAH11569.tsv \
	-o $TMP_DIR/phylogeny/core-metrics-results/unweighted_unifrac_pcoa
source deactivate qiime1

# Differential abundance testing with ANCOM

/bin/rm $TMP_DIR/{table.qza,representative_sequences.qza,85_otus.fasta,85_otu_taxonomy.txt}

OUT_DIR=$QIIME2_DIR/feature-filter-phylo-taxonomy

if [ ! -d $OUT_DIR ]; then
        mkdir -p $OUT_DIR
fi

rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

# source deactivate qiime2-2017.8
# module unload Anaconda3/4.2.0
echo "Finish - `date`"
