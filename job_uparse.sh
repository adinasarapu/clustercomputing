#!/bin/bash

if [ usearch == x ] ; then
	echo Must set usearch >> /dev/stderr
	exit 1
fi

out=../out/vax002

# remove existing out (sub)folder and it's contents
rm -rf $out

# -p: will create all intermediary dirs that don't exist
mkdir -p $out

###########################
# Merging of Paired Reads #
###########################
# fastq_mergepairs: performs merging of paired reads
# fastqout or fastaout: merged reads output
# relabel @ : to label the merged reads with the sample name

usearch -fastq_mergepairs ../fq/MiSeq_V2/ABX02*_R1_*.fastq -fastqout $out/merged.fq -relabel @ -log $out/merge.log \
	-fastq_maxdiffs 20 -fastq_maxdiffpct 20

#############################
# Filtering of merged reads #
#############################
# filtered reads are used to construct OTUs but unfiltered reads are used to construct the OTU table, 
# so merged reads before and after filtering are both needed.
# maximum expected error parameter (fastq_maxee)

# If possible, parameters for the fastq_filter command should be chosen manually for each sequencing run 
# by examining the distribution of read length and Phred scores by position in the read, as these characteristics can vary 
# considerably and can have a large impact on downstream analysis.

usearch -fastq_filter $out/merged.fq -fastq_maxee 1.0 -fastaout $out/filtered.fa -relabel Filt -log $out/filter.log

#####################################################
# Dereplication, identification of unique sequences #
#####################################################
usearch -derep_fulllength $out/filtered.fa -sizeout -relabel Uniq -fastaout $out/uniques.fa -log $out/derep.log

# Input is a FASTA file containing quality filtered, globally trimmed and dereplicated reads
# Use -minsize 2 to discard singletons

usearch -cluster_otus $out/uniques.fa -minsize 2 -otus $out/otus.fa -relabel otu -log $out/cluster_otus.log
usearch -utax $out/otus.fa -db ../utax/utax_16s_500.udb -strand plus -fastaout $out/otus_tax.fa -utax_cutoff 0.9 -log $out/utax.log

# usearch_global: alignments are global
# query_file (otus.fa): query file may be in FASTA or FASTQ format
# id: identity threshold
# db: database file
# strand option is required for nucleotide databases
# alnout: human readable output files
# userout: User-defined tabbed files (userfields: field names)

usearch -usearch_global $out/otus.fa -db ../utax/utax_16s_500.udb -strand plus -id 0.97 -alnout $out/otus_ref.aln \
	-userout $out/otus_ref.user -userfields query+target+id

usearch -usearch_global $out/merged.fq -db $out/otus_tax.fa -strand plus -id 0.97 -log $out/make_otutab.log \
	-otutabout $out/otutab.txt -biomout $out/otutab.json -mothur_shared_out $out/otutab.mothur 
