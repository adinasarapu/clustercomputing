#!/usr/bin/perl 

echo "Start - `date`" 
#$ -r n 
#$ -N PE.CNV
#$ -q b.q
#$ -pe smp 28
#$ -cwd
#$ -j y
#$ -m abe
#$ -M xyz@emory.edu 
 
PID=TYEP0004
PROJ_DIR=$HOME/zwick_rare
SCRIPT=$PROJ_DIR/script
CALL_OUT=$PROJ_DIR/$PID/CallingResults
TYE_DIR=$PROJ_DIR/$PID/TYEDirectory
BASE_FILE=$TYE_DIR/TYEDirectory.base.gz

OUT=$PROJ_DIR/$PID/PE_CNV
if [ ! -d $OUT ]; then
	mkdir -p $OUT
fi

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
      TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi
  
# A file that contains all the good variants in the genome.

# Assuming it is coming from a BIM (plink) file
# 	cut -f 1,4 plink.geno5pc.bim > good_positions_list.txt

# append chr to chromosome number

# PLINK's internal numeric coding of chromosomes
# 	The autosomes coded 1 through 22. 

# The following other codes used to specify other chromosome types:
#	X    X chromosome                    -> 23
#	Y    Y chromosome                    -> 24
#	XY   Pseudo-autosomal region of X    -> 25
#	MT   Mitochondrial                   -> 26

# awk '{OFS = "\t";$1="chr"$1 ;print}' < good_positions_list.txt > final_positions_list2.txt
# awk -v OFS='\t' '{ gsub("chr26","chrM",$1); print $1, $2 }' < final_positions_list3.txt > final_positions_list4.txt
# 	chr26	chrM
# 	chr23	chrX
 
echo "copying data to $TMPDIR"
rsync -av $BASE_FILE $TMPDIR/${PID}.base.gz
rsync -av $PROJ_DIR/hg38/hg38.{sdx,seq} $TMPDIR/
rsync -av $PROJ_DIR/$PID/PostCallQC/PE/plink/final_positions_list5.txt $TMPDIR/good_positions.txt

echo "`ls -la $TMPDIR | awk '{print $9}'`"

# maxsnps is a number bigger than the number of positions in final_positions_list4.txt
maxsnps=`cat "$TMPDIR/good_positions.txt" | wc -l`
let "maxsnps++"
echo "maxsnps --> "$maxsnps

# maxsamples is bigger than the number of samples
maxsamples=`ls -l $TYE_DIR/*.pileup.gz | wc -l`
let "maxsamples++"
echo "maxsamples --> "$maxsamples

# Important: submit this job from TYEDirectory
cd $TYE_DIR

echo "snp_depth_dump ..."
$SCRIPT/snp_depth_dump \
	$maxsnps \
	$maxsamples \
	$TMPDIR/good_positions.txt \
	$TMPDIR/hg38.sdx \
	$TMPDIR/${PID}.base.gz > $TMPDIR/file.piles.log

mv $PROJ_DIR/$PID/TYEDirectory/*.piles.txt $TMPDIR/
ls -la $TMPDIR/

for entry in $TMPDIR/*.txt; do
	fname=$(basename $entry)
	fbname=${fname%.piles*}
	$SCRIPT/pe_cnv_search.pl $TMPDIR/${fbname}.piles.txt 0.05 $TMPDIR/${fbname}.cnv.txt
done 

/bin/rm $TMPDIR/{hg38.*,*.base.gz}

echo "copying data to $OUT"

rsync -av $TMPDIR/* $OUT/

/bin/rm -rf $TMPDIR

echo "Finish - `date`" 

# finally, filter CNVs based on p-value
# awk -F"\t" -v OFS="\t" '$5 < 1 { print $1,$2,$3,$4,$5 }' SL148577.cnv.txt | head
