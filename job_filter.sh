#!/bin/sh

echo "Start - `date`" 
#$ -r n 
#$ -N filter_snp 
#$ -q b.q
#$ -pe smp 28 
#$ -cwd 
#$ -j y 
#$ -m abe 
#$ -M ashok.reddy.dinasarapu@emory.edu  

# PROJECT
PROJ_DIR=${HOME}/zwick_rare
PROJ_ID=TYEP0004

CALL_DIR=${PROJ_DIR}/${PROJ_ID}/CallingResults

cd ${CALL_DIR}

# FILTER
FILTER=${PROJ_DIR}/script/filter_genome.R
SIDS=sids.txt
SNP_IN=TYEP0004.merge.snp
SNP_OUT=TYEP0004.final.snp

Rscript ${FILTER} $SIDS $SNP_IN $SNP_OUT
sed -i -e 's/NA//g' $SNP_OUT
echo "Finish - `date`"
