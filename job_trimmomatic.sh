#!/bin/sh
echo "Start - `date`"
#$ -r n
#$ -N fastq_trim
#$ -q lh.q
##$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
##$ -pe mpi 60
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
## mkdir -p myProject/{src,doc/{api,system},tools,db}
if [ ! -d $HOME/microbiome/PTB_0816_trimmomatic ]; then
	mkdir -p $HOME/microbiome/PTB_0816_trimmomatic
fi

## Do the trimming!
# PE: paired-end data
# ILLUMINACLIP: Cut adapter & other illumina-specific sequences from the read
# SLIDINGWINDOW: Scan the read with a 4-base wide sliding window, cutting when the average quality per base drops below 15 
# LEADING: Cut bases off the start of a read, if below a threshold quality
# TRAILING: Cut bases off the end of a read, if below a threshold quality
# MINLEN: Drop the read if it is below a specified length
# /sw/eicc/Pkgs/Trimmomatic/0.36/adapters/TruSeq3-SE.fa
# NexteraPE-PE.fa, TruSeq2-PE.fa, TruSeq2-SE.fa, TruSeq3-PE-2.fa, TruSeq3-PE.fa, TruSeq3-SE.fa

module load Trimmomatic/0.36
$TRIMMOMATIC PE \
	-threads 4 \
	$HOME/microbiome/PTB_0816/MZW20791-1343.6.1_E0672-1-OraM1_S66_L001_R1_001.fastq.gz \
	$HOME/microbiome/PTB_0816/MZW20791-1343.6.1_E0672-1-OraM1_S66_L001_R2_001.fastq.gz \
	$HOME/PTB_0816_trimmomatic/MZW20791-1343.6.1_E0672-1-OraM1_S66_L001_R1_001.fastq.gz \
	$HOME/PTB_0816_trimmomatic/MZW20791-1343.6.1_E0672-1-OraM1_S66_L001_R2_001.fastq.gz \
	ILLUMINACLIP:/sw/eicc/Pkgs/Trimmomatic/0.36/adapters/TruSeq3-PE.fa:2:30:10 \
	LEADING:3 \
	TRAILING:3 \
	SLIDINGWINDOW:4:15 \
	MINLEN:36 
module unload Trimmomatic/0.36

echo "Finish - `date`"
