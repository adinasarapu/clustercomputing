#!/bin/sh

PROJ_DIR="$HOME/zwick_rare/geisert"

ls -lrt ~/zwick_rare/geisert/BWA/SL14857*/*[0-9].bam | \
	awk '{	\
	n=split($9,f,/\//); \
	st=substr(f[n],0,8); \
	print $9 "\t250\t" st \
}' > ${PROJ_DIR}/pindel/bam_config2.txt
