#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N STAR.Map
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=200:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 20
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

###############################################################################
## Cores = Cores per socket X Sockets (4 x 2 = 8 Cores)
## CPUs = Threads per core X Cores (2 x 8 = 16 CPUs/Processors)
## lscpu (2 sockets; 4 cores/ socket; 2 threads / core; i.e total 16 threads)
## lscpu | egrep 'Thread|Core|Socket|^CPU\('
## You need to be careful of the term CPU as it means different things in different contexts
###############################################################################

# https://github.com/ryanabo/rnaseq-analysis/blob/master/run_star.sh

PROJ_DIR='/home/adinasarapu/Projects/TCA21192'

:<<'END'
	sample_id             group_label
	SL151901              Sup_Arl13b
	SL151889              Control
	SL151902              Sup_Arl13b
	SL151896              Suppressed
	SL151893              Tumor
	SL151894              Suppressed
	SL151895              Suppressed
	SL151891              Tumor
	SL151890              Control
	SL151892              Control
	SL151900              Sup_Arl13b
	SL151899              Sup_Arl13b
	SL151898              Suppressed
	SL151897              Suppressed
END

BAM_INDEXING=True

module load STAR/2.5.2
# After STAR runs, you should have a few files in the /STAR_mm10/ directory:
# Genome, genomeParameters.txt, chrLength.txt, chrNameLength.txt,
# chrStart.txt, SA, SAindex, Log.out
:<<'END'
STAR --runMode genomeGenerate \
	--genomeDir ~/STAR_mm10/ \
	--genomeFastaFiles ~/iGenomes/Mus_musculus/UCSC/mm10/Sequence/WholeGenomeFasta/genome.fa \
	--runThreadN 16
END
# Alignment to genome (or transcriptome)
for postfix in 889 890 891 892 893 894 895 896 897 898 899 900 901 902
do
	OUT_DIR="${PROJ_DIR}/Out/SL151${postfix}"

	if [ ! -d ${OUT_DIR}/star_mapping ]; then
		mkdir -p ${OUT_DIR}/star_mapping
	fi

	# STAR will generate:
	# Log.out
	# Log.progress.out
	# Log.final.out – summary mapping statistics – will give you an idea on qc
	# Aligned.sortedByCoord.out.bam – sorted by coordinate – similar to samtools sort command
	# SJ.out.tab – high confidence splice junction in tab-delimited format
	# 
	# *** STAR supports multi-line FASTA, so if you convert your multi-line
	# FASTQ into FASTA, it should be OK ***.

	R1="${OUT_DIR}/fastqc/combined/SL151${postfix}_1_combined.fq.gz"
	R2="${OUT_DIR}/fastqc/combined/SL151${postfix}_2_combined.fq.gz"

	STAR --genomeDir ~/STAR_mm10/ \
		--readFilesIn ${R1} ${R2} \
		--runThreadN 20 \
		--readFilesCommand zcat \
		--sjdbGTFfile ~/iGenomes/Mus_musculus/UCSC/mm10/Annotation/Genes/genes.gtf \
		--outSAMtype BAM Unsorted SortedByCoordinate \
		--outFileNamePrefix "${OUT_DIR}/star_mapping/"
		
		# create bam index file 
		if [ "$BAM_INDEXING" = True ]; then
			module load samtools/1.3
			samtools index ${OUT_DIR}/star_mapping/Aligned.sortedByCoord.out.bam
			module unload samtools/1.3
		fi
done
module unload STAR/2.5.2
