#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N STARmap
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q b.q
#$ -l h_rt=200:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 28
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

###############################################################################
## Cores = Cores per socket X Sockets (4 x 2 = 8 Cores)
## CPUs = Threads per core X Cores (2 x 8 = 16 CPUs/Processors)
## lscpu (2 sockets; 4 cores/ socket; 2 threads / core; i.e total 16 threads)
## lscpu | egrep 'Thread|Core|Socket|^CPU\('
## You need to be careful of the term CPU as it means different things in different contexts
###############################################################################

# https://github.com/ryanabo/rnaseq-analysis/blob/master/run_star.sh

PROJ_DIR=$HOME/Hyder_Jinnah

BAM_INDEXING=True

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
        TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
        TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
        echo “Error. Cannot find mktemp to create tmp directory”
        exit
fi

# copy genome.ga into $TMPDIR
rsync -av ~/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa $TMPDIR/
rsync -av ~/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf $TMPDIR/

# After STAR runs, you should have a few files in the /STAR_mm10/ directory:
# Genome, genomeParameters.txt, chrLength.txt, chrNameLength.txt,
# chrStart.txt, SA, SAindex, Log.out

# STAR indexing of ref genome
# --genomeDir path to the directory where genome files are stored

chmod a+trwx $TMPDIR
/usr/bin/mkdir -p $TMPDIR/STAR_hg38

~/STAR/bin/Linux_x86_64/STAR --runMode genomeGenerate \
 --genomeDir $TMPDIR/STAR_hg38 \
 --genomeFastaFiles $TMPDIR/genome.fa \
 --runThreadN 26

# Alignment to genome (or transcriptome)
for ID in {76..87}; do
	SID=SL1537${ID}
	OUT_DIR=$TMPDIR/MAPPING/$SID/STAR
	if [ ! -d $OUT_DIR ]; then
		mkdir -p $OUT_DIR
	fi

	# STAR will generate:
	# Log.out
	# Log.progress.out
	# Log.final.out – summary mapping statistics – will give you an idea on qc
	# Aligned.sortedByCoord.out.bam – sorted by coordinate – similar to samtools sort command
	# SJ.out.tab – high confidence splice junction in tab-delimited format
	# 
	# *** STAR supports multi-line FASTA, so if you convert your multi-line
	# FASTQ into FASTA, it should be OK ***.

	rsync -av $PROJ_DIR/data/npc/dystonia/C957TANXX_{1..2}_GSLv3-7_${SID}.fastq.gz $TMPDIR/

	R1=$TMPDIR/C957TANXX_1_GSLv3-7_${SID}.fastq.gz
	R2=$TMPDIR/C957TANXX_2_GSLv3-7_${SID}.fastq.gz

	# --genomeDir path to the directory where genome files are stored
	# --readFilesIn paths to files that contain input read
	# --outSAMunmapped output of unmapped reads in the SAM format, None or Within SAM file
	# --outSAMunmapped output of unmapped reads in the SAM format, None or Within SAM file
	# --quantMode types of quantification requested, i.e. GeneCounts or TranscriptomeSAM
	# --twopassMode 2-pass mapping mode. 

	~/STAR/bin/Linux_x86_64/STAR --genomeDir $TMPDIR/STAR_hg38 \
	 --readFilesIn ${R1} ${R2} \
	 --runThreadN 26 \
	 --readFilesCommand zcat \
	 --sjdbGTFfile $TMPDIR/genes.gtf \
	 --outSAMtype BAM SortedByCoordinate \
	 --outFileNamePrefix $OUT_DIR/
		
	# create bam index file 
	if [ "$BAM_INDEXING" = True ]; then
	 module load samtools/1.5
	 samtools index $OUT_DIR/Aligned.sortedByCoord.out.bam
	 module unload samtools/1.5
	fi
	/bin/rm $TMPDIR/*.fastq.gz
done

/bin/rm -fr $TMPDIR/{genome.fa,genes.gtf}

# copy results
rsync -av $TMPDIR/MAPPING/* $PROJ_DIR/MAPPING/npc/dystonia/

# remove the temp directory
/bin/rm -fr $TMPDIR
