#!/bin/sh
# simply do the following single line at command (. is current dir)

# ln -s ~/ASM11398-16580/"Plate 1"/200*/*R0[0-9]C01_*.idat .
# ln -s ~/ASM11398-16580/"Plate 3"/200*/*R0[0-9]C01_*.idat .
# ln -s ~/ASM11398-16580/"Plate 4"/200*/*R0[0-9]C01_*.idat .
# ln -s ~/ASM11398-16580/"Plate 5"/200*/*R0[0-9]C01_*.idat .

# symlinks can't be accessed over network, these will only work wiht in system
# cp ~/ASM11398-16580/"Plate 5"/200*/*R0[0-9]C01_*.idat .

list=$(ls ~/Projects/TCA21192/SequenceData/11480/C92M7ANXX/C92M7ANXX_SL15*/htseq_count/*_s*/*_basename.counts)
for file in $list
do
	# target folder
	ln -s $file ~/Projects/TCA21192/htseq_count/
done
