# Downloaded TCGA lung adenocarcinoma expression matrix from the firehose website 

base.dir <- "/Users/adinasarapu/LUAD"

#=========================#
# 1. Read expression data #
#=========================#

expression_data <- read.table(file=paste(base.dir, "rnaseqv2_RSEM_genes_normalized_data.txt",sep="/"),
                              header = T, sep ='\t', 
                              stringsAsFactors = FALSE)

dim(expression_data)
# 20532x577

# solid tumor samples (N=515)
# These samples have "01" for the 14th and 15th characters in the id. 
sample_names <- colnames(expression_data)
sample_cols <- substr(sample_names, 14, 15) %in% "01"

# get gene id symbol column
sample_cols[1] <- TRUE
expression_data <- expression_data[-1,]
expression_selected <- expression_data[ , which(sample_cols)]

# rename first column column
colnames(expression_selected)[1] <- "Gene_Symbol"

gene <- data.frame(do.call('rbind', strsplit(as.character(expression_selected$Gene_Symbol),'|',fixed=TRUE)))
colnames(gene)[1:2] <- c("Symbol", "ID")
expression_selected <- cbind(gene, expression_selected)

expression_selected <- expression_selected[expression_selected$Symbol != "?",-c(2,3)]

# expression_selected[1:5,1:5]

# find duplicate Gene data, if present 
# expression_data[duplicated(expression_data$Symbol),1:9]
expression_selected[expression_selected$Symbol == "SLC35E2",1:9]

# select max values for all duplicated genes
expression_selected[] <- lapply(expression_selected, function(x) type.convert(as.character(x)))
genes.collapsed <- aggregate(. ~ Symbol, data = expression_selected, FUN = max)
# genes.collapsed[row.names(genes.collapsed) == "SLC35E2",1:9]
# 
row.names(genes.collapsed) <- genes.collapsed$Symbol
#genes.collapsed[1:5,1:5]
genes.collapsed$Symbol <- NULL
genes.transformed <- log2(genes.collapsed+1)
# genes.transformed[row.names(genes.transformed) == "SLC35E2",1:9]
#genes.transformed[1:5,1:5]
dim(genes.transformed)

#===========================================#
# 2. Determine lung adenocarcinoma subtype  #
#===========================================#
# wilkerson.2012.LAD.predictor.centroids.csv
# subtyper is a nearest centroid classifier with 3 centroids and 506 genes 
# gene   bronchioid      magnoid     squamoid
predictor.centroids <- read.table(file=paste(base.dir, "wilkerson.2012.LAD.predictor.centroids.csv",sep="/"),
                              header = T, sep =',', 
                              stringsAsFactors = FALSE)
row.names(predictor.centroids) <- predictor.centroids$gene
predictor.centroids$gene <- NULL

# dim(predictor.centroids)
luad.genes.final.centroids <- genes.transformed[row.names(genes.transformed) %in% row.names(predictor.centroids), ]
dim(luad.genes.final.centroids) # 489

file.luad.genes=paste(base.dir, "luad.genes.final.centroids.RData",sep="/")
save(luad.genes.final.centroids, file = file.luad.genes)

# Report which of the 506 are not there (answer: 17 genes). 
#luad.no.genes.centroids <- predictor.centroids[!row.names(predictor.centroids) %in% row.names(genes.transformed), ]
#row.names(luad.no.genes.centroids) # 506
#  "CDC2"          "DLG7"          "GGTLA4"        "PBEF1"         "HIG2"          "DKFZp762E1312"
# "DRAM"          "ATAD4"         "GLULD1"        "CXorf9"        "HMG4L"         "P15RS"        
# "RBM35A"        "DKFZP564O0823" "INDO"          "GOLSYN"        "LOC26010" 

predictor.final.centroids <- predictor.centroids[row.names(predictor.centroids) %in% row.names(genes.transformed), ]
dim(predictor.final.centroids) # 489

rowmed.all <- apply(genes.transformed,1,median)
genes.transformed.median <- genes.transformed - rowmed.all

# In the TCGA n=515 data set, center each gene using the gene median.  
rowmed <- apply(luad.genes.final.centroids,1,median)
luad.genes.final.median.centroids <- luad.genes.final.centroids - rowmed

# genes.transformed[row.names(genes.transformed) %in% c("A2M","AACS"),1:3]
genes.transformed.median[row.names(genes.transformed.median) %in% c("A2M","AACS"),1:3]
luad.genes.final.median.centroids[row.names(luad.genes.final.median.centroids) %in% c("A2M","AACS"),1:3]

#rowmed[1]
#luad.genes.final.median.centroids[1:5,1:3]

luad.genes.final.median.centroids <- as.matrix(luad.genes.final.median.centroids)
predictor.final.centroids <- as.matrix(predictor.final.centroids)

# head(luad.genes.final.median.centroids[,1:5])
# predictor.final.centroids[order(rownames(predictor.final.centroids)), ] 

# Calculate the Pearson correlation coefficient between each sample and each centroid using all available genes.
for(i in 1:dim(luad.genes.final.median.centroids)[2]){
  print(i)
  r <- cor(luad.genes.final.median.centroids[,i], predictor.final.centroids[,1:3], method="pearson")
  r <- as.data.frame(r)
  if(i == 1){
    r_next <- r
  }
  if(i > 1){
    r_next <- rbind(r_next,r)
  }
  row.names(r_next)[i] <- colnames(luad.genes.final.median.centroids)[i]
}
head(r_next)

file.corr.matrix=paste(base.dir, "subtype.centroids.corr.matrix.RData",sep="/")
save(r_next, file = file.corr.matrix)

# for each row, return the column name of the largest value
j1 <- max.col(r_next, "first")
value <- r_next[cbind(1:nrow(r_next), j1)]
cluster <- names(r_next)[j1]
subtype.centroids <- data.frame(value, cluster)
row.names(subtype.centroids) <- row.names(r_next)
# head(subtype.centroids)
out.subtype.centroids <- paste(base.dir, "subtype.centroids.classified.csv",sep="/")
write.csv(subtype.centroids, file = out.subtype.centroids)
#=============================================#
# 3. Association between T cells and subtype  #
#=============================================#
# calculate the T cells signature

# 19 genes in the T cells signature:
t.cell_genes <- c("PRKCQ", "CD3D", "CD3G", "CD28", "LCK", "TRAT1", 
                  "BCL11B", "CD2", "TRBC1", "TRAC", "ITM2A", "SH2D1A", "CD6", 
                  "CD96", "NCALD", "GIMAP5", "TRA", "CD3E", "SKAP1")

# Report any genes in the signature that weren't used because they weren't in TCGA.

# genes.expr <- genes.transformed
genes.expr <- genes.transformed.median
# genes.expr <- luad.genes.final.median.centroids

dim(genes.expr)

t.cell_gene_expr <- genes.expr[row.names(genes.expr) %in% t.cell_genes, ]
dim(t.cell_gene_expr) 

# 3 genes are not present; "TRBC1" "TRAC"  "TRA" 
# t.cell_genes[!t.cell_genes %in% row.names(genes.transformed)]

t.cell_signature_expr <- as.data.frame(colMeans(t.cell_gene_expr))
head(t.cell_signature_expr)

subtype_t.signature <- merge(t.cell_signature_expr,subtype.centroids, by="row.names")
colnames(subtype_t.signature) <- c("Sample","t.signature","corr_max","subtype")

row.names(subtype_t.signature) <- subtype_t.signature$Sample
subtype_t.signature <- subtype_t.signature[,c("t.signature","subtype")]
head(subtype_t.signature)

# Plot T cells signature (y-axis) vs subtype (x-axis) using boxplots in a pdf.  
library(ggplot2)
# Convert the variable subtype to a factor variable, if not

out.subtype.assoc <- paste(base.dir, "subtype.association.pdf",sep="/")
pdf(out.subtype.assoc,width=7,height=5)
p <- ggplot(subtype_t.signature, aes(x=subtype, y=t.signature)) + 
  geom_boxplot(outlier.colour="red", outlier.shape=8,
               outlier.size=1)
p
dev.off()

# Test whether subtype is associated with T cells using the Kruskal-Wallis test 
# and report the p-value only.
kruskal.test(t.signature ~ subtype, data = subtype_t.signature) 
getwd()
