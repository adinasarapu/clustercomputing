# age_data <- age_data1
# indo_mn_data <- indo_mn_data
# hai_data <- hai_data
# elispot_data <- elispot_data
# tfh_flow_data <- tfh_flow_data

getOnlylog2BaseNormData_df <- function(age_data, indo_mn_data, hai_data, elispot_data, tfh_flow_data){

    if(!is.null(indo_mn_data)){
    
    col_mn <- colnames(indo_mn_data)
    col_len_mn <- length(col_mn)
    col_req <- c(2:col_len_mn)
    
    mn_data_base_norm <- indo_mn_data[,col_req]-indo_mn_data[,1]
    colnames(mn_data_base_norm) <- paste0("MN_",col_mn[col_req]) # c("MN_D21","MN_D42","MN_D100")
  }
  if(!is.null(hai_data)){
    
    col_mn <- colnames(hai_data$Average)
    col_len_mn <- length(col_mn)
    col_req <- c(2:col_len_mn)
    
    hai_data_base_norm <- hai_data$Average[,col_req]-hai_data$Average[,1]
    colnames(hai_data_base_norm) <-  paste0("HI_",col_mn[col_req]) # c("HI_D21","HI_D42","HI_D100")
  }
  if(!is.null(mn_data_base_norm) & !is.null(hai_data_base_norm)){
    mn_hi_merged <- merge(mn_data_base_norm, hai_data_base_norm, by="row.names",all.y = T)
    row.names(mn_hi_merged) <- mn_hi_merged$Row.names
    mn_hi_merged$Row.names <- NULL
  }
  if(!is.null(mn_hi_merged) & !is.null(age_data)){
    age_mn_hai <- age_data[,c("D0"), drop=FALSE]
    colnames(age_mn_hai) <- c("Age_D0")
    mn_hai_age <- merge(age_mn_hai, mn_hi_merged, by="row.names", all.y = T)
    row.names(mn_hai_age) <- mn_hai_age$Row.names
    mn_hai_age$Row.names <- NULL
  }
  if(!is.null(mn_hai_age) & !is.null(elispot_data)){
    mn_hai_plasmablast_age <- merge(mn_hai_age, elispot_data, by="row.names", all.y = T)
    row.names(mn_hai_plasmablast_age) <- mn_hai_plasmablast_age$Row.names
    mn_hai_plasmablast_age$Row.names <- NULL
  }
  if(!is.null(mn_hai_plasmablast_age)){
    # combining all Tfh cell frequencies
    tfh_all <- data.frame(dummynames=row.names(mn_hai_plasmablast_age),stringsAsFactors=FALSE)
    row.names(tfh_all) <- row.names(mn_hai_plasmablast_age)
    for(p in 1:length(tfh_flow_data)){
      tfh_frq <- tfh_flow_data[[p]]
      
      col_mn <- colnames(tfh_frq)
      col_len_mn <- length(col_mn)
      col_req <- c(2:col_len_mn)
      
      tfh_frq_norm <- tfh_frq[,col_req] - tfh_frq[,1]
      tfh_all <- merge(tfh_all, tfh_frq_norm, by="row.names")
      row.names(tfh_all) <- tfh_all$Row.names
      tfh_all$Row.names <- NULL
    }
    tfh_all$dummynames <- NULL
    mn_hai_plasmablast_tfh_age <- merge(mn_hai_plasmablast_age, tfh_all, by="row.names", all.y = T)
    row.names(mn_hai_plasmablast_tfh_age) <- mn_hai_plasmablast_tfh_age$Row.names
    mn_hai_plasmablast_tfh_age$Row.names <- NULL
  }
  #mn_hai_plasmablast_tfh_age
  #mn_hai_plasmablast_tfh_age$HI_D21 <- NULL
  #mn_hai_plasmablast_tfh_age$HI_D100 <- NULL
  return (mn_hai_plasmablast_tfh_age)
}
#tfh_data <- tfh_flow_data
mergeTFHData <- function(tfh_data){
  ids <- row.names(tfh_data[[1]])
  tfh_all <- data.frame(dummynames=ids,stringsAsFactors=FALSE)
  row.names(tfh_all) <- ids
  for(p in 1:length(tfh_data)){
    # p = 1
    tfh_frq <- tfh_data[[p]]
    
    col_mn <- colnames(tfh_frq)
    col_len_mn <- length(col_mn)
    col_req <- c(2:col_len_mn)
    
    tfh_frq_norm <- tfh_frq[,col_req] - tfh_frq[,1]
    tfh_all <- merge(tfh_all, tfh_frq_norm, by="row.names")
    row.names(tfh_all) <- tfh_all$Row.names
    tfh_all$Row.names <- NULL
  }
  tfh_all$dummynames <- NULL
  names(tfh_all) <- names(tfh_data)
  return (tfh_all)
}

merge_lists_of_common_rowids <- function(list_data, normalize, num_cols){
  #list_data <- nes_data
  #normalize <- FALSE
  #names(list_data)
  #num_cols = 1
  ids <- row.names(list_data[[1]])
  day_list <- colnames(list_data[[1]])
  
  list_all <- data.frame(dummynames=ids,stringsAsFactors=FALSE)
  row.names(list_all) <- ids
  q = num_cols
  for(p in 1:length(list_data)){
    # p = 1
    list_frq <- list_data[[p]]
    if(normalize & length(day_list) == 2){
      
      col_mn <- colnames(list_frq)
      col_len_mn <- length(col_mn)
      col_req <- c(2:col_len_mn)
      #??merge
      list_frq_norm <- list_frq[,col_req] - list_frq[,1]
      list_all <- merge(list_all, list_frq_norm, by="row.names", all=TRUE)
      row.names(list_all) <- list_all$Row.names
      list_all$Row.names <- NULL
      if(p == 1){
        list_all$dummynames <- NULL
      }
      names(list_all)[p] <- paste(names(list_data)[p],paste(day_list,collapse="_"),sep="_")
    } else{
      list_all <- merge(list_all, list_frq, by="row.names", all=TRUE)
      if(p == 1){
        list_all$dummynames <- NULL
      }
      row.names(list_all) <- list_all$Row.names
      list_all$Row.names <- NULL
      if(num_cols > 1){
        names(list_all)[1:q] <- as.vector(t(outer(names(list_data)[1:p], day_list, paste,sep="_")))
        #q = q+4
        q = q+1
      }
    }
  }
  #
  return (list_all)
}