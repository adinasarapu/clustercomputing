#!/bin/sh

# Plugin error from demux: Invalid DISPLAY variable
# run the following command  
# echo "backend : agg" > ~/.config/matplotlib/matplotlibrc

echo "Start - `date`" 
#$ -N step1
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M <email>@emory.edu

source activate qiime2-2018.2

PROJ_DIR=$HOME/microbiome/LHA11561
QIIME2_DIR=$PROJ_DIR/qiime2_2018_2
OUT_DIR=$QIIME2_DIR/plate2_data_imported

if [ ! -d ${OUT_DIR} ]; then
 mkdir -p ${OUT_DIR}
fi

export TMPDIR=$HOME/tmp

if [ -e /bin/mktemp ]; then
 TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

FASTQ_DIR=$PROJ_DIR/Plate2
rsync -av $FASTQ_DIR/[!Undetermined]*_R[1-2]_*.fastq.gz $TMP_DIR

ls -la $TMP_DIR/

# manifest.txt file format
# sample-id,absolute-filepath,direction
echo -e "sample-id,absolute-filepath,direction" >> $TMP_DIR/manifest.txt

for entry in $TMP_DIR/*.fastq.gz; do
 fname=$(basename $entry)
 fbname=${fname%_S*}
 fcount=`ls $TMP_DIR/ | grep $fbname | wc -l`
 echo $fbname $fcount
 if [[ ${fcount} == 2 ]]; then
  # Don't use underscores (_) in sample-id of manifest file 
  #fbname=`echo $fbname | sed 's/_/-/'`
  fpre="$fbname,$TMP_DIR/$fname"
  if [[ $entry == *"_R1_"* ]]; then
   echo -e "$fpre,forward" >> $TMP_DIR/manifest.txt
  fi
  if [[ $entry == *"_R2_"* ]]; then
   echo -e "$fpre,reverse" >> $TMP_DIR/manifest.txt
  fi
 fi
done

#=====================================================#
# Import FASTQ files to create a new QIIME 2 Artifact #
#=====================================================#
# --type		: The semantic type of the artifact that will be created upon importing
# --source-format	: The format of the data to be imported

qiime tools import \
 --type SampleData[PairedEndSequencesWithQuality] \
 --input-path $TMP_DIR/manifest.txt \
 --output-path $TMP_DIR/paired-end-data.qza \
 --source-format PairedEndFastqManifestPhred33

#===============================================#
# Summarize counts per sample for all samples	# 
# Plot positional qualitites 			#
#===============================================#

qiime tools validate \
 --level max $TMP_DIR/paired-end-data.qza > $TMP_DIR/paired-end-data-validation.txt
 
qiime demux summarize \
 --i-data $TMP_DIR/paired-end-data.qza \
 --o-visualization $TMP_DIR/imported_data_qualities.qzv

/bin/rm $TMP_DIR/*.fastq.gz
rsync -av $TMP_DIR/* $OUT_DIR
/bin/rm -rf $TMP_DIR

source deactivate qiime2-2018.2

echo "Finish - `date`"
