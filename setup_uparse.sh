#!/bin/bash
usearch
if [ usearch == x ] ; then
	echo Must set \$usearch >> /dev/stderr
	exit 1
fi

mkdir -p ../utax

cd ../utax

# wget http://drive5.com/utax/data/utax_rdp_16s_tainset15.tar.gz
curl -O http://drive5.com/utax/data/utax_rdp_16s_tainset15.tar.gz

tar -zxvf utax_rdp_16s_tainset15.tar.gz

usearch -makeudb_utax utaxref/rdp_16s_trainset15/fasta/refdb.fa \
  -taxconfsin utaxref/rdp_16s_trainset15/taxconfs/500.tc \
  -output utax_16s_500.udb -log makeudb_utax.log
