#!/bin/sh

echo "Start - `date`"
## name for the job 
#$ -N MapQC
#$ -q all.q
#$ -l h_rt=220:00:00
#$ -pe smp 20
#$ -cwd
#$ -j y
#$ -m abe
#$ -M xyz@emory.edu

PROJ_DIR=$HOME/Projects/RNA_TCA21192

# BED file is tab separated, 12 column, plain text file to represent gene model.	
BED_FILE=$HOME/bed_files/mm10_UCSC_knownGene.bed

FLAG_STAT=true
RSeQC_STAT=true

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
        TMPDIR=`/bin/mktemp -d -p /tmp/` || exit
elif [ -e /usr/bin/mktemp ]; then
        TMPDIR=`/usr/bin/mktemp -d –p /tmp/` || exit
else
        echo “Error. Cannot find mktemp to create tmp directory”
        exit
fi

module load samtools/1.3

for ID in {889..902}
do
	SID=SL151${ID}

	/usr/bin/mkdir -p $TMPDIR/$SID

	rsync -av $PROJ_DIR/MAPPING/$SID/STAR/Aligned.sortedByCoord.out* $TMPDIR/$SID/

	MAP_DIR=$TMPDIR/$SID

	OUT_DIR=$TMPDIR/MAPQC/${SID}
	BAM_FILE=$MAP_DIR/Aligned.sortedByCoord.out.bam

	if [ ! -d ${OUT_DIR} ]; then
	 mkdir -p ${OUT_DIR}
	fi

	#############
	# Samtools  #
	#############
	if [ "$FLAG_STAT" = true ]; then
	 # Indexing BAM file
	 samtools index ${BAM_FILE}
	 # No. of reads (mapped and unmapped)
	 # run additional tools for counts to specific FLAG values
	 samtools view ${BAM_FILE} | wc -l > ${OUT_DIR}/SL151${ID}_read_count_total.txt
	 # assesses the information from the FLAG field
	 samtools flagstat ${BAM_FILE} > ${OUT_DIR}/SL151${ID}_flagstat.txt
	fi

	############################################	
	# RSeQC: An RNA-seq Quality Control Package#
	############################################
	# pip install RSeQC
	# -i 
	# -q 30 (default) Min mapping quality to determine uniquely mapped read
	# -o OUTPUT_PREFIX
	# -l 100 READ_ALIGNMENT_LENGTH
	# -s

	# This script determines "uniquely mapped reads" from the "NH" tag in
	# BAM/SAM file (please note "NH" is an optional tag, some aligner does
	# NOT provide this tag
	if [ "$RSeQC_STAT" = true ]; then

	 map_quality=30
	 bam_stat.py -i ${BAM_FILE} -q $map_quality > ${OUT_DIR}/SL151${ID}_${map_quality}_read_count_uniq.txt
	
	# Calculate the distributions of clipped nucleotides across reads
	 clipping_profile.py -i ${BAM_FILE} -s "PE" -o ${OUT_DIR}/SL151${ID}

	# Calculate the distributions of deletions across reads
	 deletion_profile.py -i ${BAM_FILE} -l 100 -o ${OUT_DIR}/SL151${ID}
	
	# Calculate the distributions of inserted nucleotides across reads.
	# Note: CIGAR strings within SAM/BAM file should have ‘I’ operation
	 insertion_profile.py -s "PE" -i ${BAM_FILE} -o ${OUT_DIR}/SL151${ID}
	#
	 read_distribution.py -i ${BAM_FILE} -r ${BED_FILE} > ${OUT_DIR}/SL151${ID}_knownGene_read_distribution.txt

	# Calculate the RNA-seq reads coverage over gene body
	# Only input sorted and indexed BAM file(s)
	 geneBody_coverage.py -r ${BED_FILE} -i ${BAM_FILE} -o ${OUT_DIR}/SL151${ID}_knownGene_bodyCoverage
	
	# Calculate inner distance between read pairs
	 inner_distance.py -r ${BED_FILE} -i ${BAM_FILE} -o ${OUT_DIR}/SL151${ID}_knownGene_innerDistance

	# Phred Quality Score = ord(Q) - 33
	# Q is the character to represent “base calling quality” in SAM file
	# ord() is python function that returns an integer representing the
	# Unicode code point of the character when the argument is a unicode
	# object, for example, ord(‘a’) returns 97. 
	 read_quality.py -i ${BAM_FILE} -o ${OUT_DIR}/SL151${ID}_readQuality
	
	# Calculate fragment size for each gene/transcript.
	 RNA_fragment_size.py -r ${BED_FILE} -i ${BAM_FILE} > ${OUT_DIR}/SL151${ID}_RNA_fragSize.txt
	
	# Calculate raw read count, FPM (fragment per million) and FPKM
	# (fragment per million mapped reads per kilobase exon) for each gene
	# in BED file
	 FPKM_count.py -r ${BED_FILE} -i ${BAM_FILE} -o ${OUT_DIR}/SL151${ID}_fpkm
	fi
	/bin/rm -fr $TMPDIR/$SID
done

module unload samtools/1.3

# copy results
rsync -av $TMPDIR/* $PROJ_DIR/
/bin/rm -fr $TMPDIR

# Fetch chromsome size file from UCSC
# ./fetchChromSizes mm10 > ${GRP_QC_DIR}/mm10.chrom.sizes

# http://rseqc.sourceforge.net

# Phred quality score is widely used to measure “reliability” of base-calling,
# for example, phred quality score of 20 means there is 1/100 chance that the
# base-calling is wrong, phred quality score of 30 means there is 1/1000
# chance that the base-calling is wrong. In general: 

# Phred quality score = -10xlog(10)P, here P is probability that base-calling is wrong.
