#!/bin/sh

##There are three alignment algorithms in BWA: `mem', `bwasw', and 
## `aln/samse/sampe'. If you are not sure which to use, try `bwa mem' first.
## bwa (alignment via Burrows-Wheeler transformation)
## mem (BWA-MEM algorithm)

echo "Start - `date`" 
#$ -N mpileup
#$ -q b.q
##$ -l h_rt=220:00:00
#$ -pe smp 28
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PID=TYEP0004
PROJ_DIR="$HOME/zwick_rare"
REF_DIR="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/BWAIndex"

module load bwa/0.7.12
module load samtools/1.3
module load bcftools/1.3
module load picardtools/2.6.0
module load bedtools/2.25.0

BWA_DIR=${PROJ_DIR}/${PID}/BWA

for i in {5..7}; do

	# create a unique folder on the local compute drive
 	if [ -e /bin/mktemp ]; then
		TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
	elif [ -e /usr/bin/mktemp ]; then
		TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
	else
		echo “Error. Cannot find mktemp to create tmp directory”
		exit
	fi

	DATA_DIR=${PROJ_DIR}/${PID}/SL15631${i}

	OUT_DIR=${BWA_DIR}/SL15631${i}
	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}
	fi

	R_1=`ls -la ${DATA_DIR}/*_1_* | awk '{print $9}'`
	R_2=`ls -la ${DATA_DIR}/*_2_* | awk '{print $9}'`
	
	cp ${R_1} ${TMPDIR}
	cp ${R_2} ${TMPDIR}

	echo "bwa mem -t 28 ${REF_DIR}/genome.fa \
		${TMPDIR}/*_1_* ${TMPDIR}/*_2_* \
		> ${TMPDIR}/SL15631${i}.sam"
	
	bwa mem -t 28 \
		${REF_DIR}/genome.fa \
		${TMPDIR}/*_1_* \
		${TMPDIR}/*_2_* > ${TMPDIR}/SL15631${i}.sam

	# BWA prints total CPU and wall-clock time at the end of the run. 
	# If you don't see it, the output is very likely to be incomplete.
	
	samtools view -@ 28 \
		-bS ${TMPDIR}/SL15631${i}.sam > ${TMPDIR}/SL15631${i}.bam

	samtools sort -@ 28 \
		-T ${TMPDIR}/SL15631${i}.sorted \
		-o ${TMPDIR}/SL15631${i}.sorted.bam ${TMPDIR}/SL15631${i}.bam

	$PICARD MarkDuplicates \
		MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
		METRICS_FILE=${TMPDIR}/SL15631${i}.metrics \
		REMOVE_DUPLICATES=true \
		ASSUME_SORTED=true \
		VALIDATION_STRINGENCY=LENIENT \
		INPUT=${TMPDIR}/SL15631${i}.sorted.bam \
		OUTPUT=${TMPDIR}/SL15631${i}.dedupe.bam
	
	samtools index ${TMPDIR}/SL15631${i}.dedupe.bam
	
	# delete raw data from TMPDIR	
	/bin/rm ${TMPDIR}/*.fastq.gz

	# copy results from TMPDIR
	cp ${TMPDIR}/* ${OUT_DIR}
	
	# delete TMPDIR
	/bin/rm -rf ${TMPDIR}
done

echo `ls ${BWA_DIR}/SL15631*/*.dedupe.bam` | \
	tr " " "\n" > ${BWA_DIR}/bam_list.txt

###########################################
# call raw variants with mpileup+bcftools #
###########################################
# -o write output to FILE [standard output]
# -g generate genotype likelihoods in BCF format
# -u generate uncompressed VCF/BCF output, otherwise mpileup file will be produced
# -f faidx indexed reference sequence file 
# -E recalculate BAQ (per-Base Alignment Quality) on the fly, ignore existing BQs

samtools mpileup -E \
	-uf ${REF_DIR}/genome.fa \
	-b ${BWA_DIR}/bam_list.txt \
	-go ${BWA_DIR}/${PID}.bcf

# tabix (samtools) indexes position sorted files in TAB- delimited formats 
# such as GFF, BED, PSL, SAM, VCF. Useful for manually examining local genomic 
# features on the command line  

tabix ${BWA_DIR}/${PID}.bcf

# --skip-variants indels (report only SNPs)
# -Oz compressed VCF output
# -v output variant sites only
# -m alternative model for multiallelic and rare-variant calling

bcftools call \
	-vm -Oz \
	-o ${BWA_DIR}/${PID}.vcf.gz ${BWA_DIR}/${PID}.bcf

# -p, --preset [gff, bed, sam, vcf]
tabix -p vcf ${BWA_DIR}/${PID}.vcf.gz

# Extracting information from VCFs
# https://samtools.github.io/bcftools/howtos/query.html

module unload picardtools/2.6.0
module unload bwa/0.7.12
module unload samtools/1.3
module unload bcftools/1.3
module unload bedtools/2.25.0
