#!/bin/sh
echo "Start - `date`"
#$ -r n
#$ -N snpEff.4
#$ -q lh.q
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# SnpEff is a variant annotation and effect prediction tool. 
# It annotates and predicts the effects of genetic variants (such as amino acid changes).

# A typical SnpEff use case would be:
#	Input: The inputs are predicted variants (SNPs, insertions, deletions and MNPs). 
#		The input file is usually obtained as a result of a sequencing experiment, 
#		and it is usually in variant call format (VCF).
#	Output: SnpEff analyzes the input variants. It annotates the variants and calculates 
#		the effects they produce on known genes (e.g. amino acid changes).

# TLA20883-060315_Pilot1_Promega
# TLA20883-061316_Pilot3_Promega

HOME='/home/adinasarapu/tim_lash/TLA20883-061316_Pilot3_Promega/Out/BWA'
# ls -la ${HOME}

file1='TLA20883-Promega-Control-150ng_S347_L001/TLA20883-Promega-Control-150ng_S347_L001.vcf.gz'
file2='TLA20883-Promega-Control-50ng_S348_L001/TLA20883-Promega-Control-50ng_S348_L001.vcf.gz'
file3='TLA20883-Promega-Control-50ng_S346_L001/TLA20883-Promega-Control-50ng_S346_L001.vcf.gz'
file4='TLA20883-Promega-Control-150ng_S349_L001/TLA20883-Promega-Control-150ng_S349_L001.vcf.gz'

#file1='Female-10ng-Promega-Control_S6_L001/Female-10ng-Promega-Control_S6_L001.vcf.gz'
#file2='Female-50ng-Promega-Control_S4_L001/Female-50ng-Promega-Control_S4_L001.vcf.gz'
#file3='Male-25ng-Promega-Control_S2_L001/Male-25ng-Promega-Control_S2_L001.vcf.gz'
#file4='Female-25ng-Promega-Control_S5_L001/Female-25ng-Promega-Control_S5_L001.vcf.gz'
#file5='Male-10ng-Promega-Control_S3_L001/Male-10ng-Promega-Control_S3_L001.vcf.gz'
#file6='Male-50ng-Promega-Control_S1_L001/Male-50ng-Promega-Control_S1_L001.vcf.gz'

# module display snpEff
# $SNPEFF databases | grep -i 'Homo_sapiens'
# A list of databases is available in snpEff.config file.
# Currently downloaded databases are at /sw/hgcc/Pkgs/snpEff/4.2/data
# Download a specific databases as 
# $SNPEFF download GRCh38.82

filename=$(basename "$file4")
# extension="${filename##*.}"
filename="${filename%.*}"

module load snpEff/4.2
$SNPEFF -v -stats ${HOME}/${filename}.html GRCh38.82 ${HOME}/${file4} > ${HOME}/${filename}.ann.vcf
module unload snpEff/4.2
echo "Finish - `date`"
