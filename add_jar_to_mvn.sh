#!/bin/sh
# How to build maven project with propriatery libraries
mvn deploy:deploy-file \
	-DgroupId=org.broad.igv \
	-DartifactId=igv \
	-Dversion=2.3.23 \
	-Durl=file:/home/adinasarapu/TrioCNV/local-maven-repo/ \
	-DrepositoryId=local-maven-repo \
	-DupdateReleaseInfo=true \
	-Dfile=/home/adinasarapu/TrioCNV/jar/IGVTools/igvtools.jar
mvn deploy:deploy-file \
	-DgroupId=rcaller \
	-DartifactId=RCaller \
	-Dversion=2.5 \
	-Durl=file:/home/adinasarapu/TrioCNV/local-maven-repo/ \
	-DrepositoryId=local-maven-repo \
	-DupdateReleaseInfo=true \
	-Dfile=/home/adinasarapu/TrioCNV/jar/RCaller-2.5.jar
