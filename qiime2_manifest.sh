#!/bin/sh
: <<'END'
The manifest file is a comma-separated (i.e., .csv) text file. The first field
on each line is the sample identifier that should be used by QIIME, the second
field is the absolute filepath, and the third field is the read direction.

sample-id,absolute-filepath,direction
sample1,$PWD/pe-64/s1-phred64-r1.fastq.gz,forward
sample1,$PWD/pe-64/s1-phred64-r2.fastq.gz,reverse
sample2,$PWD/pe-64/s2-phred64-r1.fastq.gz,forward
sample2,$PWD/pe-64/s2-phred64-r2.fastq.gz,reverse
END

PROJ_DIR=/home/adinasarapu/microbiome
QIIME2_DIR=${PROJ_DIR}/qiime2
FASTQ_DIR=${PROJ_DIR}/PTB_0816

echo -e "sample-id,absolute-filepath,direction" \
	>> ${QIIME2_DIR}/ptb_manifest
for entry in "$FASTQ_DIR"/*
do
	fname=$(basename $entry)
	fbname=${fname%_S*}
	fcount=`ls $FASTQ_DIR/ | grep $fbname* | wc -l`
	if [[ ${fcount} == 2 ]]; then
		fbname=`echo $fbname | sed 's/_/-/'`
		fpre="$fbname,$HOME/microbiome/PTB_0816/$fname"
		if [[ $entry == *"_R1_"* ]]; then
			echo -e "$fpre,forward" >> ${QIIME2_DIR}/ptb_manifest
		fi
		if [[ $entry == *"_R2_"* ]]; then
			echo -e "$fpre,reverse" >> ${QIIME2_DIR}/ptb_manifest
		fi
	fi
done
