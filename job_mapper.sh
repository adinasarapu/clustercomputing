#!/bin/sh

# PEmapper: a reference-based NGS short-read mapping program.
# PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).

# SGE
MAX_THREADS=60
QNAME=all.q
PE=smp
E_MAIL="ashok.reddy.dinasarapu@emory.edu"

# PROJECT
PROJ_DIR=/home/adinasarapu/zwick_rare
SID=SL156317

DATA_DIR=${PROJ_DIR}/TYEP0004/${SID}
OUT_DIR=${PROJ_DIR}/TYEP0004/MappingResults/${SID}

FWD_FILE=HT273CCXX_s7_1_GSLv3-7_47_SL156317.fastq.gz
REV_FILE=HT273CCXX_s7_2_GSLv3-7_47_SL156317.fastq.gz

# PEMapper
SEQ_READS=pa
MAX_DIST=500
MIN_DIST=0
IS_BISULFITE=N
min_match_percentage=0.95
MAX_READS=2000000000

MAPPER=$PROJ_DIR/script/pemapper

# indexed ref genome 
SDX=$PROJ_DIR/hg38/hg38.sdx

if [ ! -d "$OUT_DIR" ]; then
        mkdir -p "$OUT_DIR"
fi

ln -s ${DATA_DIR}/${FWD_FILE} ${OUT_DIR}/${FWD_FILE}
ln -s ${DATA_DIR}/${REV_FILE} ${OUT_DIR}/${REV_FILE}

FILE1=${OUT_DIR}/${SID}.file1.txt
FILE2=${OUT_DIR}/${SID}.file2.txt

if [ ! -e $FILE1 ]; then
  echo ${FWD_FILE} >> $FILE1
fi

if [ ! -e $FILE2 ]; then
  echo ${REV_FILE} >> $FILE2
fi

echo "#!/bin/sh
echo \"Start - \`date\`\"
${MAPPER} \
	${SID} \
	${SDX} \
	${SEQ_READS} \
	${SID}.file1.txt \
	${SID}.file2.txt \
	${MAX_DIST} ${MIN_DIST} ${IS_BISULFITE} \
	${min_match_percentage} ${MAX_THREADS} ${MAX_READS}
echo \"Finish - \`date\`\"" >> ${OUT_DIR}/${SID}.sh

cd ${OUT_DIR}

qsub -v USER -v PATH -cwd \
	-q ${QNAME} \
	-pe ${PE} ${MAX_THREADS} \
	-M ${E_MAIL} \
	-N ${SID} \
	-m abe \
	-j y \
	${SID}.sh
