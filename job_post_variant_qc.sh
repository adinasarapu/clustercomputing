#!/usr/bin/perl 

:<<'COMMENTS'
Descriptive statistics and quality control on sample & variants

References:
 1. https://github.com/WheelerLab/GWAS_QC/blob/master/example_pipelines/TCS_GWAS_QC/02_plink_QC.sh
 2. https://github.com/googlegenomics/codelabs/blob/master/R/PlatinumGenomes-QC/Variant-Level-QC.md
 3. http://apol1.blogspot.com/2014/11/best-practice-for-converting-vcf-files.html
 4. https://www.staff.ncl.ac.uk/heather.cordell/pak2010MDS.html

Identify and remove individuals with 
 1. Discordant sex information
 2. High missingness and outlying heterozygosity rate
 3. Duplicated and related
 4. Divergent ancestry
 
Required INPUT files: 
 1. PEMapper/PECaller based joint variant calling results (.merged.snp file)
 2. Bystro annotated merged.snp file
 3. .ped (PECaller compatible) and .fam (plink compatible) files
 4. hg38.sdx and hg38.seq files
 5. Dave Cutler's Script/bin directory PATH
 6. plink and king binary files
 7. VCFheaders.txt file

Pedigree/phenotype information:

Dave's ped file format:
A text file with no header line, and one line per sample with the following six fields:
 1. SLID	
 2. FID	
 3. IID	
 4. PID	
 5. MID	
 6. SEX	
 7. AFFECT_STAT

Plink's fam file format: 
A text file with no header line, and one line per sample with the following six fields:
 1. Family ID ('FID')
 2. Within-family ID ('IID'; cannot be '0')
 3. Within-family ID of father ('0' if father isn't in dataset)
 4. Within-family ID of mother ('0' if mother isn't in dataset)
 5. Sex code ('1' = male, '2' = female, '0' = unknown)
 6. Phenotype value ('1' = control, '2' = case, '-9'/'0'/non-numeric = missing data if case/control)

PLINK offers a more efficient binary format, which uses fam(ily), bim (binary mapping) and bed (binary pedigree) files
 1. bed file is a matrix of 0s, 1s, 2s or NAs stored in binary format
 2. bim file is a map file plus two columns, providing the A1 and A2 alleles
 3. fam file is first six columns of ped file

PLINK uses the following two-bit coding of genotypes
 1. 00 = A1/A1 (Homozygous non-reference)
 2. 01 = A1/A2 (Heterozygous)
 3. 11 = A2/A2 (Homozygous reference)
 4. 10 = 0/0 (Missing)

COMMENTS

echo "Start - `date`" 
#$ -r n 
#$ -N var.qc
#$ -q b.q
#$ -pe smp 2
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu 

# Project name
PID=TYEP0004

# PEMapper/PECaller/Bystro was used for read mapping, joint calling and variant annotation, respectively.
PE_Caller=true

# if it is trio based data
TRIO=true

PROJ_DIR=$HOME/zwick_rare
SCRIPT_DIR=$PROJ_DIR/script

# create a unique directory in TMPDIR
if [ -e /bin/mktemp ]; then
 TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
 TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
 echo “Error. Cannot find mktemp to create tmp directory”
 exit
fi

# make sub-directories in TMPDIR
chmod a+trwx $TMPDIR
/bin/mkdir -p $TMPDIR/{pecaller,plink,king}

OUT=$PROJ_DIR/$PID/PostCallQC/PE.ASHOK

if [ "$PE_Caller" = true ]; then
 #========================#
 # PEMapper/PECaller - QC #
 #========================#
 CALL_OUT=$PROJ_DIR/$PID/CallingResults_old
 SNP=$CALL_OUT/${PID}.merge.snp 
 PED=$PROJ_DIR/$PID/${PID}.ped
 SEQANT=$PROJ_DIR/$PID/SeqAnt
 # the annotation file for the merged .snp file 
 ANNOT=$SEQANT/${PID}_merge.annotation.tsv

 echo "copying data to $TMPDIR"
 rsync -av $SNP $TMPDIR/${PID}.snp
 rsync -av $ANNOT $TMPDIR/${PID}.annotation.tsv
 rsync -av $PROJ_DIR/hg38/hg38.{sdx,seq} $TMPDIR/
 rsync -av $HOME/zwick_rare/VCFheaders.txt $TMPDIR/pecaller/

 echo "`ls -la $TMPDIR | awk '{print $9}'`"
 
 # filter calls 
 threshold_to_call=0.95
 call_rate=0.9

 echo "Transition:Transversion ratio ... (Ti/Tv, sometimes called Ts/Tv"
 $SCRIPT_DIR/snp_tran_counter_site.pl $TMPDIR/${PID}.snp > $TMPDIR/pecaller/${PID}.ts_tv.txt 
 
 echo "non-synonymous:synonymous ratios ..."
 $SCRIPT_DIR/snp_tran_silent_rep_site.pl $TMPDIR/${PID}.snp \
  $TMPDIR/${PID}.annotation.tsv SNP > $TMPDIR/pecaller/${PID}.ns_ss.details.txt  

 echo "filtering SNPs ..."
  $SCRIPT_DIR/snp_filter_call_rate.pl $TMPDIR/${PID}.snp $threshold_to_call $call_rate > $TMPDIR/pecaller/${PID}.95_90.snp

 echo "making vcf file ..."
 # Usage: ./snp_to_vcf2 sdx_file snpfile [min_prob_to_make_call] 
 $SCRIPT_DIR/snp_to_vcf2 $TMPDIR/hg38.sdx $TMPDIR/${PID}.snp 0.95 > $TMPDIR/pecaller/${PID}.vcf

 echo "annotate vcf with rsid ..." 
 $SCRIPT_DIR/add_rs_to_vcf.pl $TMPDIR/${PID}.annotation.tsv $TMPDIR/pecaller/${PID}.vcf $TMPDIR/pecaller/${PID}.rsid.vcf

 echo "splitting multiallelic --> biallelic sites and vcf ---> bcf conversion"
 module load bcftools/1.3 
 # module load samtools/1.5

 # -v, --invert-match        select non-matching lines
 # -E, --extended-regexp 
 # -O, --output-type <b|u|z|v>  'b' compressed BCF;  u: uncompressed BCF
 # -h, --header-lines <file>
 # -m, --multiallelics <-|+>[type]   split multiallelics (-) or join biallelics (+), type: snps|indels|both|any [both]
 # -x, --remove <list>            list of annotations to remove (e.g. ID,INFO/DP,FORMAT/DP,FILTER).
 # -I, --set-id [+]<format>       set ID column

 # samtools faidx $TMPDIR/hg38.fastq
 # bcftools norm -Ou -f $TMPDIR/hg38.fastq | \

 grep -vE "(random|chrUni|chrEBV)" $TMPDIR/pecaller/${PID}.vcf | \
	bcftools annotate -Ou -h $TMPDIR/pecaller/VCFheaders.txt | \
	bcftools norm -Ou -m -any | \
	bcftools annotate -Ob -x ID -I +'%CHROM:%POS:%REF:%ALT' > $TMPDIR/pecaller/${PID}.bcf.gz

 # ./snp_to_linkage.pl pedfile.txt annotation_file snp_file SNP_TYPE[SNP,LOW,MESS,INS,DEL] outname 
 $SCRIPT_DIR/snp_to_linkage.pl \
	$PED $TMPDIR/${PID}.annotation.tsv $TMPDIR/pecaller/${PID}.95_90.snp SNP $TMPDIR/pecaller/snp_linkage

 # module unload samtools/1.5
 module unload bcftools/1.3 
 
 BCF=$TMPDIR/pecaller/${PID}.bcf.gz

else 
 echo "provide a VCF or BCF file ..."
 exit
fi

#################################################
# Data import: PLINK v1.90p 64-bit (2 Jul 2017) #
#################################################
# --keep-allele-order	: Keep the allele order defined in the .bim file
# --maf {freq}		: Exclude variants with minor allele frequency lower than a threshold (default 0.01).
# --geno {val}		: Exclude variants with missing call frequencies greater than a threshold (default 0.1).
# --ped [filename] : Specify full name of .ped file.
# --make-bed: Create a new binary fileset.
# --bfile {prefix}: Specify .bed + .bim + .fam prefix 
# --allow-no-sex (If you don't want those phenotypes to be ignored)

# PLINK offers a more efficient binary format, which uses fam(ily), bim (binary mapping) and bed (binary pedigree) files
# bed file is a matrix of 0s, 1s, 2s or NAs stored in binary format
# bim file is a map file plus two columns, providing the A1 and A2 alleles
# fam file is first six columns of ped file

PLINK_ARGS="--threads 2"
echo "loading bcf into plink"
$SCRIPT_DIR/plink $PLINK_ARGS \
	--bcf $BCF \
	--allow-no-sex \
	--keep-allele-order \
	--vcf-idspace-to _ \
	--make-bed \
	--out $TMPDIR/plink/plink.load

#==========================#
# update plink's .fam file #
#==========================#

awk 'BEGIN{FS=" ";OFS=" "} {print $1,$2,"0","0","0","1"}' $TMPDIR/plink/plink.load.fam > $TMPDIR/plink/tmp.fam 
awk '!/SL15631/' $TMPDIR/plink/tmp.fam > $TMPDIR/plink/tmp1.fam
/bin/rm $TMPDIR/plink/{plink.load.fam,tmp.fam}
cat $TMPDIR/plink/tmp1.fam $PROJ_DIR/$PID/${PID}_2.fam > $TMPDIR/plink/plink.load.fam
/bin/rm $TMPDIR/plink/tmp1.fam

#=====================================#
# Allele frequency: summary statistic #
#=====================================#
# --freq (for summary statistic)
# --maf (use maf for inclusion criteria)

# CHR Chromosome code
# SNP Variant identifier
# A1 Allele 1 (usually minor)
# A2 Allele 2 (usually major)
# MAF Allele 1 frequency
# NCHROBS Number of allele observations

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.load \
	--freq \
	--allow-no-sex \
	--out $TMPDIR/plink/plink.load.freq

# The Minor Allele Fraction (MAF) is the relative frequency in a relevant 
# population of the minor (2nd most common) allele.

#========================================================#
# Missingness per individual/marker: summary statistic	 #
#========================================================#
# --missing: Generate sample- and variant-based missing data reports
# --mind (Missingness per individual, As inclusion criteria) 
# --geno (Missingness per marker, As inclusion criteria)

# This command will create the files “.imiss” and “.lmiss”.  
# The fourth column in the file “.imiss” (N_MISS) denotes the number of
# missing SNPs and the sixth column (F_MISS) denotes the proportion of missing SNPs per individual.

# Sample missing data report written to .imiss
# variant-based missing data report written to lmiss

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.load \
	--missing \
	--out $TMPDIR/plink/plink.load.missing

# This analysis will automaIcally skip haploid markers (male X and Y chromosome markers)
# Genotypes below a threshold (e.g. 0.9 or 0.95) can be coded as missing

# 2% missingness means a genotyping rate or call rate of 98%

#=============================================================#
# Remove Subjects with More than 10 Percent Missing genotypes #
#=============================================================#
# ‘mind’ sets the maximum rate of per-individual missingness
# to exclude individuals with more than 10% missing genotypes (over all SNPs)
# Look at the plink log. Is there anyone with more than 5 percent missing genotype data?

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.load \
	--mind 0.1 \
	--make-bed \
	--out $TMPDIR/plink/plink.mind

#================================================================#
# Removing SNPs with More than 10 Percent missing genotype calls #
#================================================================#
# Removing SNPs with high rate of missing genotype calls 
# --geno {val}: Exclude variants with missing call frequencies greater than a threshold (default 0.1).
# exclude SNPs with more than 10% missing genotypes (over all individuals)
 
$SCRIPT_DIR/plink $PLINK_ARGS \
         --bfile $TMPDIR/plink/plink.load \
         --allow-no-sex \
         --keep-allele-order \
         --geno \
         --make-bed \
         --out $TMPDIR/plink/plink.geno10pc
 
# How many SNPs have a missing genotype rate greater than 10 percent?

#=========================#
# Check Gender Assignment #
#=========================#
# Identification of individuals with discordant sex information
# This command will calculate the mean homozygosity rate across X-chromosome markers for each individual in the study.  

# --split-x [build] <no-fail>       
# Error: --split-x requires X chromosome data.  (Use 'no-fail' to force --make-bed to proceed anyway.

# Plink assigns gender based on X chromosome homozygosity esimate (XHE) (F or inbreeding coefficient)

# FID        IID       PEDSEX       SNPSEX       STATUS            F
# Male (1) : F > 0.80
# Female (2) : F <0.20 
# No sex (0) : 0.20 < F < 0.80

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc \
	--allow-no-sex \
	--keep-allele-order \
	--split-x hg38 \
	--make-bed \
	--out $TMPDIR/plink/plink.geno10pc.split

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc.split \
	--allow-no-sex \
	--check-sex \
	--out $TMPDIR/plink/plink.geno10pc.sexcheck

# Problematic subjects can be removed from the dataset
grep PROBLEM $TMPDIR/plink/plink.geno10pc.sexcheck.sexcheck > $TMPDIR/plink/fail_sex_check.txt

#===============================================#
# Hardy-Weinberg equilibrium: summary statistic #
#===============================================#
# --hardy (As summary statistic)
# --hwe (use --hwe for inclusion criteria)

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc \
	--allow-no-sex \
	--keep-allele-order \
	--hardy \
	--out $TMPDIR/plink/plink.geno10pc.hardy

#===============#
# Mendel errors #
#===============#
# If you have families in your data, you can use the familial relationships 
# to do some detailed quality control with the ‘–mendel’ flag.

# *.mendel file contains all Mendel errors (i.e. one line per error); 
# *.imendel file contains a summary of per-individual error rates; 
# *.fmendel file contains a summary of per-family error rates; 
# *.lmendel file contains a summary of per-SNP error rates.

if [ "$TRIO" = true ]; then
	# As summary statistic
	$SCRIPT_DIR/plink $PLINK_ARGS \
		--bfile $TMPDIR/plink/plink.geno10pc \
		--allow-no-sex \
		--mendel \
		--out $TMPDIR/plink/plink.geno10pc.mendel

# exclude individuals and/or markers on the basis of Mendelian inheritance error rates.

# --me 0.05 0.1
# the first parameter determines that families with more than 5% Mendelian errors 
# (considering all SNPs) will be discarded, and 
# the second parameter indicates that SNPs with more than 10% Mendelian error rate 
# (based on the number of trios) will be excluded)

	# As inclusion criteria
	$SCRIPT_DIR/plink $PLINK_ARGS \
		--bfile $TMPDIR/plink/plink.geno10pc \
		--allow-no-sex \
		--me 0.05 0.1 \
		--make-bed \
		--out $TMPDIR/plink/plink.geno10pc.me
fi

# Most GWAS studies of common diseases are not family-based, 
# so remove the related individuals from this dataset using the ‘–founder’ flag.
# --filter-founders

# How do you think PLINK defines a founder?

#=========================#
# LD-based variant pruner #
#=========================#
# --indep-pairwise [window size]<kb> [step size (variant ct)] [r^2 threshold]

# These commands produce a pruned subset of markers that are in approximate 
# linkage equilibrium with each other, writing the IDs to plink.prune.in 
# (and the IDs of all excluded variants to plink.prune.out).

$SCRIPT_DIR/plink $PLINK_ARGS \
	--allow-no-sex \
	--bfile $TMPDIR/plink/plink.geno10pc \
	--indep-pairwise 50kb 5 0.2 \
	--make-bed \
	--out $TMPDIR/plink/plink.geno10pc.indep-pairwise

# The IBS method works best when only independent SNPs are included in the analysis.
# Independent SNP set for IBS calculaIon is generally prepared by removing regions 
# of extended LD and pruning the remaining regions so that no pair of SNPs within 
# a given window (say, 50kb) is correlated .

#==============================================#
# Heterozygosity rate / inbreeding coefficient #
#==============================================#
# Heterozygocity: frequency of heterozygotes in the population
# Excess heterozygosity - Possible sample contamination
# Less than expected heterozygosity- Possibly inbreeding
 
# This analysis will automaically skip haploid markers (male X and Y chromosome markers).

# With whole genome data, it is probably best to apply this analysis to a subset that are 
# pruned to be in approximate linkage equilibrium, say on the order of 50,000 autosomal SNPs.
 
# FID       Family ID
# IID       Individual ID
# O(HOM)    Observed number of homozygotes
# E(HOM)    Expected number of homozygotes
# N(NM)     Number of non-missing genotypes
# F         F inbreeding coefficient estimate
 
$SCRIPT_DIR/plink $PLINK_ARGS \
         --bfile $TMPDIR/plink/plink.geno10pc.indep-pairwise \
         --het \
         --allow-no-sex \
         --out $TMPDIR/plink/plink.geno10pc.indep-pairwise.het

#============================#
# Runs of homozygosity (ROH) #
#============================#
# A central aim for studying runs of homozygosity (ROHs) in genome-wide SNP data 
# is to detect the effects of autozygosity (stretches of the two homologous chromosomes 
# within the same individual that are identical by descent) on phenotypes.

# FID      Family ID
# IID      Individual ID
# CHR      Chromosome
# SNP1     SNP at start of region
# SNP2     SNP at end of region
# POS1     Physical position (bp) of SNP1
# POS2     Physical position (bp) of SNP2
# KB       Length of region (kb)
# NSNP     Number of SNPs in run
# DENSITY  Average SNP density (1 SNP per kb)
# PHOM     Proportion of sites homozygous
# PHET     Proportion of sites heterozygous

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc.indep-pairwise \
	--homozyg \
	--allow-no-sex \
	--out $TMPDIR/plink/plink.geno10pc.indep-pairwise.roh

#====================#
# IBS/IBD estimation #
#====================#

## --genome invokes an IBS/IBD computation
## To calculate IBD only for members of the same family (as designated by the PED file), 
# add the command (--rel-check)

# IBD = 1 (for duplicates or monozygotic twins)
# IBD = 0.5 (for first-degree relatives)
# IBD = 0.25 (for second-degree relatives)
# IBD = 0.125 (for third-degree relatives)

# Remove one individual from each pair; IBD > 0.98 (duplicates)

## Remove SNPs out of Hardy-Weinberg equilibrium
# Population genetic theory suggests that under ‘normal’ conditions, there is 
# a predictable relationship between allele frequencies and genotype frequencies. 
# In cases where the genotype distribution is different from what one would expect 
# based on the allele frequencies, one potential explanation for this is genotyping error. 
# Natural selection is another explanation. For this reason, we typically check for 
# deviation from Hardy-Weinberg equilibrium in the controls for a case- control study.

## How many SNPs have a HWE p-value of 10-5 or less?
# use the –hwe to automatically remove SNPs above a certain HWE p-value threshold.
# exclude markers that fail a Hardy-Weinberg test at significance threshold 0.00001

# Setting a minimum minor allele frequency
# Genetic associations with SNPs with a low minor allele frequency can give spurious results.
# It’s common practice to remove SNPs with very low minor allele frequency prior to analysis. 
# This is achieved with with ‘–maf’ flag.

# FID1      Family ID for first individual
# IID1      Individual ID for first individual
# FID2      Family ID for second individual
# IID2      Individual ID for second individual
# RT        Relationship type given PED file
# EZ        Expected IBD sharing given PED file
# Z0        P(IBD=0)
# Z1        P(IBD=1)
# Z2        P(IBD=2)
# PI_HAT    P(IBD=2)+0.5*P(IBD=1) ( proportion IBD )
# PHE       Pairwise phenotypic code (1,0,-1 = AA, AU and UU pairs)
# DST       IBS distance (IBS2 + 0.5*IBS1) / ( N SNP pairs )
# PPC       IBS binomial test
# RATIO     Of HETHET : IBS 0 SNPs (expected value is 2)

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc \
	--allow-no-sex \
	--extract $TMPDIR/plink/plink.geno10pc.indep-pairwise.prune.in \
	--genome rel-check \
	--hwe 0.00001 \
	--maf 0.05 \
	--out $TMPDIR/plink/plink.geno10pc.indep-pairwise.ibdcheck

# --exclude $TMPDIR/plink/fail_sex_check.txt \

#========================================#
# KING: Kinship-based INference for Gwas #
#========================================#

# FID: Family ID for the pair
# ID1: Individual ID for the first individual of the pair
# ID2: Individual ID for the second individual of the pair
# N_SNP: The number of SNPs that do not have missing genotypes in either of the individual
# Z0: Pr(IBD=0) as specified by the provided pedigree data
# Phi: Kinship coefficient as specified by the provided pedigree data
# HetHet: Proportion of SNPs with double heterozygotes (e.g., AG and AG)
# IBS0: Porportion of SNPs with zero IBS (identical-by-state) (e.g., AA and GG)
# Kinship: Estimated kinship coefficient from the SNP data
# Error: Flag indicating differences between the estimated and specified kinship coefficients (1 for error, 0.5 for warning)

$SCRIPT_DIR/plink $PLINK_ARGS \
	--bfile $TMPDIR/plink/plink.geno10pc \
	--allow-no-sex \
	--extract $TMPDIR/plink/plink.geno10pc.indep-pairwise.prune.in \
	--genome rel-check \
	--hwe 0.00001 \
	--maf 0.05 \
	--keep-allele-order \
	--make-bed \
	--out $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck

# --exclude $TMPDIR/plink/fail_sex_check.txt \

# PAIRWISE RELATIONSHIP WITHIN FAMILIES
# estimates pair-wise kinship coefficients (through paramter --kinship)
# Input a binary format file (-b)

$SCRIPT_DIR/king \
	-b $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.bed \
	--kinship \
	--cpus 2 \
	--prefix $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.kinship

# determines IBD segments (through parameter --ibdseg)
$SCRIPT_DIR/king \
	-b $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.bed \
	--ibdseg \
	--cpus 2 \
	--prefix $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.ibdseg

# POPULATION STRUCTURE INFERENCE
$SCRIPT_DIR/king \
	-b $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.bed \
	--mds \
	--cpus 2 \
	--prefix $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.mds

# Parameter --related --degree 2 specifies that only related pairs (up to the 2nd-degree in this case) 
# between families are included in the output.

$SCRIPT_DIR/king \
	-b $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.bed \
	--related --degree 2 \
	--prefix $TMPDIR/king/plink.geno10pc.indep-pairwise.ibdcheck.related

# Create plots
module load R/3.3.2
Rscript $SCRIPT_DIR/missing_mie_report.R $TMPDIR/plink
module unload R/3.3.2

# remove uploaded files before copying results
/bin/rm $TMPDIR/hg38.*

if [ ! -d $OUT ]; then
	mkdir -p $OUT
fi

echo "copying tmp data to $OUT"
rsync -av $TMPDIR/* $OUT/

# remove tmp folder
/bin/rm -rf $TMPDIR

echo "Finish - `date`" 
