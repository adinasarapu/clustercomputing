#!/usr/bin/perl 

## Descriptive statistics and quality control on variants
## 1. Ti/Tv (sometimes called Ts/Tv):
##    Transition: pyrimidine-pyrimidine (C <-> T) or purine-purine (A <-> G) mutations 
##    Transversion: pyrimidine to a purine or vice versa
##    Ti/Tv ratio should be about 2.1 for whole genome sequencing and 2.8 for whole exome, 
##    and that if it is lower, that means your data includes false positives caused by 
##    random sequencing errors
## 2. ns/ss is the ratio of non-synonymous substitutions (per site) to synonymous substitutions (per site). 

echo "Start - `date`" 
#$ -r n 
#$ -N Var.QC
#$ -q b.q
#$ -pe smp 2
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu 

PID=TYEP0004

PROJ_DIR=$HOME/zwick_rare
SCRIPT=$PROJ_DIR/script
SEQANT=$PROJ_DIR/$PID/SeqAnt
CALL_OUT=$PROJ_DIR/$PID/CallingResults

# SNP_FILE:merged.snp file for which you want to call ts:tv ratios 
# SNP=$CALL_OUT/${PID}.merge.snp
SNP=${CALL_OUT}/${PID}.final.snp
PED=$PROJ_DIR/$PID/${PID}_calling.ped

# the annotation file for the merged .snp file 
# ANNOT=$SEQANT/tyep0004_merge.annotation.tsv
ANNOT=${SEQANT}/tyep0004_final.annotation.tsv

OUT=$PROJ_DIR/$PID/PostCallQC/PE

OUT_JOINT_CALL=$OUT/plink

if [ ! -d "$OUT_JOINT_CALL" ]; then
	mkdir -p $OUT_JOINT_CALL
fi


# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
  TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

echo "copying data to $TMPDIR"
cp $SNP $TMPDIR/data.snp
cp $ANNOT $TMPDIR/annotation.tsv
cp -v $PROJ_DIR/hg38/hg38.{sdx,seq} $TMPDIR

echo "`ls -la $TMPDIR | awk '{print $9}'`"

# filter calls 
threshold_to_call=0.95 
call_rate=0.9

###############
# ALL SAMPLES #
###############
echo "transition:transversion ratios ..."
$SCRIPT/snp_tran_counter_site.pl $TMPDIR/data.snp > $TMPDIR/${PID}.ts_tv.txt 

echo "non-synonymous:synonymous ratios ..."
$SCRIPT/snp_tran_silent_rep_site.pl $TMPDIR/data.snp \
	$TMPDIR/annotation.tsv SNP > $TMPDIR/${PID}.ns_ss.details.txt  

echo "filtering SNPs ..."
$SCRIPT/snp_filter_call_rate.pl $TMPDIR/data.snp \
	$threshold_to_call $call_rate > $TMPDIR/${PID}.95_90.snp

echo "making vcf file ..."
$SCRIPT/snp_to_vcf $TMPDIR/hg38.sdx $TMPDIR/${PID}.95_90.snp > $TMPDIR/${PID}.95_90.vcf

echo "annotate vcf with rsid ..." 
$SCRIPT/add_rs_to_vcf.pl $TMPDIR/annotation.tsv $TMPDIR/${PID}.95_90.vcf $TMPDIR/${PID}.95_90_rsid.vcf

echo "splitting multiallelic --> biallelic sites and vcf ---> bcf conversion"
module load bcftools/1.3 

# -v, --invert-match        select non-matching lines
# -E, --extended-regexp 
# -O, --output-type <b|u|z|v>    u: uncompressed BCF
# -h, --header-lines <file>
# -m, --multiallelics <-|+>[type]   split multiallelics (-) or join biallelics (+), type: snps|indels|both|any [both]
# -x, --remove <list>            list of annotations to remove (e.g. ID,INFO/DP,FORMAT/DP,FILTER).
# -I, --set-id [+]<format>       set ID column

grep -vE "(random|chrUn)" $TMPDIR/${PID}.95_90.vcf | \
	bcftools annotate -Ou -h $HOME/zwick_rare/VCFheaders.txt | \
	bcftools norm -Ou -m -any | \
	bcftools annotate -Ou -x ID -I +'%CHROM:%POS:%REF:%ALT' > $TMPDIR/${PID}.95_90.bcf

module unload bcftools/1.3 

#################################
# Using PLINK 1.9 to perform QC #
#################################
PLINK_ARGS="--threads 2"
echo "loading bcf into plink"
$SCRIPT/plink $PLINK_ARGS \
	--bcf $TMPDIR/${PID}.95_90.bcf \
	--keep-allele-order \
	--vcf-idspace-to - \
	--make-bed \
	--out $TMPDIR/first

# To update .fam file
#====================
# awk 'BEGIN{FS=" ";OFS="\t"} {print "TYEP0004",$1,$2,$3,$4,$5}' $TMPDIR/first.fam > $TMPDIR/tmp.fam 
# awk '!/SL15631/' $TMPDIR/tmp.fam > $TMPDIR/tmp1.fam
# /bin/rm $TMPDIR/{first.fam,tmp.fam}
# cat $TMPDIR/tmp1.fam $PROJ_DIR/$PID/TYEP0004_calling.ped > $TMPDIR/first.fam
# /bin/rm $TMPDIR/tmp1.fam

cp $PED $TMPDIR/first.fam

# --geno {val}: Exclude variants with missing call frequencies greater than a threshold (default 0.1). 
# --bfile {prefix}: Specify .bed + .bim + .fam prefix 
# --missing: Generate sample- and variant-based missing data reports. 

# reports missing genotype rate for loci and individuals
# removes loci with missing rate > 0.1 (10%)

# Warning: --freq and --missing complete BEFORE --geno, --hwe, and --maf in

$SCRIPT/plink $PLINK_ARGS \
	--bfile $TMPDIR/first \
	--missing \
	--geno 0.1 \
	--keep-allele-order \
	--make-bed \
	--out $TMPDIR/TYE4
 
$SCRIPT/plink $PLINK_ARGS \
	--bfile $TMPDIR/TYE4 \
	--missing \
	--out $TMPDIR/missing

# --split-x [build] <no-fail>       
# variants with bp position <= bp1 or >= bp2
# variants would be affected by --split-x;

# make separate bed file for sex-check

# Error: --split-x requires X chromosome data.  (Use 'no-fail' to force --make-bed to proceed anyway.

$SCRIPT/plink $PLINK_ARGS \
	--bfile $TMPDIR/TYE4 \
	--split-x hg38 no-fail \
	--keep-allele-order \
	--make-bed \
	--out $TMPDIR/split

$SCRIPT/plink $PLINK_ARGS \
	--bfile $TMPDIR/split \
	--check-sex \
	--out $TMPDIR/sexcheck

# Mendel errors: check for loci with high number of MIEs
# Warning: Nonmissing nonmale Y chromosome genotype(s) present; many commands treat these as missing.
# Warning: Skipping --me/--mendel since there is no autosomal or Xchr data.

${SCRIPT}/plink $PLINK_ARGS \
	--bfile $TMPDIR/TYE4 \
	--mendel \
	--out $TMPDIR/mendel

# IBD check 
${SCRIPT}/plink $PLINK_ARGS \
	--vcf $TMPDIR/${PID}.95_90.vcf \
	--make-bed \
	--allow-extra-chr \
	--out $TMPDIR/ibdcheck

cp $PED $TMPDIR/ibdcheck.fam

${SCRIPT}/plink $PLINK_ARGS \
	--bfile $TMPDIR/ibdcheck \
	--genome \
	--allow-extra-chr \
	--out $TMPDIR/ibdcheck.ibdcheck

# ./snp_to_linkage.pl pedfile.txt annotation_file snp_file SNP_TYPE[SNP,LOW,MESS,INS,DEL] outname 
${SCRIPT}/snp_to_linkage.pl $PED $ANNOT $SNP SNP $TMPDIR/snp_linkage

/bin/rm $TMPDIR/{data.snp,annotation.tsv,hg38.*}
echo "copying data to $OUT_JOINT_CALL"
rsync -av $TMPDIR/* $OUT_JOINT_CALL
/bin/rm -rf $TMPDIR
	
echo "Finish - `date`" 

:<<'END'
Human mutations don't occur randomly. In fact, transitions (changes from A <-> G and C <-> T) are 
expected to occur twice as frequently as transversions (changes from A <-> C, A <-> T, G <-> C or G <-> T). 
Thus, another useful diagnostic is the ratio of transitions to transversions in a particular set of SNP calls. 
This ratio is often evaluated separately for previously discovered and novel SNPs. 

Across the entire genome the ratio of transitions to transversions is typically around 2. 
In protein coding regions, this ratio is typically higher, often a little above 3. 
The higher ratio occurs because, especially when they occur in the third base of a codon, 
transversions are much more likely to change the encoded amino acid. 

A refinement to this analysis, in protein coding regions, is to examine the transition to transversion ratio separately 
for non-degenerate, two-fold degenerate, three-fold degenerate and four-fold degenerate sites. 

For human-exome sequencing data, the Ti/Tv ratio is generally around 3.0, and about 2.0 outside of 
exome regions ( Bainbridge et al. , 2011 ). 

The Ti/Tv ratio is also different between synonymous and non-synonymous SNPs.
END
