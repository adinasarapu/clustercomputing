#!/bin/sh
echo "Start - `date`" 
#$ -N GTF
#$ -q all.q
#$ -l h_rt=3:00:00
#$ -pe smp 5
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
# -F separator
# awk {'printf ("%5s\t%s\n", $5, $1)'} a.txt ; 
# chr1	11874	12227	"DDX11L1";
# chr1	12613	12721	"DDX11L1";
# chr1	13221	14409	"DDX11L1";
GTF="/home/adinasarapu/iGenomes/Homo_sapiens/UCSC/hg38/Annotation/Genes/genes.gtf"
# awk -v OFS="\t" '$9 == "gene_id" {print $1,$4,$5,$10}' $GTF > out.txt
awk -F '    ' 'BEGIN {printf("create temp table T(chrom,start,end,gene); begin transaction;\n");} \
	$3=="exon" {
		n=split($9,a,/[ ;]+/);
		for(i=1;i+1< n;i++)
			if(a[i]=="gene_id") 
			printf("insert into T(chrom,start,end,gene) values (\"%s\",%s,%s,%s);\n",$1,$4,$5,a[i+1]);
		} END {printf("commit; select chrom,gene,min(start),max(end) from T group by chrom,gene;\n");}' | sqlite3 temp.db
