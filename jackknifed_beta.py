#! /usr/bin/env python

import subprocess

# activate qiime1 using the following command
# source activate qiime1

HOME_DIR='/home/adinasarapu/'
PROJ_DIR = HOME_DIR+'VFE11371/'
OUT_DIR = PROJ_DIR + 'results_qiime1/'

DIR_USEFUL = OUT_DIR + 'useful_files'
DIR_QIIME = OUT_DIR + 'panda/qiime'

jackknifed_beta = "jackknifed_beta_diversity.py"

print "\njackknife plots(for PCoA -- will need sample map)"
subprocess.call([jackknifed_beta, \
		"-i", DIR_USEFUL+ "/otu_table.biom", \
		"-t", DIR_USEFUL+ "/phylogenetic.tre", \
		"-m", DIR_USEFUL+ "/sample_mapping_corrected.txt", \
		"-o", DIR_USEFUL+ "/jackknifed_plots_200seq_per_individual/", \
		"-e", "200"])
