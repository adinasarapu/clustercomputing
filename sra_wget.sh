#!/bin/sh

# Downlaod raw RNASeq data from NCBI SRA database 

# Install sratoolkit
fastq_dump=$HOME/sratoolkit.2.9.0-centos_linux64/bin/fastq-dump

j=0
# 439 - 825
for i in {509..825}; do
 j=$(( $j+1 ))
 wget ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR682/SRR6826${i}/SRR6826${i}.sra
 echo "$j -> SRR6826${i}.sra downloaded ..\n"
 $fastq_dump --gzip ~/scrnaseq/SRR6826${i}.sra 
 echo "$j -> SRR6826${i}.fastq.gz created\n"
done
