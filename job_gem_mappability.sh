#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N BigWig_smp_40
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: 
## one computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 40
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# Reference: http://wiki.bits.vib.be/index.php/Create_a_mappability_track
# http://hgdownload.cse.ucsc.edu/goldenpath/hg38/bigZips/hg38.fa.gz
# https://github.com/CancerGenome/rn5_mappability
# http://www.helsinki.fi/~tsjuntun/autogscan/pedigreefile.html

#===============================================================================
PROJ_DIR="/home/adinasarapu/zwick_rare/geisert"

set -o nounset      # Treat unset variables as an error

pref="hg38" # should be changed
reference="${PROJ_DIR}/hg38.fa" # should be changed
# GENOME_REF='/home/adinasarapu/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fq'

idxpref="${pref}_index"
thr=10; # use 10 cores, change it to the number you want to use
# outmappa=$PROJ_DIR'/hg38_mappability/hg38.mappa.tsv' # should be changed

if [ ! -d "$PROJ_DIR/hg38_mappability" ]; then
	mkdir -p "$PROJ_DIR/hg38_mappability"
fi

#make index for the genome
gem-indexer \
	-T ${thr} \
	-c dna \
	-i ${reference} \
	-o "${PROJ_DIR}/hg38_mappability/${idxpref}"

# The following calculates index and creates mappability tracks with various
# kmer lengths. It may take long time to finish. choose the right kmer length 
# for you.
for kmer in 50
do
	# if [ ! -d $PROJ_DIR/hg38_mappability/${pref}.${kmer} ]; then
	#	mkdir -p $PROJ_DIR/hg38_mappability/${pref}.${kmer}
	# fi
	# compute mappability data
	gem-mappability \
		-T ${thr} \
		-I ${PROJ_DIR}/hg38_mappability/${idxpref}.gem \
		-l ${kmer} \
		-o ${PROJ_DIR}/hg38_mappability/${pref}.${kmer}
#	mpc=$(gawk '{c+=gsub(s,s)}END{print c}' s='!' $PROJ_DIR/hg38_mappability/${pref}.${kmer}.mappability)
#	echo ${pref}_${kmer}"\t"$mpc >> $outmappa
	# convert results to wig and bigwig
	gem-2-wig \
		-I $PROJ_DIR/hg38_mappability/${idxpref}.gem \
		-i $PROJ_DIR/hg38_mappability/${pref}.${kmer}.mappability \
		-o $PROJ_DIR/hg38_mappability/${pref}.${kmer}
	# Convert ascii format wig file (in fixedStep, variableStep
	# or bedGraph format) to binary big wig format
	wigToBigWig ${PROJ_DIR}/hg38_mappability/${pref}.${kmer}.wig \
		$PROJ_DIR/hg38_mappability/${pref}.${kmer}.sizes \
		$PROJ_DIR/hg38_mappability/${pref}.${kmer}.bw
	# Convert from bigWig to bedGraph format
	bigWigToBedGraph \
		$PROJ_DIR/hg38_mappability/${pref}.${kmer}.bw \
		$PROJ_DIR/hg38_mappability/${pref}.${kmer}.bedGraph
done
