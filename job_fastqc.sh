#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N FastQC
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 20
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M xyz@emory.edu
## mkdir -p myProject/{src,doc/{api,system},tools,db}
## qconf -sq all.q
###############################################################################
## Cores = Cores per socket X Sockets (4 x 2 = 8 Cores)
## CPUs = Threads per core X Cores (2 x 8 = 16 CPUs/Processors)
## lscpu (2 sockets; 4 cores/ socket; 2 threads / core; i.e total 16 threads)
## lscpu | egrep 'Thread|Core|Socket|^CPU\('
## You need to be careful of the term CPU as it means different things in different contexts
###############################################################################
PROJ_DIR=$HOME/Projects/RNA_TCA21192
SEQ_DATA=$PROJ_DIR/SequenceData/11480/C92M7ANXX

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
	TMPDIR=`/bin/mktemp -d -p /tmp/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMPDIR=`/usr/bin/mktemp -d –p /tmp/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

rsync -av $SEQ_DATA/C92M7ANXX_*/*.fastq.gz $TMPDIR/ 

module load FastQC/0.11.4 

OUTDIR=$TMPDIR/QC

for pid in {889..902}; do
	SID=SL151${pid}
	if [ ! -d $OUTDIR/$SID ]; then
		mkdir -p $OUTDIR/$SID/fastqc/{individual,combined}
	fi

	## QC metrics for all of FASTQs /individual
	fastqc -t 20 $TMPDIR/*${SID}.fastq.gz -o $OUTDIR/$SID/fastqc/individual
	
	## QC metrics for each sample (combined FASTQs) /combined
	for mid in 1 2
	do
		R="$OUTDIR/$SID/fastqc/combined/${SID}_${mid}_combined.fastq.gz"
		# cat $TMPDIR/*_${mid}_*_${SID}.fastq.gz >> ${R}
		zcat $TMPDIR/*_${mid}_*_${SID}.fastq.gz | gzip -c > ${R}
		fastqc -t 20 ${R} -o ${OUTDIR}/$SID/fastqc/combined
	done
done

/bin/rm -fr $TMPDIR/*.fastq.gz

# copy results
rsync -av $OUTDIR $PROJ_DIR/

# remove the temp directory
/bin/rm -fr $TMPDIR

module unload FastQC/0.11.4
echo "Finish - `date`"
