#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N GeisertQC
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=24:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe mpi 60
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
## mkdir -p myProject/{src,doc/{api,system},tools,db}
## qconf -sq all.q
###############################################################################
## Cores = Cores per socket X Sockets (4 x 2 = 8 Cores)
## CPUs = Threads per core X Cores (2 x 8 = 16 CPUs/Processors)
## lscpu (2 sockets; 4 cores/ socket; 2 threads / core; i.e total 16 threads)
## lscpu | egrep 'Thread|Core|Socket|^CPU\('
## You need to be careful of the term CPU as it means different things in different contexts
###############################################################################
# EIGC project ID: SSH21117 (Sequence Data)
PROJ_DIR='/home/adinasarapu/zwick_rare/geisert'
module load FastQC/0.11.4 
# 6 7 8 9
for postfix in 9
do
	GRP_DIR=${PROJ_DIR}/'/SL14857'${postfix}
	if [ ! -d ${GRP_DIR}/fastqc ]; then
		mkdir -p ${GRP_DIR}/fastqc/{individual,combined}
	fi
	## QC metrics for all of FASTQs /individual
	fastqc ${GRP_DIR}/*.fastq.gz -o ${GRP_DIR}/fastqc/individual
	## QC metrics for each sample (combined FASTQs) /combined
	cat ${GRP_DIR}/*.fastq.gz > \
		${GRP_DIR}/fastqc/combined/SL14857${postfix}_combined.fq.gz
	fastqc ${GRP_DIR}/fastqc/combined/SL14857${postfix}_combined.fq.gz \
		-o ${GRP_DIR}/fastqc/combined
done
module unload FastQC/0.11.4
echo "Finish - `date`"
