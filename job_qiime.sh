#!/bin/sh
echo "Start - `date`"
#$ -r n
#$ -N qiime_1 
#$ -q all.q
##$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
#$ -pe smp 40
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
## activate qiime environment
# module load Anaconda2/4.2.0
# module load QIIME/1
source activate qiime1
# test qiime
# print_qiime_config.py -t
## alias python='~/anaconda3/envs/qiime1/bin/python2.7'
## load the pandaseq module
module load pandaseq/2.10.0
python ../script/qiime.py
## unload the pandaseq module
module unload pandaseq/2.10.0
source deactivate qiime1
#module unload QIIME/1
#module unload Anaconda2/4.2.0
### source deactivate
echo "Finish - `date`"
