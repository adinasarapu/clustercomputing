#!/bin/sh

#################################################
# Author: Ashok R. Dinasarapu			#
# Emory University, Dept. of Human Genetics	#
# E-mail: ashok.reddy.dinasarapu@emory.edu	#
#################################################

# https://github.com/yongzhuang/TrioCNV

# STEP 1. git clone https://github.com/yongzhuang/TrioCNV.git

# STEP 2. Add local/missing jar files to Maven project (update pom.xml)
# Build maven project with propriatery libraries like
# download RCaller-2.5.jar and igv.jar into TrioCNV/lib
# create a dir TrioCNV/local-maven-repo
# update pom.xml file

# <repositories>
#  <repository>
#   <id>local-maven-repo</id>
#   <url>file:///${project.basedir}/local-maven-repo</url>
#  </repository>
# </repositories>
# <dependencies>
#  <dependency>
#   <groupId>rcaller</groupId>
#   <artifactId>RCaller</artifactId>
#   <version>2.5</version>
#  </dependency>
#  <dependency>
#  <groupId>org.broad.igv</groupId>
#   <artifactId>igv</artifactId>
#   <version>2.3.23</version>
#  </dependency>
# </dependencies>

# STEP 3. Enter "cd TrioCNV/" directory, run the following script, which  will add 
# missing jars to your local maven project, add_jar_to_mvn.sh

# STEP 4. then run mvn build
# mvn clean install -Dmaven.compiler.source=1.7 -Dmaven.compiler.target=1.7

# STEP 5. To get BAM files use, job_bwa_mpileup.sh 

# STEP 6. If necessary, update your BAM files 
# Note: The Individual ID in Pedigree file must be same as the @RG SM tag of the BAM file.
# To add (or replace) @RG SM tag to BAM file run the following script
# AddOrReplaceReadGroups.sh 

# STEP 7. Use the current file/script (HGCC, node02)

echo "Start - `date`"
#$ -N Trio.node1
#$ -q b.q@node01
#$ -pe smp 40
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

echo -----------------------------------------------
echo -e `cat $PE_HOSTFILE` # | awk 'BEGIN{ORS=" ";} {print $1}'`
echo -----------------------------------------------
echo SGE: qsub is running on $HOSTNAME
echo SGE: job identifier is $JOB_ID
echo SGE: job name is $JOB_NAME
echo SGE: parallel environment $PE
echo ------------------------------------------------------

set -o nounset      # Treat unset variables as an error

PID=TYEP0004
bin_or_kmer=200
PROJ_DIR=$HOME/zwick_rare
BUILD=hg38
idxpref=${BUILD}_index

# TrioCNV: to identify copy number variants in human genome
TrioCNV=$HOME/TrioCNV/target/TrioCNV-0.1.2.jar

# Note: The Individual ID in Pedigree file must be same as the @RG SM tag of the BAM file.
# add Add or Replace @RG SM tag to BAM file using the following script
# AddOrReplaceReadGroups.sh

# Pedigree file 
# Family ID, Individual ID, Paternal ID, Maternal ID, sex, Phenotype

PED_FILE=$PROJ_DIR/$PID/${PID}_calling.ped
REF_GENOME=$HOME/iGenomes/Homo_sapiens/UCSC/${BUILD}/Sequence/WholeGenomeFasta/genome.fa
# REF_GENOME=$PROJ_DIR/$BUILD/${BUILD}.fa
OUT_DIR="$PROJ_DIR/$PID/TrioCNV/Bin${bin_or_kmer}_1"

if [ ! -d $OUT_DIR ]; then
	mkdir -p $OUT_DIR
fi

# create a unique folder on the local compute drive
if [ -e /bin/mktemp ]; then
  TMPDIR=`/bin/mktemp -d -p /scratch/` || exit
elif [ -e /usr/bin/mktemp ]; then
  TMPDIR=`/usr/bin/mktemp -d –p /scratch/` || exit
else
  echo “Error. Cannot find mktemp to create tmp directory”
  exit
fi

cp $REF_GENOME $TMPDIR/${BUILD}.fa
echo `ls $TMPDIR`

# Mappability tracks: https://www.biostars.org/p/181014/
# https://genome.ucsc.edu/cgi-bin/hgTrackUi?db=hg38&g=wgEncodeGencodeV24
# make index for the genome
gem-indexer \
	-T 38 \
	-m unlimited -c dna \
	--mm-tmp-prefix $TMPDIR/mm_new- \
	-i $TMPDIR/${BUILD}.fa \
	-o $TMPDIR/$idxpref

# compute mappability data
gem-mappability \
	-T 38 \
	-I $TMPDIR/${idxpref}.gem \
	-l $bin_or_kmer \
	-o $TMPDIR/${BUILD}.${bin_or_kmer}

# convert results to wig and bigwig
gem-2-wig \
	-I $TMPDIR/${idxpref}.gem \
	-i $TMPDIR/${BUILD}.${bin_or_kmer}.mappability \
	-o $TMPDIR/${BUILD}.${bin_or_kmer}

# convert ascii format wig file (in fixedStep, variableStep
# or bedGraph format) to binary big wig format
wigToBigWig $TMPDIR/${BUILD}.${bin_or_kmer}.wig \
	$TMPDIR/${BUILD}.${bin_or_kmer}.sizes \
	$TMPDIR/${BUILD}.${bin_or_kmer}.bw

BWA_DIR=$PROJ_DIR/$PID/BWA

# Note: The Individual ID must be same as the @RG SM tag of the bam file.

echo "Copy BAM --> $TMPDIR"
# copy on the data
cp $BWA_DIR/SL15631*/*.rg.bam $TMPDIR

echo "Create a one column BAM list file @ $TMPDIR"
# create a one column BAM list file
echo `ls $TMPDIR/*.bam` | tr " " "\n" > $TMPDIR/bam_list.txt

module load samtools/1.3

echo "samtools indexing BAM files @ $TMPDIR"
for i in {5..7}; do
	samtools index $TMPDIR/SL15631${i}.rg.bam
done

module unload samtools/1.3

echo "Running $TrioCNV preprocess"
# -R reference genome file (required)
# -B bam list file (required)
# -P pedigree file (required)
# -M mappability file (required)
# -O output file (required)

# -d64	use a 64-bit data model if available
# -Xms  set initial Java heap size
# -Xmx  set maximum Java heap size

module load java/1.8.0_111 
module load R/3.3.2

# for java property settings, use
# java -XshowSettings
# java -XX:+PrintFlagsFinal -version | grep HeapSize
JVM_ARGS="-d64 -Xms9g -Xmx32g" 
java $JVM_ARGS -jar $TrioCNV preprocess \
	-R $TMPDIR/${BUILD}.fa \
	-B $TMPDIR/bam_list.txt \
	-P $PED_FILE \
	-M $TMPDIR/${BUILD}.${bin_or_kmer}.bw \
	-O $TMPDIR/${PID}.${bin_or_kmer}.preprocess \
	--window $bin_or_kmer \
	--min_mapping_quality 0

# -I read count file got by the preprocess step (require)
# -P pedigree file (required)
# -M mappability file (required)
# -O output structural variation file (required)

java $JVM_ARGS -jar $TrioCNV call \
	-I $TMPDIR/${PID}.${bin_or_kmer}.preprocess \
	-P $PED_FILE \
	-M $TMPDIR/${BUILD}.${bin_or_kmer}.bw \
	--nt 28 \
	-O $TMPDIR/${PID}.${bin_or_kmer}.call

# remove the original fastq file
/bin/rm $TMPDIR/${BUILD}.fa
/bin/rm $TMPDIR/${BUILD}_index*
/bin/rm $TMPDIR/*.bam
/bin/rm $TMPDIR/*.bam.bai

# copy your local data to your user directory
rsync -av $TMPDIR/* $OUT_DIR

# remove the temp directory
/bin/rm -fr $TMPDIR

module unload java/1.8.0_111
module unload R/3.3.2

echo "Finish - `date`"
