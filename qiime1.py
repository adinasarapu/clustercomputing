#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import sys
import glob
import matplotlib
import subprocess
import shlex

"""Turns out the problem was that qiime was running on a server with no graphic
support, which caused problems with the matplotlib default configuration.
In order to solve it, create (or modify) the matplotlib config file :

~/.config/matplotlib/matplotlibrc

and add the following:

backend : agg

"""

# login in as 
# ssh -Y <USER_ID>@eicc.emory.edu

# http://stackoverflow.com/questions/2339469/reading-a-os-popencommand-into-a-string

HOME_DIR='/home/adinasarapu/'
PROJ_DIR = HOME_DIR+'VFE11371/'
FASTQ_DIR = PROJ_DIR + 'VFE11371-16923/'
OUT_DIR = PROJ_DIR + 'results_qiime1/'

otus_file = HOME_DIR+'qiime-1.8.0/gg_otus-13_8-release/rep_set/97_otus.fasta'
otus_tax = HOME_DIR+'qiime-1.8.0/gg_otus-13_8-release/taxonomy/97_otu_taxonomy.txt'

# Change/set current working directory and create output directories
# os.chdir(PROJ_DIR)

DIR_USEFUL = OUT_DIR + 'useful_files'
DIR_JOINED = OUT_DIR + 'panda/joined'
DIR_LOGS = OUT_DIR + 'panda/logs'
DIR_QIIME = OUT_DIR + 'panda/qiime'

## check before creating directories
if not os.path.exists(OUT_DIR):
	os.makedirs(OUT_DIR)
if not os.path.exists(DIR_USEFUL):
	os.makedirs(DIR_USEFUL)
if not os.path.exists(DIR_JOINED):
	os.makedirs(DIR_JOINED)
if not os.path.exists(DIR_LOGS):
	os.makedirs(DIR_LOGS)
if not os.path.exists(DIR_QIIME):
	os.makedirs(DIR_QIIME)

## Get UNIQUE File names, for this "1_001.fastq.gz" remove
def uniq_files(file_names):
	s = []
	for i in file_names:
		i = i[:-14]
		if i not in s:
			s.append(i)
	return s

## get all fastq.gz files from data folder
# VFE11371-CBP275-7_S65_L001_R1_001.fastq.gz
fastq_files = []
for name in glob.glob(FASTQ_DIR + 'VFE11371-*R[1-2]*.fastq.gz'):
	fastq_files.append(name)

## collect unique files
dir_uniq_files = uniq_files(fastq_files)
# print dir_uniq_files

## This joins paired end reads with PANDASeq AND generates a mapping file based
## on file names; Make sure you look at the logging output to ensure you haven't 
## f*&%ed up

print "\nJoining paired ends and creating mapping file...\n"
for file_name in dir_uniq_files:
	print file_name+'\n'
	regexp = re.compile(file_name)
	# pcounter: number of paired sequences
	pcounter = 0
	# index: file positions 
	index = 0
	file_list = []
	for k in fastq_files:
		m = regexp.match(k)
		index += 1
		if m:
			pcounter += 1
			file_list.append(index)
	sorted(file_list)
	if pcounter == 2 and len(file_list) == 2:
		FWD = fastq_files[file_list[0]-1]
		REV = fastq_files[file_list[1]-1]
		# important: check here the order of selected files
		print " FWD --> "+FWD + '\tREV --> ' + REV
		FWDbase = os.path.basename(FWD)
		PANDA_CMD = "pandaseq -f %s -r %s -B -T 1 -N -g %s/%s.txt > %s/%s" \
				% (FWD,REV,DIR_LOGS,FWDbase,DIR_JOINED,FWDbase)
		print 'Now running --> ' + PANDA_CMD
		os.popen(PANDA_CMD)

## Merge and summarize PANDASeq logs
print "\nGenerating PANDASeq stats log...\n"

logs_file = open(DIR_USEFUL +'/paired_end_joining_stats.txt', 'w')
logs_file.write("File\tREADS\tNOALGN\tLOWQ\tHASN\tENDTOTAL")

log_files = [DIR_LOGS+'/'+x for x in os.listdir(DIR_LOGS)]

## If multiple threads are used, which the default on most platforms, each
## thread collects this information separately. 
## The output log will output a group of STAT lines per thread.

for fname in log_files:
	with open(fname) as infile:
		lines = [line.strip().split('\t') for line in infile.readlines()]
		for idx in range(0,len(lines)):
			cols = lines[idx]
			# READS: the number of read pairs in the input 
			if len(cols) != 4:
				continue
			elif (len(cols) == 4 and cols[1] == 'STAT'):
				if cols[2] == 'READS':
					reads = cols[3]+'\t'
				if cols[2] == 'NOALGN':
					noalign = cols[3]+'\t'
				if cols[2] == 'LOWQ':
					lowq = cols[3]+'\t'
				# DEGENERATE/HASN means that the read has an ambiguous base
				if cols[2] == 'DEGENERATE':
					degenerate = cols[3]+'\t'
				# ENDTOTAL/OK means reads that successfully make it through
				# PANDASeq
				if cols[2] == 'OK':
					ok = cols[3]+'\t'
	head, tail = os.path.split(fname)
	filename = tail.replace('.fastq.gz.txt','\t')
	logs_str = filename+reads+noalign+lowq+degenerate+ok
	print logs_str
	logs_file.write('\n'+logs_str)
logs_file.close();

import pandas as pd

def ws_strip(text):
	try:
		return text.strip()
	except AttributeError:
		return text

def get_barcode(sample_id):
	csv_file = '/home/adinasarapu/VFE11371/SampleSheet.csv'
	df = pd.read_csv(csv_file, \
			header=19, \
			index_col='Sample_ID',\
			usecols=['Sample_ID', 'index','index2'],\
			converters={'Sample_ID' : ws_strip,\
			'index' : ws_strip,\
			'index2' : ws_strip})
	df['Sample_ID'] = df.index
	df['Sample_ID'] = df['Sample_ID']\
			.replace(to_replace=['-','_'], \
			value='.',regex=True)
	df["BarcodeSequence"] = df["index"] + df["index2"]
	# where 1 is the axis number (0 for rows and 1 for columns.)
	df = df.drop(['index','index2'], 1)
	# df["LinkerPrimerSequence"] = "YATGCTGCCTCCCGTAGGAGT"
	# idx = 'YGO11472-0089_G4-2'
	return df.at[sample_id,'BarcodeSequence']

## Prepare joined paired end files for QIIME
print("\nProcessing for QIIME...\n")
files_joined = os.listdir(DIR_JOINED)
fidx = 0
while fidx < len(files_joined):
	joined_file = files_joined[fidx]
	ifile = open(DIR_JOINED+'/'+joined_file)
	# YGO11472-0024-A2-1_S24_L001_R1_001.fastq.gz.fna
	ofile = open(DIR_QIIME+'/'+joined_file+".fna", 'w')
	counter = 0
	sid = joined_file.split("_")[0]
	sid = sid.replace("-",".")
	bc = get_barcode(sid)
	for line in ifile:
		if counter % 2 == 0 and line.startswith('>'):
			newline = ">"+sid+"_"+str(counter/2)+" "+line[1:line.rindex(':')]+\
					" orig_bc="+bc+" new_bc="+bc+" bc_diffs=0"
			ofile.write(newline+'\n')
		elif counter % 2 == 1:
			ofile.write(line)
		counter += 1
	ifile.close()
	fidx += 1
	print("\t"+ joined_file + " has " + str(counter/2) + " sequences")
	ofile.close()

## Concatenate all files into one
print "\nConcatenating files...\n"
qi_files = os.listdir(DIR_QIIME)
with open(DIR_QIIME+'/combined_sequences.txt', 'wb') as outfile:
	for f in qi_files:
		with open(DIR_QIIME+'/'+f,'rb') as infile:
			outfile.write(infile.read())

## Perform closed OTU picking on the combined file
comb_file = DIR_QIIME+'/combined_sequences.txt'
##print comb_file+"\n"+otus_file+"\n"+otus_tax+"\n"+ PROJ_DIR+"closed_ref_OTUs"

if(not os.path.isfile(comb_file)):
	print("{} does not exist".format(comb_file))
if(not os.path.isfile(otus_file)):
	print("{} does not exist".format(otus_file))
if(not os.path.isfile(otus_tax)):
	print("{} does not exist".format(otus_tax))

cmd_otu = "pick_closed_reference_otus.py"
cmd_alpha = "alpha_diversity.py"
summ_taxa = "summarize_taxa.py"
plot_taxa = "plot_taxa_summary.py"
pick_rep = "pick_rep_set.py"
align_seq = "align_seqs.py"
filt_align = "filter_alignment.py"
ptree = "make_phylogeny.py"
beta = "beta_diversity.py"
jackknifed_beta = "jackknifed_beta_diversity.py"

print "Performing closed OTU picking on the combined file"
subprocess.call([cmd_otu, \
		"-i", comb_file, \
		"-r", otus_file, \
		"-t", otus_tax, \
		"-o", OUT_DIR+"closed_ref_OTUs"])

# copy the .biom file over to the "useful_files"
OTU_DIR = OUT_DIR + "closed_ref_OTUs"
BIOM_FILE = OTU_DIR + "/otu_table.biom"
if os.path.isfile(BIOM_FILE) and os.access(BIOM_FILE, os.R_OK):
	print "BIOM file exists and is readable!!!"
	subprocess.call(["cp", \
			BIOM_FILE,\
			DIR_USEFUL])

	# perform alpha diversity metrics
	print "\nCalculating alpha diversity...\n"
	subprocess.call([cmd_alpha, \
			"-i", BIOM_FILE, \
			"-m", "chao1,shannon", \
			"-o", DIR_USEFUL+"/alpha_diversity.txt"])

	# generate plots and abundance tables
	print "\nMaking abundance tables...\n"
	subprocess.call([summ_taxa, \
			"-i", DIR_USEFUL+ "/otu_table.biom",\
			"-o", DIR_USEFUL+ "/taxa_summaries",\
			"-L", "2,3,4,5,6,7"])
	
	print "\nPlotting...\n"
	otu_table_L2 = "/taxa_summaries/otu_table_L2.txt"
	otu_table_L3 = "/taxa_summaries/otu_table_L3.txt"
	otu_table_L4 = "/taxa_summaries/otu_table_L4.txt"
	otu_table_L5 = "/taxa_summaries/otu_table_L5.txt"
	otu_table_L6 = "/taxa_summaries/otu_table_L6.txt"
	otu_table_L7 = "/taxa_summaries/otu_table_L7.txt"
	subprocess.call([plot_taxa, \
			"-i", DIR_USEFUL+ otu_table_L2+ "," \
			+ DIR_USEFUL+ otu_table_L3 + "," \
			+ DIR_USEFUL+ otu_table_L4 + "," \
			+ DIR_USEFUL+ otu_table_L5 + "," \
			+ DIR_USEFUL+ otu_table_L6 + "," \
			+ DIR_USEFUL+ otu_table_L7, \
			"-o", DIR_USEFUL+ "/taxa_summary_plots/"])

	print "\nPicking representative sequences for OTUs...\n"
	subprocess.call([pick_rep,\
			"-i", OTU_DIR + \
			"/uclust_ref_picked_otus/combined_sequences_otus.txt", \
			"-r", otus_file, \
			"-o", OTU_DIR + "/OTU_rep_sequences.fna"])

	print "\nAligning representative sequences...\n"
	subprocess.call([align_seq, \
			"-i", OTU_DIR+"/OTU_rep_sequences.fna",\
			"-t", HOME_DIR+"qiime-1.8.0/core_set_aligned.fasta.imputed",\
			"-o", OTU_DIR+"/aligned_sequences/"])

	print "\nFiltering alignment...\n"
	subprocess.call([filt_align, \
			"-i", OTU_DIR +\
			"/aligned_sequences/OTU_rep_sequences_aligned.fasta", \
			"-m", HOME_DIR + "qiime-1.8.0/lanemask_in_1s_and_0s", \
			"-o", OTU_DIR + "/filtered_alignment/"])

	print "\nGenerating phylogenetic tree...\n"
	subprocess.call([ptree, \
			"-i", OTU_DIR+"/filtered_alignment/OTU_rep_sequences_aligned_pfiltered.fasta",
			"-o", DIR_USEFUL+"/phylogenetic.tre"])
	
	print "\nCalculating weighted UniFrac distances...\n"
	subprocess.call([beta, \
			"-i", DIR_USEFUL+ "/otu_table.biom", \
			"-m", "weighted_unifrac", \
			"-o", DIR_USEFUL+ "/weighted_unifrac/", \
			"-t", DIR_USEFUL+ "/phylogenetic.tre"])

	#print "\njackknife plots(for PCoA -- will need sample map)"
	#subprocess.call([jackknifed_beta, \
	#		"-i" DIR_USEFUL+ "/otu_table.biom", \
	#		"-t" DIR_USEFUL+ "/phylogenetic.tre", \
	#		"-m" "map_for_filtered.txt", \
	#		"-o" DIR_USEFUL+ "/jackknifed_plots_individuals/", \
	#		"-e", 2000])
else:
	print OTU_DIR+"/otu_table.biom, Missing/Not readable!"
