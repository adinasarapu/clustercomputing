#!/bin/sh

# merges the variant information contained in #####.snp (output from PECaller, job_caller.sh) with the 
# insertion/deletion information in the ###.indel.txt.gz files (output from PEMapper, job_mapper.sh) 
#  PEMapper and PECaller provide a simplified approach to whole-genome sequencing. PNAS 2017;114(10).

echo "Start - `date`"

## job is rerunnable
#$ -r n
## name for the job 
#$ -N PE_Merge
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q all.q
##$ -l h_rt=120:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 30
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR=/home/adinasarapu/zwick_rare
PROJ_ID=TYEP0004
TYE_DIR=${PROJ_DIR}/${PROJ_ID}/TYEDirectory

CALL_DIR=${PROJ_DIR}/${PROJ_ID}/CallingResults
SDX=${PROJ_DIR}/hg38/hg38.sdx

cd ${CALL_DIR}

OUT_MERGE=${CALL_DIR}/${PROJ_ID}.merge.snp

MERGE_INDEL=${PROJ_DIR}/script/merge_indel_snp_fixed.pl

${MERGE_INDEL} \
	${SDX} \
	${TYE_DIR}/${PROJ_ID}.snp \
	${TYE_DIR} \
	${OUT_MERGE}

echo "Finish - `date`"
