#!/bin/sh

echo "Start - `date`" 
#$ -N QIIME.2
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M adinasa@emory.edu

# module load Anaconda3/4.2.0
source activate qiime2-2017.4

PROJ_DIR=/home/adinasarapu/microbiome
QIIME2_DIR=${PROJ_DIR}/qiime2
OUT_DIR=${QIIME2_DIR}/qiime2_ptb_out

if [ ! -d ${OUT_DIR} ]; then
	mkdir -p ${OUT_DIR}
fi

# for EICC (Since no /scratch dir at EICC computing nodes  use ...)
export TMPDIR=${PROJ_DIR}/tmp

# for HGCC (creates a unique dir on local compute drive)
# export TMPDIR=/scratch/tmp 

if [ -e /bin/mktemp ]; then
	TMP_DIR=`/bin/mktemp -d -p ${TMPDIR}/` || exit
elif [ -e /usr/bin/mktemp ]; then
	TMP_DIR=`/usr/bin/mktemp -d –p ${TMPDIR}/` || exit
else
	echo “Error. Cannot find mktemp to create tmp directory”
	exit
fi

# Bug in QIIME2: Don't use underscores (_) in sample-id of manifest file (ptb_manifest) 
#===============================================================================#
# sample-id,absolute-filepath,direction						#
# EIGC9-001-2,$HOME/PTB_0816/EIGC9-001-2_S37_L001_R1_001.fastq.gz,forward	#
# EIGC9-001-2,$HOME/PTB_0816/EIGC9-001-2_S37_L001_R2_001.fastq.gz,reverse	#
# EIGC9-002-2,$HOME/PTB_0816/EIGC9-002-2_S38_L001_R1_001.fastq.gz,forward	#
# EIGC9-002-2,$HOME/PTB_0816/EIGC9-002-2_S38_L001_R2_001.fastq.gz,reverse	#
#===============================================================================#

cp ${QIIME2_DIR}/ptb_manifest ${TMP_DIR}/

#==========================================#
# Import FASTQ files into QIIME2 Artifact  #
#==========================================#

qiime tools import \
	--type SampleData[PairedEndSequencesWithQuality] \
	--input-path ${TMP_DIR}/ptb_manifest \
	--output-path ${TMP_DIR}/paired-end-ptb.qza \
	--source-format PairedEndFastqManifestPhred33

#============================#
# Plot positional qualitites #
#============================#

qiime dada2 plot-qualities \
	--i-demultiplexed-seqs ${TMP_DIR}/paired-end-ptb.qza \
	--o-visualization ${TMP_DIR}/ptb-qualities.qzv \
	--p-n 10

cp ${TMP_DIR}/paired-end-ptb.qza ${OUT_DIR}/paired-end-ptb.qza
cp ${TMP_DIR}/ptb-qualities.qzv ${OUT_DIR}/ptb-qualities.qzv

/bin/rm ${TMP_DIR}/paired-end-ptb.qza
/bin/rm ${TMP_DIR}/ptb-qualities.qzv

#=======================================#
# denoise paired-end sequences		#
# dereplicate, and filter chimeras	#
#=======================================#
# Run the following steps after looking quality plots
# OUTPUT: table.qza and representative_sequences.qza
# Value for --p-n-threads depends on # of slots/cores available (-pe smp 30)
 
qiime dada2 denoise-paired \
	--i-demultiplexed-seqs ${OUT_DIR}/paired-end-ptb.qza \
	--output-dir ${OUT_DIR}/table-dada2 \
	--p-n-threads 30 \
	--p-chimera-method pooled \
	--p-trim-left-f 20 \
	--p-trim-left-r 20 \
	--p-trunc-len-f 200 \
	--p-trunc-len-r 200 \
	--verbose

#=======================================#
# Export data from a QIIME2 Artifact	#
#=======================================#
# OUTPUT: feature-table.biom
BIOM_EXP=${OUT_DIR}/table-dada2/exported-feature-table

qiime tools export \
	${OUT_DIR}/table-dada2/table.qza \
	--output-dir ${BIOM_EXP}

# pip install biom-format python package

# Summarize a BIOM table
biom summarize-table \
	-i ${BIOM_EXP}/feature-table.biom > ${BIOM_EXP}/biom_summary.txt

biom convert \
	-i ${BIOM_EXP}/feature-table.biom \
	-o ${BIOM_EXP}/feature-table.tsv \
	--to-tsv

# Normalizing a BIOM table to relative abundnace
biom normalize-table \
	-i ${BIOM_EXP}/feature-table.biom \
	-r \
	-o ${BIOM_EXP}/normalized_table.biom

biom convert \
	-i ${BIOM_EXP}/normalized_table.biom \
	-o ${BIOM_EXP}/normalized_table.tsv \
	--to-tsv

rsync -av ${TMP_DIR}/ ${OUT_DIR}
/bin/rm -rf ${TMP_DIR}

source deactivate qiime2-2017.4
# module unload Anaconda3/4.2.0
echo "Finish - `date`"
