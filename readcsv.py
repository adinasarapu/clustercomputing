#!/usr/bin/python
# -*- coding: utf-8 -*-

import pandas as pd

def strip(text):
	try:
		return text.strip()
	except AttributeError:
		return text

csv_file = '/home/adinasarapu/VFE11371/SampleSheet.csv'
df = pd.read_csv(csv_file, \
		header=19, \
		index_col='Sample_ID',\
		usecols=['Sample_ID', 'index','index2'],\
		converters = {'Sample_ID' : strip,'index' : strip, 'index2':strip})

df['Sample_ID'] = df.index
df['Sample_ID'] = df['Sample_ID'].replace(to_replace=['-','_'], value='.', regex=True)
df["BarcodeSequence"] = df["index"] + df["index2"]
# where 1 is the axis number (0 for rows and 1 for columns.)
df = df.drop(['index','index2'], 1)
df["LinkerPrimerSequence"] = "YATGCTGCCTCCCGTAGGAGT"
# print df
#sid = "YGO11472-0024-A2-1_S24_L001_R1_001.fastq.gz"
#sid = sid.split("_")[0]
#sid = sid[0:13] + '_' + sid[13+1:]
#print sid
#idx = 'YGO11472-0089_G4-2'
# VFE11371.CBP319.2.ASCENDING
# VFE11371.CBP1022.3
print df.at['VFE11371.CBP1022.3','BarcodeSequence']
