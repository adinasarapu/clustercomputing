#!/bin/sh
##There are three alignment algorithms in BWA: `mem', `bwasw', and 
## `aln/samse/sampe'. If you are not sure which to use, try `bwa mem' first.
## bwa (alignment via Burrows-Wheeler transformation)
## mem (BWA-MEM algorithm)
echo "Start - `date`" 
#$ -N SL6_smp_40
#$ -q all.q
#$ -l h_rt=120:00:00
#$ -pe smp 40
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

PROJ_DIR=$HOME/zwick_rare/geisert
REF_DIR=$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/BWAIndex

# 
# SL148576  SL148577  SL148578  SL148579
# REGION=${PROJ_DIR}/regions/chrIV.bed

SID="SL148576" 

OUT_DIR=$PROJ_DIR/${SID}/BWA
OUT_PLOTS=$PROJ_DIR/${SID}/BWA/plots

# rm -rf ${OUT_DIR}

if [ ! -d ${OUT_DIR} ]; then
	mkdir -p ${OUT_DIR}
fi

module load bwa/0.7.12
module load samtools/1.3
module load bcftools/1.3
module load picardtools/2.0.1
module load bedtools/2.25.0

FILE_1=`ls -la ${PROJ_DIR}/${SID}/*_1_*_${SID}.fastq.gz | awk '{print $9}'`
FILE_2=`ls -la ${PROJ_DIR}/${SID}/*_2_*_${SID}.fastq.gz | awk '{print $9}'`

echo "bwa mem ${REF_DIR}/genome.fa ${FILE_1} ${FILE_2} > ${OUT_DIR}/${SID}.sam"
bwa mem ${REF_DIR}/genome.fa ${FILE_1} ${FILE_2} > ${OUT_DIR}/${SID}.sam

# BWA prints total CPU and wall-clock time at the end of the run. If you don't
# see it, the output is very likely to be incomplete.

##################################
# Variant calling using SAMtools #
##################################
# Like many Unix tools, SAMTools is able to read directly from a stream i.e. stdout.
# bwa mem  ref.fasta f1.fq.gz f2.fq.gz | samtools view  -Sb - | samtools sort - sorted && samtools index sorted.bam
# view:	SAM<->BAM conversion
# sort:	sort alignment file
# If the header information is available: samtools view -bS
# If the header is absent from the SAM file: samtools view -bT

samtools view -bS ${OUT_DIR}/${SID}.sam > ${OUT_DIR}/${SID}.bam
samtools sort -T /tmp/${SID}.sorted -o ${OUT_DIR}/${SID}.sorted.bam ${OUT_DIR}/${SID}.bam

#  MarkDuplicates looks at the starting position and the CIGAR string of the reads.
#  If the start position and CIGAR are identical, then the reads are
#  duplicates. So the program only leaves one read untouched and marks the
#  others so they can be ignored in downstream analyses.
# after loading picartools
# echo $PICARD
# java -Xmx2g -jar /sw/eicc/Pkgs/picardtools/2.0.1/picard.jar
$PICARD MarkDuplicates \
	MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 \
	METRICS_FILE=${OUT_DIR}/${SID}.metrics \
	REMOVE_DUPLICATES=true \
	ASSUME_SORTED=true \
	VALIDATION_STRINGENCY=LENIENT \
	INPUT=${OUT_DIR}/${SID}.sorted.bam \
	OUTPUT=${OUT_DIR}/${SID}.dedupe.bam

samtools index ${OUT_DIR}/${SID}.dedupe.bam

# flagstat:	simple stats
# samtools flagstat out.sorted.bam

# Call SNPs using samtools using mpileup (multi-way pileup)
# to limit pileup to regions use 'something like' -I regions.bed 
# (samtools mpileup -I regions.bed ...)
# '-' is used samtools/bcftools to tell the program to take the input from pipe 
# '-' = /dev/stdin
# BCF, the binary variant call format
# samtools mpileup -uf ${REF_DIR}/genome.fa ${OUT_DIR}/out.dedupe.bam | bcftools view -bvcg - > ${OUT_DIR}/out.bcf
# --skip-indels (report only SNPs)
samtools mpileup \
	-go ${OUT_DIR}/${SID}.bcf \
	-f ${REF_DIR}/genome.fa ${OUT_DIR}/${SID}.dedupe.bam

#################
# BCFtools call #
#################
# The BCFtools call utility calls the SNPs and Indels
# Index the BCF file with tabix, then use bacftools call to call SNPs
tabix ${OUT_DIR}/${SID}.bcf 
# --skip-variants indels (report only SNPs)
bcftools call -vm -Oz -o ${OUT_DIR}/${SID}.vcf.gz ${OUT_DIR}/${SID}.bcf


###################
# BCFtools filter #
###################
# Variant filtration is a subject worthy of an article in inself and the exact
# filteres you will need to use will depend on the purpose of your study and 
# quality and depth of the data used to call the variants.
#bcftools filter -O z -o ${OUT_DIR}/${SID}.filtered.vcf.gz \
#	-s LOWQUAL -i'%QUAL>10' ${OUT_DIR}/${SID}.vcf.gz

######################
# BCFtools consensus #
######################
# call the concensus sequences for the reference genome assembly
# prepare VCF for querying, index it using tabix!
# Index the output VCF file with tabix, then call the concensus sequences for
# the reference genome assembly
tabix -p vcf ${OUT_DIR}/${SID}.vcf.gz
bcftools consensus \
	-f ${REF_DIR}/genome.fa \
	-i ${OUT_DIR}/${SID}.vcf.gz > ${OUT_DIR}/${SID}.consensus.fa

# add dbSNP rs IDs to vcf files
# bcftools annotate ­-c ID ­-a dbsnp.vcf.gz  sample1.vcf.gz > sample1.rs.vcf
# java -jar SnpSift.jar annotate dbsnp.vcf  sample1.vcf > sample1.rs.vcf

#########################
# GATK VariantAnnotator #
#########################
#Annotate a VCF with dbSNP IDs and depth of coverage for each sample

java -jar $GATK \
	-R ${REF_DIR}/genome.fa \
	-T VariantAnnotator \
	-I ${OUT_DIR}/${SID}.dedupe.bam \
	-o ${OUT_DIR}/${SID}.dbSNP.vcf.gz \
	-A Coverage \
	-V ${OUT_DIR}/${SID}.vcf.gz \
	-L ${OUT_DIR}/${SID}.vcf.gz \
	--dbsnp $HOME/hg38bundle/dbsnp_144.hg38.vcf.gz

#####################
# BEDtools getfasta #
#####################
# (aka fastaFromBed)
# Extract the regions of interest using BEDtools fastaFromBed
# -fi Input FASTA file
# -bed BED/GFF/VCF file of ranges to extract from -fi
# -fo Output file (can be FASTA ot TAB-delimited, -tab)

#bedtools getfasta \
#	-fi ${OUT_DIR}/${SID}.consensus.fa \
#	-bed ${REGION} \
#	-fo ${OUT_DIR}/${SID}.chrIV.consensus.fa

##################
# BCFtools stats #
##################
# Extract stats from a VCF/BCF file or compare two VCF/BCF files
# -F faidx indexed reference sequence file to determine INDEL context
# -s samples List
bcftools stats -F ${REF_DIR}/genome.fa \
	-s - ${OUT_DIR}/${SID}.vcf.gz > ${OUT_DIR}/${SID}_vcf_stats.vchk

#################
# plot-vcfstats #
#################
# -p The output directory. This directory will be created if it does not exist.
plot-vcfstats -p ${OUT_PLOTS}/ ${OUT_DIR}/${SID}_vcf_stats.vchk

# Run to get summary stats
# samtools flagstat ${OUT_DIR}/${SID}.bam 

# check out the VCF entries with a genotype likelihood
# zcat ${SID}.filtered.vcf.gz | grep -v "^#" | grep PL | cut -f1-6 | head -100
# bcftools query -f'%CHROM\t%POS\t%REF,%ALT\n' ${SID}.filtered.vcf.gz

module unload picardtools/2.0.1
module unload bwa/0.7.12
module unload samtools/1.3
module unload bcftools/1.3
module unload bedtools/2.25.0
