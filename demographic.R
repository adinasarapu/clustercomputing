##############################################################
# Demographics data
# Subjects: 50
# HC: Young age 25-40y; Older 60+y
# demographic_file <- "Microarray sample inventory.xlsx"
##############################################################
demographic_df <- function(demoExcelFile){
  #setwd("/Volumes/Ashok/Vax010/Data")
  #header = TRUE
  #sheet = "demographics" 
  #startRow=4
  #endRow=102
  #startCol = 2
  #endCol=7
  #demoExcelFile <- "/Volumes/Ashok/Vax010/Data/Microarray sample inventory.xlsx"
  demo_df <- parseExcelFile(demoExcelFile, TRUE, "demographics_modified", 4, 102, 2, 7)
  # DF with "Participant.Identification.Number", "Vaccinated", "Date of Birth","Race", "Ethnicity", Sex"
  demo_df <- demo_df[demo_df$Vaccinated == "yes",]
  row.names(demo_df) <- demo_df[,1]
  demo_df[,1] <- NULL
  #header = TRUE
  #sheet = "Vaccine arms"
  #startRow=1
  #endRow=51
  #startCol = 1
  #endCol=3
  vac_df <- parseExcelFile(demoExcelFile, TRUE, "Vaccine arms", 1, 51, 1, 3)
  vacc_df<- vac_df[,3,drop=FALSE]
  row.names(vacc_df) <- vac_df[,1]
  df <- merge(demo_df,vacc_df, by="row.names")
  row.names(df) <- df[,1]
  df[,1] <- NULL
  return (df)
}

# Subject	Day	Vaccine	Date.of.vaccination	Date.of.boost	Date.of.draw
# dob_data <-  demographic_data
# sampleFile <- sampleDateFile
# sheet_name <- "Sheet3"
# arg1 =1
# arg2 =451
# arg3 =1
# arg4 =4
# expression_data = FALSE
# sampleDateFile, demographic_data, "Sheet3", 1, 451, 1, 4, FALSE
age_df <- function(sampleFile, dob_data, sheet_name,arg1, arg2, arg3, arg4,expression_data){
  age_dob <- parseExcelFile(sampleFile, TRUE, sheet_name, arg1, arg2, arg3, arg4)
  #age_dob <- age_dob[-1,c(3,5,8)]
  #names(age_dob)
  if(!expression_data){
    age_dob <- age_dob[!age_dob$Sample %in% c("quit","no show"),c("SubjectID","Day","Date.of.draw")]
    
  } else {
    age_dob <- age_dob[,c("SubjectID","Day","Date.of.draw")]
  }
  age_dob$Day <- gsub("\\D+","D",age_dob$Day)
  #dob_data <- dob_data[,!(names(dob_data) %in% c("Vaccine"))]
  age_dob <- merge(age_dob, dob_data, by.x="SubjectID", by.y="row.names",all.x=TRUE, all.y=FALSE)
  age_dob["age"] <- NA
  age_dob$age <- as.numeric(as.Date(age_dob$Date.of.draw) - as.Date(age_dob$Date.of.Birth)) / 365 
  subjects_age_merged <- data.frame(SubjectID = character(0), stringsAsFactors = FALSE)
  day_lst <- unique(age_dob$Day)
  for (i in 1:length(day_lst)) {
    age_data_one <- subset(age_dob, age_dob$Day %in% day_lst[i], select = c("SubjectID", "age"))
    #dim(age_data_one)
    subjects_age_merged <- merge(subjects_age_merged, age_data_one, all = TRUE)
    names(subjects_age_merged)[i + 1] <- day_lst[i]
    #dim(mn_data_strain)
  }
  subjects_age_merged <- subjects_age_merged[!duplicated(subjects_age_merged$SubjectID), ]
  row.names(subjects_age_merged) <- subjects_age_merged$SubjectID
  subjects_age_merged$SubjectID <- NULL
  if("D42" %in% names(subjects_age_merged)){
    subjects_age_merged$D100 <- subjects_age_merged$D42+(58/30)
  }
  return (subjects_age_merged)
}

parseExcelFile <- function(arg1, arg2, arg3, arg4, arg5, arg6, arg7){
  # load the library "XLConnect"
  library("XLConnect")
  file_df <- readWorksheetFromFile(file = arg1, 
                                   header = arg2,
                                   sheet = arg3, 
                                   startRow = arg4, 
                                   endRow = arg5,
                                   startCol = arg6,
                                   endCol = arg7)
  # unload the library "XLConnect"
  detach("package:XLConnect")
  return (file_df)
}
# merge(demographic_df,vaccine_arm_df, by="row.names")
#demographic_df
# vaccine_arm_df