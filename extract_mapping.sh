#!/bin/sh

IN_FOLDER='/home/adinasarapu/VFE11371/results_qiime1/panda/qiime'
OUT_FILE='/home/adinasarapu/VFE11371/results_qiime1/useful_files/sample_mapping.txt'

if [ -e "$OUT_FILE" ] ; then
	rm "$OUT_FILE"
fi

iHEAD="#SampleID\tBarcodeSequence\tLinkerPrimerSequence"
echo -e ${iHEAD} >> ${OUT_FILE}

# >YGO11472-0001_A1-1_0 \
#	M01541:184:000000000-B3MBK:1:1101:13823:1093 \
#	1:N:0:1 \
#	orig_bc=ACTCGCTACTCTCTAT \
#	new_bc=ACTCGCTACTCTCTAT \
#	bc_diffs=0

for file in ${IN_FOLDER}/*.fastq.gz.fna; do
	# select first line of file
	fline=$(awk 'NR==1 {print; exit}' ${file})
	# string split and get first word
	sample_id=$(echo $fline | cut -d' ' -f 1 | sed 's/\_0//g' | sed 's/[-_]/./g')
	bc=$(echo $fline | cut -d' ' -f 3)  
	linker="YATGCTGCCTCCCGTAGGAGT"
	# remove first char '>' and export it as map.txt
	echo -e ${sample_id//>}'\t'${bc//orig_bc=}'\t'${linker} >> ${OUT_FILE}
done

# validate_mapping_file.py -m sample_mapping.txt -o check_id_output/ -p -b
