# Function to parse Excel file
parseExcel <- function(file,sheet=1,startRow=1,endRow,startCol=1,endCol){
  library("XLConnect")
  # samples
  btm_df <- readWorksheetFromFile(file, 
                                  header = TRUE,
                                  sheet = sheet, 
                                  startRow=startRow,
                                  endRow=endRow,
                                  startCol = startCol,
                                  endCol = endCol) 
  # unload the library "XLConnect"
  detach("package:XLConnect")
  return (btm_df)
}

otu_data_file <- "/Users/adinasarapu/Documents/Emory/share/UGANDA/HIV_TIV_OTU.xls"
otu_data_ann <- parseExcel(file=otu_data_file, endRow=19, endCol=8)
otu_data_ann$Genus <- gsub("g__","",otu_data_ann$Genus)
otu_data_ann <- otu_data_ann[,c("taxa_of_interest","Genus")]
row.names(otu_data_ann) <- otu_data_ann$taxa_of_interest
otu_data_ann$taxa_of_interest <- NULL

otu_data <- parseExcel(file=otu_data_file, sheet=2, endRow=190, endCol=24)
head(otu_data)

# OTUs of interest at D0 (Baseline), D7, D28
day <- c("_D0","_D7","_D28")
day_num <- c("0","7","28")
for(i in 1:length(day)){
  # i = 3
  otu_data_out <- otu_data[otu_data$VISIT_DAY == day_num[i],-c(1,2,4:6)]
  otu_data_haart <- otu_data_out[grep("^E1",otu_data_out$PATIENT_ID),]
  row.names(otu_data_haart) <- paste0(gsub("-","_",paste0("TIV_",otu_data_haart$PATIENT_ID)),day[i])
  otu_data_haart$PATIENT_ID <- NULL
  if(i == 1){
    otu_data_one <- otu_data_haart
  } else {
    otu_data_one <- rbind(otu_data_one, otu_data_haart)
  }
}
otu_data_one <- t(otu_data_one)

otu_haart <- merge(otu_data_ann, otu_data_one, by="row.names")
colnames(otu_haart)[1] <- "OTU"
otu_haart

 # write.table(otu_haart, 
 #             file="/Users/adinasarapu/Documents/Emory/share/UGANDA/standard_files/otu_haart.txt", 
 #             sep="\t",   col.names = T, row.names = F, quote=FALSE
 # )
 

# read FASTA sequence file                            
# Microbiome testing technology                       
# V4 16S rRNA gene sequencing with the MiSeq platform 

# Human stool samples, 
# To identify microbial species a region of 16S gene amplified and sequenced
###################################################
# STEP 1: UPARSE pipeline from reads to OTU table #
###################################################
# Operational Taxonomic Units (OTUs)
# Use one of the R packages for OTU clustering (UPARSE, biom, mothur, pyrotagger, qiime, RDP)
# http://drive5.com/usearch/manual/upp_tut_misop.html

###########################
# STEP 2: OTU-clustering  #
###########################
# OUTs are defined based on similarity clustering using R packages such as  
# QIIME, mothur, PANGEA, RDP pipeline, Pyrotagger, CLoVR- 16S, 
# Genboree, QIIME EC2 image, n3phele, MG-RAST 
# phylOTU, pyrotagger, 

# QIIME (canonically pronounced chime) stands for Quantitative Insights Into Microbial Ecology


# STEP 2: Use a) sample_data and b) OTU clustering as input for "phyloseq" function
# "phyloseq" -->  1) OTU abundance (otu_table)        --> matrix
#                 2) Sample Variables (sample_data)   --> data.frame
#                 3) Taxonomy Table (taxonomyTable)   --> matrix
#                 4) Phylogenetic Tree (phylo)        --> use "ape" package
#                 5) Reference Seq (XStringSet)       --> use "Biostrings" package
# http://bioconductor.org/packages/release/bioc/html/phyloseq.html
library("phyloseq")
library("ggplot2")

theme_set(theme_bw())


otutab_file = "/Users/adinasarapu/Documents/Emory/share/Vax002/UPARSE/misop/out/mouse1/otutab.txt"
otumat <- read.table(
  file = otutab_file, 
  skip = 0,  sep = "\t", header=T, stringsAsFactors=F, fill = TRUE, quote = "")

#mothur_file = list("/Users/adinasarapu/Documents/Emory/share/Vax002/UPARSE/misop/out/mouse1/otutab.mothur")
#import_mothur(mothur_file)
import_usearch_uc(otutab_file)

row.names(otumat) <- otumat$OTU.ID
otumat$OTU.ID <- NULL
taxmat <- otumat[,"taxonomy",drop=FALSE]
otumat$taxonomy <- NULL

otumat <- as.matrix(otumat)
taxmat <- as.matrix(taxmat)
head(taxmat)

OTU = otu_table(otumat, taxa_are_rows = TRUE)
TAX = tax_table(taxmat)
OTU
TAX

# http://joey711.github.io/phyloseq/plot_ordination-examples
physeq = phyloseq(OTU, TAX)
physeq
# plot_heatmap
plot_heatmap(physeq, fill = "taxonomy")
# ntaxa(physeq)
# plot_ordination
physeq.ord <- ordinate(physeq)
#plot_scree(physeq,physeq.ord)
p1 = plot_ordination(physeq, physeq.ord, type = "taxa", color = "taxonomy", title = "taxa")
#print(p1)
p1 + facet_wrap(~taxonomy, 3)

p2 = plot_ordination(physeq, physeq.ord, type = "samples", shape = "human")

# plot_bar
plot_bar(physeq)

# 4.
library(ape)
tree_file <- "/Users/adinasarapu/Documents/Emory/share/UGANDA_SP/Second_Genome/Supporting_Files_EPAN15_0259_2015-10-23/idealized_tree.tre"
pdf("/Users/adinasarapu/Desktop/Tree_Plot.pdf")
plot(read.tree(tree_file))
dev.off()
# 5.
library(Biostrings)
fasta_file <- "/Users/adinasarapu/Documents/Emory/share/UGANDA_SP/Second_Genome/Supporting_Files_EPAN15_0259_2015-10-23/repSeqs.fasta"
# Read 2 FASTA files at once: readDNAStringSet(c(filepath2, filepath3))
fastaFile <- readDNAStringSet(fasta_file)
head(fastaFile)
seq_name = names(fastaFile)
sequence = paste(fastaFile)
df <- data.frame(seq_name, sequence)
dim(df)



# sequence width 251 nt
# total microbial OTUs (operational taxonomic units): 19188





# STEP 3: 
# STEP 4: 
# STEP 5: 

# STEP2: cluster reads into OTU (based on similarity)
# a. estimate alpha and beta diversities in the context of sample metadata
# b. statistical tests for differential abundance
 

 
 
 
 ###################
 # HAI assay titer #
 ###################
 library("plyr")
 
 # A/Victoria/361/2011 (H3N2)
 # A/California/7/09 (H1N1)
 # B/Massachusetts/2/12
 
 # excel sheet numbers
 # 1: HAART_H3N2
 # 2: LTNP_H3N2*
 # 3: Healthy_H3N2
 # 4: HAART_H1N1
 # 5: LTNP_H1N1*
 # 6: Healthy (H1N1)
 # 7: HAART_B
 # 8: LTNP_B*
 # 9: Healthy_B
 
 parseExcelFile <- function(s_file,header,sheet,startRow,endRow,startCol, endCol){
   # load the library "XLConnect"
   library("XLConnect")
   s_df <- readWorksheetFromFile(s_file, 
                                 header=header,
                                 sheet=sheet, 
                                 startRow=startRow,
                                 endRow=endRow,
                                 startCol=startCol,
                                 endCol=endCol) 
   # unload the library "XLConnect"
   detach("package:XLConnect")
   return (s_df)
 }
   
 # get Healthy "log2_transformed/D0_normalized" HAI titers 
 getMaxDeviatedAbTiters <- function(st){
   vac_strains <- c(paste0(st,"_H1N1"),paste0(st,"_H3N2"),paste0(st,"_B"))
   for(i in 1:length(vac_strains)){
     # i = 1
     hai_data <- parseExcelFile("/Users/adinasarapu/Documents/Emory/share/UGANDA/HAI/HAI_Uganda.xls", TRUE, vac_strains[i], 1, 27, 1, 5)
     # hai_data <- hai_data[complete.cases(hai_data),]
     hai_data[is.na(hai_data)] <- 10
     hai_data
     
     log2_d0 <- log(hai_data$D0,2)
     log2_d28 <- log(hai_data$D28,2)
     log2_d100 <- log(hai_data$D100,2)
     
     log2_D28vD0 <- log2_d28-log2_d0
     log2_D100vD0 <- log2_d100-log2_d0
     
     one_hai <- data.frame(ID=hai_data$ID, 
                           D0=log2_d0,
                           D28=log2_d28,
                           D100=log2_d100,
                           D28vD0=log2_D28vD0,
                           D100vD0=log2_D100vD0,
                           titer=vac_strains[i]
     )
     if(i == 1){
       two_hai <- one_hai
     } else{
       two_hai <- rbind(two_hai,one_hai)
     }
   }
   
   final_hai <- ddply(two_hai,.(ID),summarize,
                D28vD0 = D28vD0[which.max(abs(D28vD0))],
                D100vD0 = D100vD0[which.max(abs(D100vD0))])
   row.names(final_hai) <- gsub("-","_",final_hai$ID)
   final_hai$ID <- NULL
   final_hai_d28 <- final_hai[,"D28vD0", drop=FALSE]
   row.names(final_hai_d28) <- paste(row.names(final_hai_d28),"D28vD0", sep="_")
   final_hai_d100 <- final_hai[,"D100vD0", drop=FALSE]
   row.names(final_hai_d100) <- paste(row.names(final_hai_d100), "D100vD0", sep="_")
   hai_out <- cbind(t(final_hai_d28),t(final_hai_d100))
   row.names(hai_out) <- NULL
   return(hai_out)
 }
 st <- "Healthy" # HAART, LTNP, Healthy
 ab_norm_data <- getMaxDeviatedAbTiters(st)
 ab_norm_data
 
  write.table(ab_norm_data, 
              file="/Users/adinasarapu/Documents/Emory/share/UGANDA/standard_files/hai_max_healthy.txt", 
              sep="\t",   col.names = T, row.names = F, quote=FALSE
  )
 