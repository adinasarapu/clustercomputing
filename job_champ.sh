#!/bin/sh
echo "Start - `date`"
## job is rerunnable
#$ -r n
## name for the job 
#$ -N ChAMP_Pro
## The cluster resources (the nodes) are grouped into Queues.
## Each queue is associated with a number of slots: one computational process runs in each slot;
#$ -q all.q
#$ -l h_rt=100:00:00
## ask for more CPU (slots) using -pe option
## request parallel environment (-pe) mpi requesting 60 slots/cores ( ?? )
#$ -pe smp 30
#$ -cwd
## merge standard error with standard output
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
## mkdir -p myProject/{src,doc/{api,system},tools,db}
## qconf -sq all.q
###############################################################################
## Cores = Cores per socket X Sockets (4 x 2 = 8 Cores)
## CPUs = Threads per core X Cores (2 x 8 = 16 CPUs/Processors)
## lscpu (2 sockets; 4 cores/ socket; 2 threads / core; i.e total 16 threads)
## lscpu | egrep 'Thread|Core|Socket|^CPU\('
## You need to be careful of the term CPU as it means different things in different contexts
###############################################################################
module load R/3.3.2
Rscript $HOME/clustercomputing/ChampProcessMethylChip.R
module unload R/3.3.2
